﻿namespace Wazzapps
{
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(WazzappsSettings))]
public class WazzappsSettingsEditor : Editor
{
    bool showPackageName = false;
    public override void OnInspectorGUI()
		{
			EditorGUILayout.LabelField("Bundle identifier:", new GUILayoutOption[0]);

			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.SelectableLabel(PlayerSettings.bundleIdentifier, EditorStyles.textField, GUILayout.Height(EditorGUIUtility.singleLineHeight));
			if (GUILayout.Button ("Изменить")) {
				EditorApplication.ExecuteMenuItem("Edit/Project Settings/Player");
			}
			EditorGUILayout.EndHorizontal ();

			GUILayout.Space (20);

            this.DrawDefaultInspectorWithoutScriptField();
                    
            WazzappsSettings settings = (WazzappsSettings)target;

			string[] defines = PlayerSettings.GetScriptingDefineSymbolsForGroup (BuildTargetGroup.iOS).Split(';');

			List<string> newDefines = new List<string> ();
			foreach (string define in defines) {
				if (define == "IOS_PUSH_ENABLED") {
					continue;
				}
				newDefines.Add (define);
			}
			if(WazzappsSettings.EnablePushInIOS) {
				newDefines.Add ("IOS_PUSH_ENABLED");
			}
			PlayerSettings.SetScriptingDefineSymbolsForGroup (BuildTargetGroup.iOS, string.Join(";", newDefines.ToArray()));

			if (GUILayout.Button ("Сбросить настройки")) {
				settings.Clear ();
			}
        }
}
    public static class DefaultInspector_EditorExtension
    {
        public static bool DrawDefaultInspectorWithoutScriptField(this Editor Inspector)
        {
            EditorGUI.BeginChangeCheck();

            Inspector.serializedObject.Update();

            SerializedProperty Iterator = Inspector.serializedObject.GetIterator();

            Iterator.NextVisible(true);

            while (Iterator.NextVisible(false))
            {
                EditorGUILayout.PropertyField(Iterator, true);
            }

            Inspector.serializedObject.ApplyModifiedProperties();

            return (EditorGUI.EndChangeCheck());
        }
    }
}