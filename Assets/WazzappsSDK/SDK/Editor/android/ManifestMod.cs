namespace Wazzapps
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;

    using UnityEditor;
    using UnityEngine;

    public class ManifestMod
    {
        public const string ApplicationName = "org.wazzapps.sdk.WazzApplication";
        public const string ActivityName = "org.wazzapps.wazzappssdkunity.WazzappsUnitySDKActivity";
        public const string TokenMetaDataName = "WazzappsSDKToken";
        public const string AndroidManifestPath = "Plugins/Android/AndroidManifest.xml";
        public const string AndroidManifestName = "AndroidManifest.xml";
        public const string WazzappsDefaultAndroidManifestPath = "WazzappsSDK/SDK/Editor/android/DefaultAndroidManifest.xml";

#if UNITY_EDITOR
        [MenuItem("Wazzapps/Generate Manifest")]

        


        public static void GenerateManifest()
        {
            var outputFile = Path.Combine(Application.dataPath, ManifestMod.AndroidManifestPath);
            // Create containing directory if it does not exist
            Directory.CreateDirectory(Path.GetDirectoryName(outputFile));

            // only copy over a fresh copy of the AndroidManifest if one does not exist
            if (!File.Exists(outputFile)) {
                ManifestMod.CreateDefaultAndroidManifest(outputFile);
            }

            UpdateManifest(outputFile);
        }
#endif
        public static bool CheckManifest()
        {
            bool result = true;
            var outputFile = Path.Combine(Application.dataPath, ManifestMod.AndroidManifestPath);
            if (!File.Exists(outputFile)) {
                // Debug.LogError("An android manifest must be generated for the Wazzapps SDK to work. ");
                return false;
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(outputFile);

            if (doc == null) {
                Debug.LogError("Couldn't load " + outputFile);
                return false;
            }

            XmlNode manNode = FindChildNode(doc, "manifest");
            XmlNode dict = FindChildNode(manNode, "application");

            if (dict == null) {
                Debug.LogError("Error parsing " + outputFile);
                return false;
            }
            /*
            XmlElement loginElement;
            if (!ManifestMod.TryFindElementWithAndroidName(dict, UnityLoginActivityName, out loginElement))
            {
                Debug.LogError(string.Format("{0} is missing from your android manifest.  Go to Facebook->Edit Settings and press \"Regenerate Android Manifest\"", LoginActivityName));
                result = false;
            }*/



            return result;
        }

        public static bool UpdateManifest(string fullPath)
        {
            string token = WazzappsSettings.Token;


            XmlDocument doc = new XmlDocument();
            doc.Load(fullPath);

            if (doc == null) {
                Debug.LogError("Couldn't load " + fullPath);
                return false;
            }

            XmlNode manNode = FindChildNode(doc, "manifest");
            XmlNode dict = FindChildNode(manNode, "application");

            if (dict == null) {
                Debug.LogError("Error parsing " + fullPath);
                return false;
            }

            string ns = dict.GetNamespaceOfPrefix("android");

            
            //try find application element without name, or wazzppsname
            XmlElement applicationElement;
            if (TryFindElementWithAndroidName(manNode, "", out applicationElement, "application") || TryFindElementWithAndroidName(manNode, ApplicationName, out applicationElement, "application")) {

                applicationElement.SetAttribute("name", ns, ApplicationName);

            } else return false;
            //add wazzapps applicaton
            

            // add the new wazzapps activity or replace unity activity
            //
            XmlElement activityElement;
            if (TryFindElementWithAndroidName(applicationElement, "com.unity3d.player.UnityPlayerNativeActivity", out activityElement, "activity") ||
                TryFindElementWithAndroidName(applicationElement, "com.unity3d.player.UnityPlayerActivity", out activityElement, "activity") ||
                TryFindElementWithAndroidName(applicationElement, ActivityName, out activityElement, "activity") ||
                TryFindElementWithAndroidName(applicationElement, "", out activityElement, "activity")) {

                activityElement.SetAttribute("name", ns, ActivityName);
                activityElement.SetAttribute("label",ns, "@string/app_name");



            }
            // We have to create an intent filter 
            XmlElement intentFilter;
            bool created_filter = false;
            if (!TryFindElementWithAndroidName(activityElement, "", out intentFilter, "intent-filter")) {
                intentFilter = doc.CreateElement("intent-filter");
                created_filter = true;
            }

            //add action or find exist
            XmlElement action;
            if (!TryFindElementWithAndroidName(intentFilter, "android.intent.action.MAIN", out action, "action")) {
                action = doc.CreateElement("action");
                
                intentFilter.AppendChild(action);

            }
            action.SetAttribute("name", ns, "android.intent.action.MAIN");
            XmlElement category;
            if (!TryFindElementWithAndroidName(intentFilter, "android.intent.category.LAUNCHER", out category, "category")) {
                category = doc.CreateElement("category");
                
                intentFilter.AppendChild(category);

            }
            category.SetAttribute("name", ns, "android.intent.category.LAUNCHER");
            if (created_filter) {
                activityElement.AppendChild(intentFilter);
            }
            XmlElement dataElement;
            if (!TryFindElementWithAndroidName(activityElement, "unityplayer.UnityActivity", out dataElement, "meta-data")) {

                dataElement = doc.CreateElement("meta-data");
                dataElement.SetAttribute("name",ns, "unityplayer.UnityActivity");
                dataElement.SetAttribute("value",ns, "true");
                activityElement.AppendChild(dataElement);
            } 

            // add the token
            // <meta-data android:name="WazzappsSDKToken" android:value="<Token>" />
            XmlElement appIdElement;
            if (!TryFindElementWithAndroidName(applicationElement, TokenMetaDataName, out appIdElement, "meta-data")) {
                appIdElement = doc.CreateElement("meta-data");
                appIdElement.SetAttribute("name", ns, TokenMetaDataName);
                appIdElement.SetAttribute("value", ns, token);
                applicationElement.AppendChild(appIdElement);
            } else {
                appIdElement.SetAttribute("value", ns, token);
            }








            
            /* // add the login activity
             XmlElement loginElement = CreateLoginElement(doc, ns);
             ManifestMod.SetOrReplaceXmlElement(dict, loginElement);





             // Add the facebook activity
             // <activity
             //   android:name="org.wazzapps.wazzappssdkunity.WazzappsUnitySDKActivity"
             //   android:label="@string/app_name"

             XmlElement facebookElement = CreateWazzappsElement(doc, ns);
             ManifestMod.SetOrReplaceXmlElement(dict, facebookElement);
             */
            // Save the document formatted
            XmlWriterSettings settings = new XmlWriterSettings {
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            };
            
            using (XmlWriter xmlWriter = XmlWriter.Create(fullPath, settings)) {
                doc.Save(xmlWriter);
            }
            Debug.Log("Manifest generated succesfully!");

            return true;
        }

      private static XmlNode FindChildNode(XmlNode parent, string name)
        {
            XmlNode curr = parent.FirstChild;
            while (curr != null) {
                if (curr.Name.Equals(name)) {
                    return curr;
                }

                curr = curr.NextSibling;
            }

            return null;
        }
        
        private static void SetOrReplaceXmlElement(
            XmlNode parent,
            XmlElement newElement)
        {
            string attrNameValue = newElement.GetAttribute("name");
            string elementType = "application";

            XmlElement existingElment;
            if (TryFindElementWithAndroidName(parent, attrNameValue, out existingElment, elementType)) {
                parent.ReplaceChild(newElement, existingElment);
            } else {
                parent.AppendChild(newElement);
            }
        }

        private static bool TryFindElementWithAndroidName(
            XmlNode parent,
            string attrNameValue,
            out XmlElement element,
            string elementType = "activity")
        {
            string ns = parent.GetNamespaceOfPrefix("android");
            var curr = parent.FirstChild;
            while (curr != null) {
                var currXmlElement = curr as XmlElement;
                if (currXmlElement != null &&
                    currXmlElement.Name == elementType && 
                    currXmlElement.GetAttribute("name", ns) == attrNameValue ) {
                    element = currXmlElement;
                    return true;
                }

                curr = curr.NextSibling;
            }

            element = null;
            return false;
        }
        
        private static void AddSimpleActivity(XmlDocument doc, XmlNode xmlNode, string ns, string className, bool export = false)
        {
            XmlElement element = CreateActivityElement(doc, ns, className, export);
            ManifestMod.SetOrReplaceXmlElement(xmlNode, element);
        }

        private static XmlElement CreateWazzappsActivity(XmlDocument doc, string ns)
        {
            // <activity android:name="org.WazzappsSDKUnityActivity" >
            // </activity>
            XmlElement activityElement = ManifestMod.CreateActivityElement(doc, ns, ActivityName);
            activityElement.SetAttribute("label", ns, "@string/app_name");
            return activityElement;
        }
    

        private static XmlElement CreateActivityElement(XmlDocument doc, string ns, string activityName, bool exported = false)
        {
            // <activity android:name="activityName" android:exported="true">
            // </activity>
            XmlElement activityElement = doc.CreateElement("activity");
            activityElement.SetAttribute("name", ns, activityName);
            if (exported) {
                activityElement.SetAttribute("exported", ns, "true");
            }

            return activityElement;
        }

        private static XmlElement CreateApplicationElement(XmlDocument doc, string ns, string applicationName, bool exported = false)
        {
            // <activity android:name="activityName" android:exported="true">
            // </activity>
            XmlElement applicationElement = doc.CreateElement("application");
            applicationElement.SetAttribute("name", ns, applicationName);
            

            return applicationElement;
        }

        private static void AddWazzApp(XmlDocument doc, XmlNode xmlNode, string ns)
        {
            XmlNode dict = FindChildNode(xmlNode, "application");
            XmlElement element;
            //if (null == dict)
                element = ManifestMod.CreateApplicationElement(doc, ns, ApplicationName, false);
            
           // dict.
            
            


            ManifestMod.SetOrReplaceXmlElement(xmlNode, element);
        }

        private static void CreateDefaultAndroidManifest(string outputFile)
        {
            var inputFile = Path.Combine(
                EditorApplication.applicationContentsPath,
                "PlaybackEngines/androidplayer/AndroidManifest.xml");
            if (!File.Exists(inputFile)) {
                // Unity moved this file. Try to get it at its new location
                inputFile = Path.Combine(
                    EditorApplication.applicationContentsPath,
                    "PlaybackEngines/AndroidPlayer/Apk/AndroidManifest.xml");

                if (!File.Exists(inputFile)) {
                    // On Unity 5.3+ we don't have default manifest so use our own
                    // manifest and warn the user that they may need to modify it manually
                    inputFile = Path.Combine(Application.dataPath, WazzappsDefaultAndroidManifestPath);
                    Debug.Log(
                        string.Format(
                            "No existing android manifest found at '{0}'. Creating a default manifest file",
                            outputFile));
                }
            }

            File.Copy(inputFile, outputFile);
        }
    }
}
