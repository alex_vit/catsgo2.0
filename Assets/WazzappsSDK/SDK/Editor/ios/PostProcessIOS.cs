#if (UNITY_5 && UNITY_IOS)
using UnityEditor;
using UnityEngine;
using UnityEditor.Callbacks;
using System.Collections;

using UnityEditor.iOS.Xcode;
using System.IO;
 
// Настройка XCode проекта для работы его с SDK
public class PostProcessIOS {
    [PostProcessBuild]
    public static void ChangeInfoPlist(BuildTarget buildTarget, string pathToBuiltProject) {
 
		if (buildTarget == BuildTarget.iOS) {

            // Get plist
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));
       
            // Get root
            PlistElementDict rootDict = plist.root;
       
			var buildKey = "WazzappsSDKToken";
			rootDict.SetString(buildKey, Wazzapps.WazzappsSettings.TokenIOS);

			PlistElementDict nSAppTransportSecurity = rootDict.CreateDict ("NSAppTransportSecurity");
			nSAppTransportSecurity.SetBoolean ("NSAllowsArbitraryLoads", true);

			PlistElementDict nSExceptionDomains = nSAppTransportSecurity.CreateDict ("NSExceptionDomains");

			PlistElementDict wazzappsException = nSExceptionDomains.CreateDict ("wazzapps.org");
			wazzappsException.SetBoolean ("NSIncludesSubdomains", true);
			wazzappsException.SetBoolean ("NSTemporaryExceptionAllowsInsecureHTTPLoads", true);
			wazzappsException.SetString ("NSTemporaryExceptionMinimumTLSVersion", "TLSv1.1");
       
            // Write to Info.plist
			File.WriteAllText(plistPath, plist.WriteToString());

			// Изменяем настройки проекта
			string project = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj" ;

			PBXProject projectPBX = new PBXProject();
			projectPBX.ReadFromFile (project);
			string releaseBuild = projectPBX.TargetGuidByName ("Unity-iPhone");

			projectPBX.SetBuildProperty (releaseBuild, "LD_RUNPATH_SEARCH_PATHS", "$(inherited) @executable_path/Frameworks");
			projectPBX.SetBuildProperty (releaseBuild, "EMBEDDED_CONTENT_CONTAINS_SWIFT", "YES");
			projectPBX.SetBuildProperty (releaseBuild, "CLANG_ENABLE_MODULES", "YES");
			projectPBX.SetBuildProperty (releaseBuild, "GCC_ENABLE_OBJC_EXCEPTIONS", "YES");
			projectPBX.SetBuildProperty (releaseBuild, "ENABLE_BITCODE", "NO");

			// Добавим фреймворк в Copy Bundles фазу
			string frameworkDir = pathToBuiltProject + "/Unity-iPhone/Frameworks";
			if (Directory.Exists (frameworkDir) == false) {
				Directory.CreateDirectory (frameworkDir);
				DirectoryCopy (pathToBuiltProject + "/Frameworks/Plugins/iOS/WazzappsAdSDK.framework", frameworkDir+"/WazzappsAdSDK.framework", true);
			}
			string wazzFramework = projectPBX.AddFile("Unity-iPhone/Frameworks", "Frameworks");
			projectPBX.AddFileToBuild (releaseBuild, wazzFramework);

			// Добавим/исключим push
			if (Wazzapps.WazzappsSettings.EnablePushInIOS == false) {
				projectPBX.RemoveFile (projectPBX.FindFileGuidByProjectPath ("Libraries/Plugins/iOS/WazzappsFirebaseAdsNotifications.h"));
				projectPBX.RemoveFile (projectPBX.FindFileGuidByProjectPath ("Libraries/Plugins/iOS/WazzappsFirebaseAdsNotifications.m"));
				File.Delete (pathToBuiltProject + "/Libraries/Plugins/iOS/WazzappsFirebaseAdsNotifications.h");
				File.Delete (pathToBuiltProject + "/Libraries/Plugins/iOS/WazzappsFirebaseAdsNotifications.m");
			} else {
				projectPBX.UpdateBuildProperty (releaseBuild, "HEADER_SEARCH_PATHS", new string[]{"$(inherited)"}, null);
				projectPBX.UpdateBuildProperty (releaseBuild, "OTHER_CFLAGS", new string[]{"$(inherited)"}, null);
				projectPBX.UpdateBuildProperty (releaseBuild, "OTHER_LDFLAGS", new string[]{"$(inherited)"}, null);
				projectPBX.UpdateBuildProperty (releaseBuild, "HEADER_SEARCH_PATHS", new string[]{"$(inherited)"}, null);
				File.WriteAllText (pathToBuiltProject + "/Podfile", "platform :ios, '8.0'\nuse_frameworks!\n\npod 'Firebase/Messaging'\n");
			}
			projectPBX.WriteToFile (project);
        }
    }

	private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
	{
		// Get the subdirectories for the specified directory.
		DirectoryInfo dir = new DirectoryInfo(sourceDirName);

		if (!dir.Exists)
		{
			throw new DirectoryNotFoundException(
				"Source directory does not exist or could not be found: "
				+ sourceDirName);
		}

		DirectoryInfo[] dirs = dir.GetDirectories();
		// If the destination directory doesn't exist, create it.
		if (!Directory.Exists(destDirName))
		{
			Directory.CreateDirectory(destDirName);
		}

		// Get the files in the directory and copy them to the new location.
		FileInfo[] files = dir.GetFiles();
		foreach (FileInfo file in files)
		{
			string temppath = Path.Combine(destDirName, file.Name);
			file.CopyTo(temppath, false);
		}

		// If copying subdirectories, copy them and their contents to new location.
		if (copySubDirs)
		{
			foreach (DirectoryInfo subdir in dirs)
			{
				string temppath = Path.Combine(destDirName, subdir.Name);
				DirectoryCopy(subdir.FullName, temppath, copySubDirs);
			}
		}
	}
}
#endif