﻿namespace Wazzapps
{
    using UnityEngine;
    using System.Collections;
    using System;
    using System.Text;
   
	using System.Collections.Generic;
	using MiniJSON;


#if UNITY_IOS && !UNITY_EDITOR
	using System.Runtime.InteropServices;
	using NotificationServices = UnityEngine.iOS.NotificationServices;
	using NotificationType = UnityEngine.iOS.NotificationType;
#endif
    
    public class WazzButton
    {
        public Texture2D Texture;
        public String Text;
    }

    public class WazzappsAds : MonoBehaviour
    {
		public bool showOnStart = false;

        /// <summary>
        /// Событие закрытия рекламного окна
        /// </summary>
        public static event Action AdClosed = delegate { };
        public static event Action AdLeftApplication = delegate { };
        public static event Action ButtonLoaded = delegate { };
		public static event Action<String> PushReceived = delegate { };
        public static WazzButton seeAlsoButton;

        private static WazzappsAds m_Instance;
		public static WazzappsAds instance {
			get {
				if (m_Instance == null) {
					m_Instance = FindObjectOfType<WazzappsAds> ();
				}
				return m_Instance;
			}
		}

#if UNITY_ANDROID && !UNITY_EDITOR
		private static AndroidJavaObject wazzappsSDKObject;
		#elif UNITY_IOS && !UNITY_EDITOR
		// Init SDK and set token from info.plist
		[DllImport ("__Internal")]
		private static extern void WazzappsAds_InitUnitySDK();


		#if IOS_PUSH_ENABLED
		// Init firebase must call before WazzappsAds_AskAndInitNotifications()
		[DllImport ("__Internal")]
		private static extern void WazzappsAds_InitFirebaseNotifications();

		// Ask permission for push
		[DllImport ("__Internal")]
		private static extern void WazzappsAds_AskAndInitNotifications();
		#endif

		// Init UnitySendMessages callback
		[DllImport ("__Internal")]
		private static extern void WazzappsAds_InitUnityMessages();

		// Loads Ad
		[DllImport ("__Internal")]
		private static extern void WazzappsAds_LoadAd(bool isForced);

		// Check is Ad ready
		[DllImport ("__Internal")]
		private static extern bool WazzappsAds_IsAdReady();

		// Show Ad if ready
		[DllImport ("__Internal")]
		private static extern void WazzappsAds_ShowAd();

		// Load See Also
		[DllImport ("__Internal")]
		private static extern void WazzappsAds_LoadSeeAlso();

		// Run See Also
		[DllImport ("__Internal")]
		private static extern void WazzappsAds_RunSeeAlsoAction();

		// OnPause
		[DllImport ("__Internal")]
		private static extern void WazzappsAds_ApplicationDidEnterBackground();

		// OnResume
		[DllImport ("__Internal")]
		private static extern void WazzappsAds_ApplicationDidBecomeActive();

		// OnDestroy
		[DllImport ("__Internal")]
		private static extern void WazzappsAds_PushRecievedJSON(String pushInfo);

		// OnPushRecieved
		[DllImport ("__Internal")]
		private static extern void WazzappsAds_ApplicationWillTerminate();
#endif

        private static bool dummyAdShow;

        void Start()
        {
            DontDestroyOnLoad(gameObject);

			gameObject.name = "UnityWazzappsSDKPlugin";
			InitWazzSDK(showOnStart);
        }

		// Init SDK
		private void InitWazzSDK(bool showOnStart)
        {
#if !UNITY_EDITOR && UNITY_ANDROID
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            wazzappsSDKObject = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
			wazzappsSDKObject.Call("LoadAd", showOnStart); 
#elif !UNITY_EDITOR && UNITY_IOS
			WazzappsAds_InitUnityMessages();
			WazzappsAds_InitUnitySDK();
			WazzappsAds_LoadAd(showOnStart);
#endif
        }

		public void AskAndEnableNotifications() {
			
	#if !UNITY_EDITOR && UNITY_ANDROID
			Debug.Log("Not implemented in android");
	#elif !UNITY_EDITOR && UNITY_IOS

			#if IOS_PUSH_ENABLED

			WazzappsAds_InitFirebaseNotifications();
			WazzappsAds_AskAndInitNotifications();

			#endif
	#else
			Debug.Log("Push is not implemented in Editor");
	#endif

		}


#region JavaPluginCallbacks
        public void MessageHandler(string message)
        {
            switch (message) {

                case "ad_closed":
                    AdClosed();
                    break;

                default:
                    Debug.LogError("Unhandled message : " + message);
                    break;
            }
        }

		// See also button was loaded
        public void ButtonReceiver(string buttonData)
        {
            Dictionary<string, object> buttonInfo = Json.Deserialize(buttonData) as Dictionary<string, object>;
            
            if (null == buttonInfo) { 
                Debug.LogError("Error decode json");
           		return;                
            }
            var wazzButton = new WazzButton();
            
            System.Object buttonText = "";
            if (buttonInfo.TryGetValue("text", out buttonText)) {
                wazzButton.Text = buttonText as String;
            } else {
                Debug.LogError("ButtonText Parse Error");
                return;
            }
            
            System.Object buttonImage = "";
            if (buttonInfo.TryGetValue("image", out buttonImage)) {

                Texture2D text = new Texture2D(1, 1, TextureFormat.ARGB32, false);
                byte[ ] imageBytes = Convert.FromBase64String(buttonImage as String);
                if (null != imageBytes) {
                    if (!text.LoadImage(imageBytes)) Debug.LogError("ButtonImage Load Error");
                }
                wazzButton.Texture = text;

            } else {
                Debug.LogError("ButtonImage Parse Error");
                return;
            }
            
            seeAlsoButton = wazzButton;
            ButtonLoaded();
        }

		// Push recieved
        public void PushReceiver(string message)
        {
//            Debug.Log("PushReceived : " + message);
			PushReceived(message);
        }
#endregion

		// Start loading See also button
        public static void LoadButton()
        {
			#if !UNITY_EDITOR && UNITY_ANDROID
			if (null != wazzappsSDKObject) {
				wazzappsSDKObject.Call ("LoadButton"); 
			}
			#elif !UNITY_EDITOR && UNITY_IOS
			WazzappsAds_LoadSeeAlso();
			#elif UNITY_EDITOR
			Debug.Log("LoadButton dummy in editor");
			#endif
        }

		// Launch See also button action
        public static void RunButtonAction()
		{
			#if !UNITY_EDITOR && UNITY_ANDROID
			if (null != wazzappsSDKObject) {
				wazzappsSDKObject.Call ("RunButtonAction");
			}
			#elif !UNITY_EDITOR && UNITY_IOS
			WazzappsAds_RunSeeAlsoAction();
			#elif UNITY_EDITOR
			Debug.Log("RunButtonAction dummy in editor");
			#endif
        }

		// Show Interstitial AD
        public static void ShowAd()
        {
			#if !UNITY_EDITOR && UNITY_ANDROID
			if (null != wazzappsSDKObject) {
				wazzappsSDKObject.Call ("ShowAd");
			}
			#elif !UNITY_EDITOR && UNITY_IOS
			WazzappsAds_ShowAd();
			#elif UNITY_EDITOR
			dummyAdShow = true;
			#endif
        }

		public static bool isAdReady
		{
			get {
				#if UNITY_ANDROID && !UNITY_EDITOR
				if (null != wazzappsSDKObject) {
				return wazzappsSDKObject.Call<bool>("isAdReady");
				} else {
				return false;
				}
				#elif UNITY_IOS && !UNITY_EDITOR
				return WazzappsAds_IsAdReady();
				#elif UNITY_EDITOR
				return true;
				#endif
			}
		}

#if !UNITY_EDITOR && UNITY_IOS
		void OnApplicationQuit() {
			WazzappsAds_ApplicationWillTerminate();
		}
		void OnApplicationPause(bool isPause) {
			if(isPause) {
				WazzappsAds_ApplicationDidEnterBackground();
			} else {
				WazzappsAds_ApplicationDidBecomeActive();
			}
		}

		void Update () {
		if(NotificationServices.remoteNotificationCount > 0) {
//		Debug.Log("NotificationServices.remoteNotifications="+NotificationServices.remoteNotifications);
		for (int i = 0; i < NotificationServices.remoteNotificationCount; i++) {
//		Debug.Log (Json.Serialize(NotificationServices.remoteNotifications[i].userInfo));
		WazzappsAds_PushRecievedJSON (Json.Serialize(NotificationServices.remoteNotifications[i].userInfo));
		}
		NotificationServices.ClearRemoteNotifications ();
		}
		}
		#endif

#if UNITY_EDITOR
		// Show dummy AD
        public void OnGUI()
        {
            if (dummyAdShow) {
                GUI.Box(new Rect(Screen.width / 10, Screen.height / 10, 0.8f * Screen.width, 0.8f * Screen.height), "Dummy WazzApps AD");
                if (GUI.Button(new Rect(Screen.width / 5, 3 * Screen.height / 5, 0.6f * Screen.width, 0.1f * Screen.height), "Close")) {
                    dummyAdShow = false;
                    AdClosed();
                }  

            }
        }
#endif
   }
}
