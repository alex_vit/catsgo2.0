﻿namespace Wazzapps
{
    using UnityEngine;
    using System.Collections;

    public static class Debug 
    {
        public static void Log(string message)
        {

            if (WazzappsSettings.Logging)
                UnityEngine.Debug.Log(message);
        }

        public static void LogError(object message)
        {
            if (WazzappsSettings.Logging)
                UnityEngine.Debug.LogError(message);
        }




    }
}
