﻿
namespace Wazzapps
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using UnityEngine;

#if UNITY_EDITOR
    [InitializeOnLoad]
#endif
    public class WazzappsSettings  : ScriptableObject
    {
        private const string WazzappsSettingsAssetName = "WazzappsSettings";
        private const string WazzappsSettingsPath = "WazzappsSDK/SDK/Resources";
        private const string WazzappsSettingsAssetExtension = ".asset";

        private static WazzappsSettings instance;

        [SerializeField]
		private string token;
		[SerializeField]
		private string tokenIOS;
		[SerializeField]
		private bool enablePushInIOS;

        private bool logging = true;
#if UNITY_EDITOR
        [MenuItem("Wazzapps/Edit Settings")]
        
        public static void Edit()
        {
            Selection.activeObject = Instance;
        }
#endif
        public static void SettingsChanged()
        {
            WazzappsSettings.DirtyEditor();
        }
        public static bool Logging
        {
            get
            {
                return Instance.logging;
            }

            set
            {
                if (Instance.logging != value)
                {
                    Instance.logging = value;
                    DirtyEditor();
                }
            }
        }
        public static string Token
        {
            get
            {
                return Instance.token;
            }

            set
            {
                if (Instance.token != value)
                {
                    Instance.token = value;
                    DirtyEditor();
                }
            }
		}
		public static string TokenIOS
		{
			get
			{
				return Instance.tokenIOS;
			}

			set
			{
				if (Instance.tokenIOS != value)
				{
					Instance.tokenIOS = value;
					DirtyEditor();
				}
			}
		}

        public static bool EnablePushInIOS
        {
            get
            {
                return Instance.enablePushInIOS;
            }

            set
            {
                if (Instance.enablePushInIOS != value)
                {
                    Instance.enablePushInIOS = value;
                    DirtyEditor();
                }
            }
        }
            
 
        private static WazzappsSettings Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = Resources.Load(WazzappsSettingsAssetName) as WazzappsSettings;
                    if (instance == null)
                    {
                        // If not found, autocreate the asset object.
                        instance = ScriptableObject.CreateInstance<WazzappsSettings>();
#if UNITY_EDITOR
                        string properPath = Path.Combine(Application.dataPath, WazzappsSettingsPath);
                        if (!Directory.Exists(properPath))
                        {
                            Directory.CreateDirectory(properPath);
                        }

                        string fullPath = Path.Combine(
                            Path.Combine("Assets", WazzappsSettingsPath),
                            WazzappsSettingsAssetName + WazzappsSettingsAssetExtension);
                        AssetDatabase.CreateAsset(instance, fullPath);
#endif
                    }
                }

                return instance;
            }
        }

        private static void DirtyEditor()
        {
#if UNITY_EDITOR
            EditorUtility.SetDirty(Instance);
#endif
        }

        public void Clear()
        {
			token = "";
			tokenIOS = "";
            
        }
       
    }
}
