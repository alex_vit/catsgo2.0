﻿using UnityEngine;
using System;
using System.Collections;

public class PathCalculator {
	static readonly double _eQuatorialEarthRadius = 6378.1370D;
	static readonly double _d2r = (Math.PI / 180D);

	static Vector2 beginPos = Vector2.zero;
	static Vector2 endPos = Vector2.zero;
	static double distance = 0;

	public static int DistInMeter(double lat1, double long1, double lat2, double long2) {
		return (int) (1000D * DistInKilometer(lat1, long1, lat2, long2));
	}

	public static double DistInKilometer(double lat1, double long1, double lat2, double long2) {
		double dlong = (long2 - long1) * _d2r;
		double dlat = (lat2 - lat1) * _d2r;
		double a = Math.Pow((dlat / 2D), 2D) + Math.Cos(lat1 * _d2r) * Math.Cos(lat2 * _d2r) * Math.Pow((dlong / 2D), 2D);
		double c = 2D * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1D - a));
		double d = _eQuatorialEarthRadius * c;
		return d;
	}

	public static void Run(Vector2 pos){
		beginPos = pos;
		distance = 0;
	}

	public static void Stop(){
		beginPos = Vector2.zero;
		distance = 0;
	}

	public static void Update(Vector2 currPos){
		if (beginPos != Vector2.zero) {
			distance += DistInKilometer (beginPos.x, beginPos.y, currPos.x, currPos.y);
			if (distance >= 1D) {
				AchievmentManager.GetInstance ().ReceiveMsg (PlayerAction.PASS_DIST);
				//DailyQuestManager.GetInstance ().ReceiveMsg (PlayerAction.PASS_DIST);
				distance = 0;
			}
			beginPos.x = currPos.x;
			beginPos.y = currPos.y;
		}
	}

	public static double GetDist(Vector2 currPos){
		double _dist = DistInKilometer (beginPos.x, beginPos.y, currPos.x, currPos.y);
		beginPos.x = currPos.x;
		beginPos.y = currPos.y;
		return _dist;
	}
}
