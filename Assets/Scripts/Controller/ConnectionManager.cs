﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.IO;

public class ConnectionManager{
	static ConnectionManager _instance;
	// Use this for initialization

	public static ConnectionManager GetInstance(){
		return _instance;
	}

	public static bool CheckGPS(){
		#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass jc = new AndroidJavaClass("com.wazzapps.unitylocationlibrary.GPSManager");
		int status = jc.CallStatic<int> ("checkGPSStatus");
		if(status != 0) {
			return false;
		} else {
			Input.location.Start();
			return true;
		}
		#else
		return true;
		#endif
	}

	public static bool CheckInternetConnection(){
		WebClient _client = null;

		try{
			_client = new WebClient();
			byte[] _raw = _client.DownloadData("http://api.wazzapps.org/timestamp.php");
			//string _time = System.Text.Encoding.UTF8.GetString(_raw);
			//CDebug.Log("time : " + _time); 
			return true;
		}catch(Exception e){
			CDebug.Log(e.ToString ());
			return false;
		}finally{
			if(_client != null) {_client.Dispose ();}
		}
	}

	public static Int64 GetGlobalTime(){
		WebClient _client = null;
		try{
			_client = new WebClient();
			byte[] _raw = _client.DownloadData("http://api.wazzapps.org/timestamp.php");
			Int64 _time = Convert.ToInt64(System.Text.Encoding.UTF8.GetString(_raw));
			CDebug.Log("time : " + _time.ToString()); 
			return _time;
		}catch(Exception e){
			CDebug.Log(e.ToString ());
			return 0;
		}finally{
			if(_client != null) {_client.Dispose ();}
		}
	}

	public static void TryEnableInternet(){
		#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass jc = new AndroidJavaClass("com.wazzapps.unitylocationlibrary.GPSManager");
		jc.CallStatic("goWiFiSettings");
		#else
		#endif
	}

	public static void TryEnableGPS(){
		#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass jc = new AndroidJavaClass("com.wazzapps.unitylocationlibrary.GPSManager");
		jc.CallStatic("goGPSSettings");
		#else
		#endif
	}
}
