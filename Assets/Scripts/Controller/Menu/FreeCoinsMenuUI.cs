﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FreeCoinsMenuUI : MonoBehaviour {
	static FreeCoinsMenuUI _instance;

	[SerializeField]
	FreeMoneyButton menuFreeMoneyButton;
	[SerializeField]
	Text menuFreeCoinsText;
	// Use this for initialization
	void Awake () {
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
	}

	public static FreeCoinsMenuUI GetInstance(){
		return _instance;
	}

	public void Activate(){
		menuFreeMoneyButton.TurnOn ();
		//menuFreeCoinsText.text = Localizator.GetTextValue (_define.menuWatchAdButton);
	}

	public void Desactivate(){
		menuFreeMoneyButton.TurnOff ();

	}

	public void UpdateInfo(int currMin, int currSec){
		menuFreeCoinsText.text = Localizator.GetTextValue (_define.menuWatchAdButtonInactive) + " " + currMin.ToString () + ":" + (currSec < 10 ? "0" + currSec.ToString () : currSec.ToString ());
	}
}
