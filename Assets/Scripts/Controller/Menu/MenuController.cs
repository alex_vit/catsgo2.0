﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using Advertise;
using System;

namespace Menu{
	public class MenuController : MonoBehaviour {
		static MenuController _instance;

		// Use this for initialization
		void Awake(){
			if (_instance == null)
				_instance = this;
			else
				Destroy (gameObject);
			Advertise.AdCernel.GetInstance ().ShowBanner (AdType.BANNER_BOTTOM);
		}

		void Start(){
			Analytics.LogEvent (_define.gameActionKey, new Dictionary<string, object> () {
				{_define.playerMenuActionKey, true}	
			});
			TryStartMenuScene ();
		}

		public static MenuController GetInstance(){
			return _instance;
		}

		internal void OnPlayGame(){
			if (StaticData.playerInfo.level < 3)
				++StaticData.mapBannerCounter;
			else
				StaticData.mapBannerCounter++;
			if(StaticData.mapBannerCounter % 3 == 0)
				AdCernel.GetInstance ().ShowBanner (AdType.INTERSTITIAL);
			FreeCoinsController.GetInstance ().State = GameState.LOADING;
			SceneManager.LoadScene ("Map");
		}

		internal void OnMoreGames(){
			SocialController.GetInstance ().GoToAppAccount ();
		}

		void OnBackClick(){
			Application.Quit ();
		}

		internal void TurnOnOffSound(bool enable){
			StaticData.Sound = (enable ? 1f :  0f);
			CDebug.Log("sound " + StaticData.Sound);
		}

		public void TryStartMenuScene(){
			if (CheckConnectionState ())
				CheckDailyQuests ();
			else
				CDebug.Log ("fail gps or inet");
		}

		internal void CheckDailyQuests(){
			if (!StaticData.playing) {
				StaticData.playing = true;
				if (!DailyQuestManager.GetInstance ().isOpened)
					DailyQuestManager.GetInstance ().TryOpenDailyQuests ();
				else
					DailyQuestManager.GetInstance ().CheckDailyQuests ();

				//DailyQuestManager.GetInstance ().CheckDailyQuests ();
			}
		}

		bool CheckConnectionState(){
			bool isInternetConnected = false;
			bool isGPSConnected = false;



			isInternetConnected = ConnectionManager.CheckInternetConnection ();
			isGPSConnected = ConnectionManager.CheckGPS ();
			if (isInternetConnected && isGPSConnected)
				return true;
			if (!isInternetConnected && !isGPSConnected) {
				MenuViewController.GetInstance ().ShowNoConnectionDialog (_define.notAvailableGPSAndInternet);
				return false;
			}
			if (!isInternetConnected) {
				CDebug.Log ("no inet state");
				MenuViewController.GetInstance ().ShowNoConnectionDialog (_define.notAvailableInternet);
				return false;
			}
			if (!isGPSConnected) {
				MenuViewController.GetInstance ().ShowNoConnectionDialog (_define.notAvailableGPS);
				return false;
			}
			return false;
		}
	}
}