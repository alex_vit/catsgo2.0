﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/*
 * class that describes menu UI logic
 */
namespace Menu{
	public class MenuViewController : MonoBehaviour {
		static MenuViewController _instance;
		[Header("Panels")]
		[SerializeField]
		GameObject menuPanel;
		[SerializeField]
		GameObject tint;
		[Header("UI Elements")]
		[SerializeField]
		Dialog newDailyQuestWindow;
		[SerializeField]
		Dialog notConnectionWindow;
		[SerializeField]
		GameObject turnOffAdButton;
		[SerializeField]
		SwitchButton soundButton;
		[SerializeField]
		Dialog freeCoinsWindow;

		GameObject mActivePanel;

		// Use this for initialization
		void Awake(){
			if (_instance == null)
				_instance = this;
			else
				Destroy (gameObject);

			if (!StaticData.availableAd)
				SetTurnOffAdButtonInActive ();

			soundButton.OnSwitchState += MenuController.GetInstance ().TurnOnOffSound;
			CheckSound ();
		}

		void Start(){
			FreeCoinsController.GetInstance ().State = GameState.MENU;
		}

		public static MenuViewController GetInstance(){
			return _instance;
		}
		#region button handlers
		public void OnPlayButttonClick(){
			MenuController.GetInstance ().OnPlayGame ();
		}

		public void OnMoreGamesButtonClick(){
			MenuController.GetInstance ().OnMoreGames ();
		}

		public void TurnOffAdButtonClick(){
			ShopController.GetInstance ().MakePurchase (_define.noAdsId);
		}

		public void SetTurnOffAdButtonInActive(){
			//turnOffAdButton.SetActive (false);
		}

		public void OnVkGroupButtonClick(){
			SocialController.GetInstance ().GoToVkGroup ();
		}
			
		public void OnWatchVideoButtonClick(){
			if (FreeCoinsController.GetInstance ().IsAvailable ()) {
				if (!StaticData.isAdvWindowShowed) {
					(freeCoinsWindow as AlertDialog).OkAction += () => {
						StaticData.isAdvWindowShowed = true;
						Advertise.AdCernel.GetInstance ().ShowVideo (AdType.REWARDED_VIDEO, SuccessFreeCoinsClick, null, null);
					};
					(freeCoinsWindow as AlertDialog).OnHide += () => {
						SetTintPanelVisibility (false);
					};
					ShowDialog (freeCoinsWindow);
				}else
					Advertise.AdCernel.GetInstance ().ShowVideo (AdType.REWARDED_VIDEO, SuccessFreeCoinsClick, null, null);
			}
		}

		public void OnUpdateStateAppButtonClick(){
			MenuController.GetInstance ().TryStartMenuScene ();
		}

		public void OnBackToMenuButtonClick(){
			ShowPanel (menuPanel);
		}

		#endregion
		#region methods for panels
		public void ShowPanel(GameObject panel){
			if (mActivePanel != null)
				mActivePanel.SetActive (false);
			mActivePanel = panel;
			mActivePanel.SetActive (true);
		}
		public void SetTintPanelVisibility(bool visible){
			tint.SetActive (visible);
		}
		#endregion
		#region work with dialog windows
		internal void ShowNoConnectionDialog(string errorCode){
			(notConnectionWindow as AlertDialog).SetText (errorCode);
			(notConnectionWindow as AlertDialog).OnHide += () => {
				SetTintPanelVisibility (false);
				//MenuController.GetInstance ().TryStartMenuScene ();
			};
			ShowDialog (notConnectionWindow);
		}

		public void ShowNewDailyQuestWindow(DailyQuest dc){
			newDailyQuestWindow.GetComponent<TakeDailyQuestWindow> ().Show (dc);
			(newDailyQuestWindow as AlertDialog).OnHide += () => {
				SetTintPanelVisibility (false);
			};
			ShowDialog (newDailyQuestWindow);
		}

		void ShowDialog(Dialog dialog){
			SetTintPanelVisibility (true);
			dialog.Show ();
		}
		#endregion

		void CheckSound(){
			soundButton.SwitchState (StaticData.Sound == 1f);
			CDebug.Log ("volume " + StaticData.Sound);
		}

		void SuccessFreeCoinsClick(){
			Analytics.LogEvent (_define.advWatchInfoKey, new Dictionary<string, object>(){{_define.advWatchInfoKey, true}});
			ShopController.GetInstance ().GetFreeCoins ();
			//SetFreeMoneyTimer (_define.timerForFreeMoney);
			FreeCoinsController.GetInstance().StartTimer();
		}
	}
}
