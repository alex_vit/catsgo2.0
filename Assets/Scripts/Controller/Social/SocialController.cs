﻿using UnityEngine;
using System.Collections;

public class SocialController : MonoBehaviour {
	static SocialController _instance;

	void Awake(){
		if (_instance == null) {
			_instance = this;
		} else
			Destroy (gameObject);
	}

	public static SocialController GetInstance(){
		return _instance;
	}

	public void GoToVkGroup(){
		Application.OpenURL ("https://vk.com/go.cats");
	}

	public void GoToAppAccount(){
		Application.OpenURL("https://play.google.com/store/apps/developer?id=Banana4apps");
	}
}
