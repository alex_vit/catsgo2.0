﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Assets.Helpers;
using UnityEngine.SceneManagement;
using Advertise;

namespace Map{
	public class MapController : MonoBehaviour {
		static MapController _instance;

		[SerializeField]
		AnimatedPlayer player;
		public float border;
		public Vector2 pos;

		Bounds mCurrentTileBounds;

		bool isInternetConnected;
		bool isGPSConnected;
		float mOffset = 35f;

		public Text testText;

		// Use this for initialization
		void Awake(){
			if (_instance == null)
				_instance = this;
			else
				Destroy (gameObject);
		}

		void Start () {
			World.GetInstance ().OnInited += () => {
				MapViewController.GetInstance ().HideLoadingWindow ();
				PlacePlayerToMap ();
				if(StaticData.tutorial){
					MapViewController.GetInstance ().ShowPlayerMenuButton (false);
					TutorialManager.GetInstance ().Run ();
				}

			};
			player.OnTileChanged += () => {
				World.GetInstance ().UpdateTiles();
				ObjectGenerator.GetInstance ().Generate (World.GetInstance().GetCenterTile().GetInfo().TileTms);
			};
			player.OnSetActive += () => {TutorialManager.GetInstance ().ShowMapPart(); CheckOutNotificationReward();};
			TutorialManager.GetInstance ().OnEnded += PlacePlayerToMap;
			MapViewController.GetInstance ().ShowPlayerMenuButton (!StaticData.tutorial);
			//World.GetInstance ().Init (player);
			//MapViewController.GetInstance ().ShowLoadingDialog ();
		}

		void OnBackClick(){
			if (World.GetInstance ().isInited) {
				SceneManager.LoadScene ("Menu");
			}
		}

		void OnEnable(){
			//CheckConnectionState ();
		}

		public void Run(){
			Analytics.LogEvent (_define.gameActionKey, new Dictionary<string, object> () {
				{_define.mapActionKey, true}	
			});
			if (!World.GetInstance ().isInited) {
				World.GetInstance ().Init (player);
				MapViewController.GetInstance ().ShowLoadingDialog ();
			}
			ObjectGenerator.GetInstance ().Run ();
			//if world isn't inited draw it
			MapViewController.GetInstance ().UpdatePlayerInfo ();
			if (!StaticData.tutorial && !StaticData.isRewarded) {
				MapViewController.GetInstance ().ShowPlayerMenuButton (true);
				//player.SetActive ();
				CDebug.Log ("start map");
				//PathCalculator.Run (World.GetInstance ().CalculateNewPlayerPos ());
				StopAllCoroutines ();
				StartCoroutine (UpdatePlayerPosition ());

			} //else if (StaticData.tutorial) {
				//MapViewController.GetInstance ().ShowPlayerMenuButton (false);
				//TutorialManager.GetInstance ().Run ();
			//}
			//ObjectGenerator.GetInstance ().Generate (World.GetInstance().GetCenterTile().ID);

		}

		public void Stop(){
			//StopAllCoroutines ();
		}

		public void CheckConnections(){
			StartCoroutine (CheckConnection ());
		}

		public static MapController GetInstance(){
			return _instance;
		}
		/// <summary>
		/// Sets the player to default position.
		/// </summary>
		void SetPlayerPosition(){
			CDebug.Log ("center tile" + World.GetInstance ().GetCenterTile ().ID);
			Vector2 _pos = World.GetInstance ().CalculateNewPlayerPos ();

			double _dist = PathCalculator.GetDist (_pos);
			testText.text = _dist.ToString ();
			player.GetTransform ().position = new Vector3 (_pos.x, player.GetTransform ().position.y, _pos.y);
			player.CheckPosition ();
			player.SetActive ();
		}

		/// <summary>
		/// Places the player to map.
		/// </summary>
		void PlacePlayerToMap(){
			#if UNITY_EDITOR
			player.SetActive();
			#endif
			CDebug.Log ("center tile " + World.GetInstance ().GetCenterTile ().ID); 

			if (!StaticData.tutorial && gameObject.activeSelf) {
				//PathCalculator.Run (World.GetInstance ().CalculateNewPlayerPos ());
				StopAllCoroutines ();
				StartCoroutine (UpdatePlayerPosition ());
			} else {
				player.SetActive ();
			}
			ObjectGenerator.GetInstance ().Generate (World.GetInstance().GetCenterTile().GetInfo().TileTms);
		}

		void CheckOutNotificationReward(){
			if (StaticData.isRewarded) {
				StaticData.isRewarded = false;
				if (!StaticData.tutorial) {
					Vector2 _pos = new Vector2 (player.GetTransform ().position.x + mOffset, player.GetTransform ().position.z + mOffset);
					ObjectGenerator.GetInstance ().GenerateCatAtPos ((Rarity)StaticData.currReward, _pos);
				}
			}
		}

		void CheckConnectionState(){
			bool gpsWindowOpened = false;
			bool dataWindowOpened = false;



			isInternetConnected = ConnectionManager.CheckInternetConnection ();
			isGPSConnected = ConnectionManager.CheckGPS ();
			if (!isInternetConnected) {
				if (!dataWindowOpened) {
					MapViewController.GetInstance ().ShowNoInternetDialog ();
					dataWindowOpened = true;
				}
			} else if (!isGPSConnected) {
				if (!gpsWindowOpened) {
					MapViewController.GetInstance ().ShowNoGPSDialog ();
					gpsWindowOpened = true;
				}
			}

			if (isInternetConnected && isGPSConnected) {
				Run ();
				return;
			}
		}

		#region methods for UI
		public void BackToMenu(){
			//PathCalculator.Stop ();
			SceneManager.LoadScene ("Menu");
		}

		public void OpenPlayerMenu(){
			if (StaticData.playerInfo.level < 3)
				++StaticData.playerMenuBannerCounter;
			else
				StaticData.playerMenuBannerCounter++;
			if(StaticData.playerMenuBannerCounter % 3 == 0)
				AdCernel.GetInstance ().ShowBanner (AdType.INTERSTITIAL);
			//PathCalculator.Stop ();
			World.GetInstance ().Stop ();
			SceneManager.LoadScene ("PlayerMenu");
		}
		#endregion

		#region co-rutines
		IEnumerator UpdatePlayerPosition(){
			while (true) {
				yield return new WaitForSeconds (0.5f);
				#if !UNITY_EDITOR
				SetPlayerPosition ();
				#else
				//CheckPlayerPosition();
				player.CheckPosition();
				#endif
			}
		}

		IEnumerator CheckConnection(){
			bool gpsWindowOpened = false;
			bool dataWindowOpened = false;
			while (true) {
				if (isInternetConnected && isGPSConnected) {
					Run ();
					break;
				}
				isInternetConnected = ConnectionManager.CheckInternetConnection ();
				isGPSConnected = ConnectionManager.CheckGPS ();
				if (!isInternetConnected) {
					if (!dataWindowOpened) {
						MapViewController.GetInstance ().ShowNoInternetDialog ();
						dataWindowOpened = true;
					}
				} else if (!isGPSConnected) {
					if (!gpsWindowOpened) {
						MapViewController.GetInstance ().ShowNoGPSDialog ();
						gpsWindowOpened = true;
					}
				}
				yield return new WaitForSeconds (_define.timeStep);
			}
		}
		#endregion


	}
}
