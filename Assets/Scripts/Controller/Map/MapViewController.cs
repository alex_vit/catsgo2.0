﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

namespace Map{
	public class MapViewController : MonoBehaviour {
		static MapViewController _instance;
		[Header("Dialogs")]
		[SerializeField]
		Dialog loadingDialog;
		[SerializeField]
		Dialog noGPSDialog;
		[SerializeField]
		Dialog noInternetDialog;
		[SerializeField]
		Dialog achieveView;
		[Header("Panels")]
		[SerializeField]
		GameObject dialogPanel;
		[Header("Player UI")]
		[SerializeField]
		Text playerLevelText;
		[SerializeField]
		ImageSlider playerLevelView;
		[SerializeField]
		Text playerMoneyText;
		[SerializeField]
		FadeText notifyText;
		[SerializeField]
		GameObject playerMenuButton;
		[SerializeField]
		TakeAchieveWindow achieveWindow;

		void Awake(){
			if (_instance == null)
				_instance = this;
			else
				Destroy (gameObject);
		}
		// Use this for initialization
		void Start () {
			notifyText.OnFaded += UpdatePlayerInfo;
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public static MapViewController GetInstance(){
			return _instance;
		}

		public void BackButtonClick(){
			MapController.GetInstance ().BackToMenu ();
		}

		public void PlayerMenuButtonClick(){
			MapController.GetInstance ().OpenPlayerMenu ();
		}
		


		public void HideLoadingWindow(){
			loadingDialog.Hide ();
			dialogPanel.SetActive (false);
		}

		public void UpdatePlayerInfo(){
			playerLevelText.text = StaticData.playerInfo.level.ToString ();
			playerMoneyText.text = StaticData.playerInfo.money.ToString ();
			playerLevelView.SetDefaultValue (StaticData.playerInfo.nextLevelXp);
			playerLevelView.RecalculateValue (StaticData.playerInfo.currentXp);
		}

		public void ShowPlayerMenuButton(bool isActive){
			playerMenuButton.SetActive (isActive);
		}

		#region dialogs methods
		public void ShowLoadingDialog(){
			dialogPanel.SetActive (true);
			loadingDialog.Show ();
		}

		public void ShowNoInternetDialog(){
			dialogPanel.SetActive (true);
			noInternetDialog.Show ();
			(noInternetDialog as AlertDialog).OkAction += () => {
				ConnectionManager.TryEnableInternet ();
			};
		}

		public void ShowNoGPSDialog(){
			dialogPanel.SetActive (true);
			noGPSDialog.Show ();
			(noGPSDialog as AlertDialog).OkAction += () => {
				ConnectionManager.TryEnableGPS ();
			};
		}
		#endregion

		public void ShowMoneyNotification(string msg, Vector3 pos){
			notifyText.ShowText (msg, pos, playerMoneyText.transform.position);
		}

		public void ShowNewAchieveDialog(int id){
			dialogPanel.SetActive (true);
			achieveWindow.Show (id);
			achieveView.Show();
		}
	}
}
