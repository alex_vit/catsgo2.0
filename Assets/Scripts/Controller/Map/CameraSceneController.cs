﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Game;
using Map;

public class CameraSceneController : MonoBehaviour {
	static CameraSceneController _instance;

	[Header("Objects needs activated for map")]
	[SerializeField]
	List<GameObject> mapObjects;
	[Header("Objects needs activated for game")]
	[SerializeField]
	List<GameObject> gameObjects;

	[SerializeField]
	Camera mapCamera;
	[SerializeField]
	Camera gameCamera;


	Camera mCamera;
	GameState _state = GameState.MAP_VIEW;
	public GameState State{
		get{ return _state;}
		set{ 
			_state = value;

			if (_state == GameState.GAME_VIEW) {
				SetActiveMapScene (false);
				SetActiveGameScene (true);
				GameController.GetInstance ().Run ();
			}

			if (_state == GameState.MAP_VIEW) {
				SetActiveGameScene (false);
				SetActiveMapScene (true);
			}
		}
	}
		
	void Awake(){
		if (_instance == null) {
			_instance = this;
		} else
			Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {
		Init ();
	}

	public static CameraSceneController GetInstance(){
		return _instance;
	}

	void Init(){
		State = GameState.MAP_VIEW;
	}

	public Camera GetActiveCamera(){
		return mCamera;
	}

	#region methods for change view
	void SetActiveGameScene(bool isActive){
		for (int i = 0; i < gameObjects.Count; i++) {
			gameObjects [i].SetActive (isActive);
		}
		if(isActive)
			mCamera = gameCamera;
	}

	void SetActiveMapScene(bool isActive){
		for (int i = 0; i < mapObjects.Count; i++) {
			mapObjects [i].SetActive (isActive);
		}
		if (isActive) {
			//MapController.GetInstance ().CheckConnections ();
			mCamera = mapCamera;
			MapController.GetInstance ().Run ();
		}
		else
			MapController.GetInstance ().Stop ();
	}
	#endregion
}
