﻿using UnityEngine;
using System.Collections;

public class GeneralUIController : MonoBehaviour {
	static GeneralUIController _instance;
	[SerializeField]
	GameObject generalUIPanel;
	[SerializeField]
	TakeAchieveWindow completeAchieveDialog;
	[SerializeField]
	CompleteDailyQuestWindow completeQuestDialog;
	[SerializeField]
	TakeDailyQuestWindow getNewQuestWindow;

	// Use this for initialization
	void Awake(){
		if (_instance == null) {
			_instance = this;
		}
		else
			Destroy (gameObject);
	}

	public static GeneralUIController GetInstance(){
		return _instance;
	}

	public void ShowAchieveCompleteWindow(Achieve ach){
		ShowUIPanel ();
		completeAchieveDialog.Show (ach.id);
		completeAchieveDialog.GetComponent<AlertDialog>().Show();
		completeAchieveDialog.GetComponent<AlertDialog> ().OnHide += HideUIPanel;
	}

	public void ShowQuestCompleteWindow(DailyQuest dc){
		ShowUIPanel ();
		completeQuestDialog.Show (dc);
		completeQuestDialog.GetComponent<AlertDialog>().Show();
		completeQuestDialog.GetComponent<AlertDialog> ().OnHide += HideUIPanel;
	}

	public void ShowNewQuestWindow(DailyQuest dc){
		CDebug.Log ("show_quest");
		//ShowUIPanel ();
		getNewQuestWindow.Show (dc);
		getNewQuestWindow.GetComponent<AlertDialog> ().Show ();
		//getNewQuestWindow.GetComponent<AlertDialog> ().OnHide += HideUIPanel;
	}

	void ShowUIPanel(){
		generalUIPanel.SetActive (true);
	}

	void HideUIPanel(){
		generalUIPanel.SetActive (false);
	}
}
