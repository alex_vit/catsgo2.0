﻿using UnityEngine;
using System.Collections;
using Map;
using System;
using System.Collections.Generic;

public class TutorialManager : MonoBehaviour {
	static TutorialManager _instance;

	[Header("Tutorial components")]
	public AnimatedPlayer player;
	public ObjectFactory objFactory;
	public RectTransform mapHand;
	public RectTransform gameHand;
	public GameObject gameDirArrow;
	public GameObject playerMenuButton;
	public GameObject backFromGameButton;
	public Camera mapCamera;
	public ClickController clickController;
	public Dialog finalTutorialDialog;

	public float minScaleItem;
	public float maxScaleItem;
	float mOffset = 30f;

	public Action OnMapPartEnded;
	public Action OnEnded;

	void Awake(){
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
	}
	public static TutorialManager GetInstance(){
		return _instance;
	}

	public void Run(){
		Analytics.LogEvent (_define.tutorialActionKey, new Dictionary<string, object> () {
			{_define.tutorialBeginKey, true}	
		});
	}

	public void ShowMapPart(){
		if (StaticData.tutorial) {
			//instantiate cat near player
			//check player position
			Vector2 _pos = new Vector2 (player.GetTransform ().position.x + mOffset, player.GetTransform ().position.z + mOffset);
			SpawnedItem _cat = ObjectGenerator.GetInstance ().GenerateBeginCat (_pos);
			clickController.OnItemClicked += () => {
				HideMapPart(_cat);
			};
			StartCoroutine (BlinkCat (_cat.transform));
			playerMenuButton.SetActive (false);

			Analytics.LogEvent (_define.tutorialActionKey, new Dictionary<string, object> () {
				{ _define.tutorialBeginKey, true }	
			});
		}
	}

	public void ShowGamePart(){
		if (StaticData.tutorial) {
			gameHand.gameObject.SetActive (true);
			StartCoroutine (BlinkGame (gameHand));
			gameDirArrow.SetActive (true);
			backFromGameButton.SetActive (false);
			ObjectCatchController.GetInstance ().OnObjectCatched += () => {
				HideGamePart ();
				//EndTutorial();
			};
		}
	}

	void HideMapPart(SpawnedItem tutorialCat){
		if (StaticData.tutorial) {
			Analytics.LogEvent (_define.tutorialActionKey, new Dictionary<string, object> () {
				{ _define.tutorialMapStageKey, true }	
			});
			StopCoroutine("BlinkCat");
			Destroy(tutorialCat.gameObject);
		}
	}

	void HideGamePart(){
		if (StaticData.tutorial) {
			StopAllCoroutines ();
			gameHand.gameObject.SetActive (false);
			gameDirArrow.SetActive (false);
			Analytics.LogEvent (_define.tutorialActionKey, new Dictionary<string, object> () {
				{ _define.tutorialGameStageKey, true }	
			});
		}
	}

	public void ShowFinalPart(){
		if (StaticData.tutorial) {
			(finalTutorialDialog as AlertDialog).SetText (_define.finalTutorialText);
			finalTutorialDialog.OnHide += EndTutorial;
			finalTutorialDialog.Show ();
		}
	}

	void EndTutorial(){
		Analytics.LogEvent (_define.tutorialActionKey, new Dictionary<string, object> () {
			{_define.tutorialEndedKey, true}	
		});
		playerMenuButton.SetActive(true);
		backFromGameButton.SetActive (true);
		StaticData.tutorial = false;
		StaticData.SaveInfo ();

		if (OnEnded != null)
			OnEnded ();
	}

	IEnumerator Blink(Vector2 pos, RectTransform trans){
		Vector2 beginPos = pos + new Vector2 (20, 20);
		Vector2 endPos = pos + new Vector2 (50, 50);

		Vector2 offset = new Vector2 (2f, 2f);
		trans.anchoredPosition = beginPos;
		bool flag = false;
		while (true) {
			if (trans.anchoredPosition.x >= endPos.x || trans.anchoredPosition.x <= beginPos.x) {
				flag = !flag;
			}
			if (flag) {
				trans.anchoredPosition += offset;
			} else {
				trans.anchoredPosition -= offset;
			}
			yield return new WaitForFixedUpdate ();
		}
	}

	IEnumerator BlinkGame(RectTransform trans){
		Vector2 beginPos = new Vector2 (0, -300);
		Vector2 endPos = new Vector2 (0, 0);

		Vector2 offset = new Vector2 (0f, 2f);
		trans.anchoredPosition = beginPos;
		bool flag = false;
		while (true) {
			if (trans.anchoredPosition.y >= endPos.y || trans.anchoredPosition.y <= beginPos.y) {
				flag = !flag;
			}
			if (flag) {
				trans.anchoredPosition += offset;
			} else {
				trans.anchoredPosition -= offset;
			}
			yield return new WaitForFixedUpdate ();
		}
	}

	IEnumerator BlinkCat(Transform item){
		Vector3 _currScale;
		Vector3 _step = new Vector3 (0.1f, 0, 0.1f);
		bool _flag = true;
		while (true) {
			if (item != null) {
				if (item.localScale.x >= maxScaleItem || item.localScale.x <= minScaleItem) {
					_flag = !_flag;
				}
				if (_flag)
					item.localScale += _step;
				else
					item.localScale -= _step;
				yield return new WaitForFixedUpdate ();
			} else
				break;
		}
	}
}
