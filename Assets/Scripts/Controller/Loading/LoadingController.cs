﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

/*
 *class for scene Loading, that handles initialization logic StaticData, AdManager and e.t.c
 */
public class LoadingController : MonoBehaviour {
	[SerializeField]
	bool clearSettings;

	[Header("Splash")]
	[SerializeField]
	GameObject splash1;
	[SerializeField]
	GameObject splash2;
	void Awake(){
		if (clearSettings)
			ClearSettings ();
		StaticData.Init ();
		Localizator.activeLanguage = (Application.systemLanguage == SystemLanguage.Russian ? Language.RUS : Language.ENG);
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		DontDestroyOnLoad (gameObject);
	}

	void Start(){
		NotificationManager.GetInstance ().Init ();
		AchievmentManager.GetInstance ().Init ();
		DailyQuestManager.GetInstance ().LoadQuests ();
		StopAllCoroutines ();
		StartCoroutine (WaitForLoading (_define.splashScreenTimer));
	}

	void ClearSettings(){
		PlayerPrefs.DeleteAll ();
		Serializator.RemoveProgress ();
	}

	void OnApplicationQuit(){
		//StaticData.playerData.lastTimePlayed = ConnectionManager.GetGlobalTime ();
		//AchievmentManager.GetInstance().CheckAchievmentsDelay(AchieveTimeDelay.SESSION);
		//Serializator.SaveAchivs(AchievmentManager.GetInstance().GetAchievs());
		StaticData.SaveInfo ();
		Analytics.LogEvent (_define.endSessionKey, new Dictionary<string, object>(){{_define.endSessionKey, true}});
	}

	void LoadNextScene(){
		StopAllCoroutines ();
		StaticData.sessionCount++;
		Advertise.AdCernel.GetInstance ().ShowBanner (AdType.INTERSTITIAL);
		int _curIndex = SceneManager.GetActiveScene ().buildIndex;
		if (SceneManager.GetSceneAt (_curIndex+1) != null)
			SceneManager.LoadScene ((_curIndex+1));
		Analytics.LogEvent (_define.startSessionKey, new Dictionary<string, object>(){{_define.startSessionKey, "Session_" + StaticData.sessionCount.ToString()}});
	}

	IEnumerator WaitForLoading(int time){
		int _counter= 0;
		splash1.SetActive (true);
		splash2.SetActive (false);
		while (true) {
			if (_counter++ >= time) {
				LoadNextScene ();
				break;
			}
			if (_counter >= 3) {
				splash1.SetActive (false);
				splash2.SetActive (true);
			}
			yield return new WaitForSeconds (_define.timeStep);
			//LoadNextScene ();
		}
	}
}
