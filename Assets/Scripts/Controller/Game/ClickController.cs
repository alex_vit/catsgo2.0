﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Map;
using System;

public class ClickController : MonoBehaviour, IPointerClickHandler{
	public Camera mapCamera;
	RaycastHit[] mInfo;
	Vector3 mClickPos;

	public Action OnItemClicked;

	void Click(){
		Ray _ray = mapCamera.ScreenPointToRay (Input.mousePosition);
		CDebug.Log (Input.mousePosition);
		mClickPos = Input.mousePosition;
		mInfo = Physics.RaycastAll (_ray, 500);
		for (int i = 0; i < mInfo.Length; i++) {
			Catchable _catch = mInfo [i].collider.GetComponent<Catchable> ();
			if (_catch != null && _catch.IsAvailable ()) {
				//detect type of catch
				if (_catch.itemType == SpawnItemType.CAT) {
					StaticData.playerData.gameInfo.currentCatId = _catch.GetItemInfo().itemId;
					CameraSceneController.GetInstance ().State = GameState.GAME_VIEW;
					_catch.Catch ();
				} else if (_catch.itemType == SpawnItemType.BALL) {
					DailyQuestManager.GetInstance ().ReceiveMsg (PlayerAction.FIND_BALL);
					StaticData.playerData.playerBalls [BallType.WOOL]++;
					_catch.Catch ();
				} else if (_catch.itemType == SpawnItemType.MONEY) {
					int _reward = UnityEngine.Random.Range(1,StaticData.playerInfo.level);
					CDebug.Log ("reward " + _reward);
					StaticData.playerInfo.money += _reward;
					MapViewController.GetInstance ().ShowMoneyNotification ("+" + _reward.ToString (), mClickPos);
					_catch.Catch ();
					DailyQuestManager.GetInstance ().ReceiveMsgWithParam (PlayerAction.FIND_MONEY, _reward);
				}
				StaticData.SaveInfo ();
				//MapViewController.GetInstance ().UpdatePlayerInfo ();
				if(!StaticData.tutorial && StaticData.playerData.spawnedItemsList.ContainsKey(World.GetInstance ().GetCenterTile().GetInfo().TileTms))
					StaticData.playerData.spawnedItemsList [World.GetInstance ().GetCenterTile().GetInfo().TileTms].Remove (_catch.GetItemInfo());

				Analytics.LogEvent (_define.mapSelectItem, new Dictionary<string, object> () {
					{ _define.itemTypeKey, _catch.itemType}
				});

				if (OnItemClicked != null)
					OnItemClicked ();

				break;
			}
		}
	}

	#region IPointerClickHandler implementation

	public void OnPointerClick (PointerEventData eventData)
	{
		Click ();
	}

	#endregion
}
