﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ObjectGenerator : MonoBehaviour {
	static ObjectGenerator _instance;
	[SerializeField]
	ObjectFactory objectFactory;
	[SerializeField]
	int catCountForSquare;
	[SerializeField]
	int ballsCountForSquare;
	[SerializeField]
	int coinsCountForSquare;
	[SerializeField]
	float border;
	[SerializeField]
	float distance;

	public int cellCount;

	List<int> filledTiles;
	List<Vector2> _objectPosList;
	List<SpawnItemInfo> _objectList;

	bool isInited = false;

	List<Vector2> mFilledTiles;
	void Awake(){
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {
		//filledTiles = new List<int> ();

	}

	public void Run(){
		if (!isInited) {
			isInited = true;
			mFilledTiles = new List<Vector2>();
		}
	}

	public static ObjectGenerator GetInstance(){
		return _instance;
	}
	
	public void Generate(Vector2 tileTms){
		//generate cats and balls
		CDebug.Log("center tile ID " + tileTms);

		/*if (!StaticData.playerData.catList.ContainsKey (tileID)) {
			_objectList = new List<Catchable> ();
			_objectPosList = new List<Vector2> ();
			//generate cats
			//Bounds _bounds = World.GetInstance ().GetCenterTileBounds ();
			//GeneratePos (catCountForSquare, _bounds.min, _bounds.max, _offset);
			GeneratePos (catCountForSquare, World.GetInstance ().tileSize, border);
			for (int i = 0; i < _objectPosList.Count; i++) {
				_objectList.Add (objectFactory.CreateCat (_objectPosList [i], World.GetInstance ().GetCenterTile ().transform).GetComponent<Catchable>());
			}
			//generate balls
			_objectPosList = new List<Vector2>();
			GeneratePos (ballsCountForSquare, World.GetInstance ().tileSize, border);
			for (int i = 0; i < _objectPosList.Count; i++) {
				_objectList.Add (objectFactory.CreateBall (_objectPosList [i], World.GetInstance ().GetCenterTile ().transform));
			}
			List<Catchable> _list = new List<Catchable> ();
			for (int i = 0; i < _objectList.Count; i++) {
				_list.Add(_objectList[i]);
			}
			StaticData.playerData.catList.Add (tileID, _list);
		} else {
			/*List<Catchable> _objectsFromThisTile = StaticData.playerData.catList [tileID];
			for (int i = 0; i < _objectsFromThisTile.Count; i++) {
				_objectsFromThisTile [i].transform.localPosition = _objectsFromThisTile [i].GetPosition ();
			}
		}*/

		//generate items
		//check if it's already drawed, not draw once more

		if (!mFilledTiles.Contains (tileTms) && isInited && !StaticData.tutorial) {

			if (!StaticData.playerData.spawnedItemsList.ContainsKey (tileTms)) {
				//spawn items on this tile
				_objectList = new List<SpawnItemInfo> ();
				_objectPosList = new List<Vector2> ();
				//spawn cats
				int _cellSq = cellCount * cellCount;
				//calculate p items
				CDebug.Log("generate items");


				//int _catDensity = Mathf.RoundToInt((float)catCountForSquare/(_cellSq));
				int _currLevel = Mathf.Clamp(StaticData.playerInfo.level, 1, 6);
				int _catDensity = StaticData.MapItemsBalanceInfo [_currLevel].cats;
				int _ballDensity = StaticData.MapItemsBalanceInfo [_currLevel].balls;
				int _moneyDensity = StaticData.MapItemsBalanceInfo [_currLevel].coins;

				int _totalItemCount = _catDensity + _ballDensity + _moneyDensity;

				List<List<Vector2>> itemPosOnCells = GeneratePosForItems (DivideTileToCells (World.GetInstance ().tileSize), World.GetInstance ().tileSize, 3*_totalItemCount);
				CDebug.Log ("Density " + _catDensity + " " + _ballDensity + " " + _moneyDensity);
				CDebug.Log ("itemPosOnCells " + itemPosOnCells.Count);

				for (int i = 0; i < itemPosOnCells.Count; i++) {
					List<Vector2> cellItemPos = itemPosOnCells [i];

					for (int j = 0; j < cellItemPos.Count; j++) {
					//work with concrete positions
						if (j >= _catDensity + _ballDensity && j < _totalItemCount) {
							_objectList.Add (objectFactory.CreateItem (cellItemPos [j], World.GetInstance ().GetCenterTile ().transform, SpawnItemType.MONEY, new SpawnItemInfo ()).GetSpawnInfo ());
						} else if (j >= _catDensity && j <_catDensity + _ballDensity) {
							_objectList.Add (objectFactory.CreateItem (cellItemPos [j], World.GetInstance ().GetCenterTile ().transform, SpawnItemType.BALL, new SpawnItemInfo ()).GetSpawnInfo ());
						} else if (j >= 0 && j < _catDensity) {
							_objectList.Add (objectFactory.CreateItem (cellItemPos[j], World.GetInstance ().GetCenterTile ().transform, SpawnItemType.CAT, new SpawnItemInfo ()).GetSpawnInfo ());
						}
						/*for (int l = _catDensity; l < _catDensity +_ballDensity; l++) {
							_objectList.Add (objectFactory.CreateItem (cellItemPos[l], World.GetInstance ().GetCenterTile ().transform, SpawnItemType.BALL, new SpawnItemInfo ()).GetSpawnInfo ());
						} 
						for (int m = (_catDensity + _ballDensity); m < _catDensity + _ballDensity + _moneyDensity; m++) {
							_objectList.Add (objectFactory.CreateItem (cellItemPos[m], World.GetInstance ().GetCenterTile ().transform, SpawnItemType.BALL, new SpawnItemInfo ()).GetSpawnInfo ());
						}*/
					} 
				
				}

				/*GeneratePos (catCountForSquare, World.GetInstance ().tileSize, border,  catDistance);
				for (int i = 0; i < _objectPosList.Count; i++) {
					_objectList.Add (objectFactory.CreateItem (_objectPosList [i], World.GetInstance ().GetCenterTile ().transform, SpawnItemType.CAT, new SpawnItemInfo ()).GetSpawnInfo ());
				}
				//update position list
				_objectPosList = new List<Vector2> ();
				GeneratePos (ballsCountForSquare, World.GetInstance ().tileSize, border, ballsDistance);
				//generate balls
				for (int i = 0; i < _objectPosList.Count; i++) {
					_objectList.Add (objectFactory.CreateItem (_objectPosList [i], World.GetInstance ().GetCenterTile ().transform, SpawnItemType.BALL, new SpawnItemInfo ()).GetSpawnInfo ());
				}

				_objectPosList = new List<Vector2> ();
				GeneratePos (coinsCountForSquare, World.GetInstance ().tileSize, border, coinDistance);
				//generate coins
				for (int i = 0; i < _objectPosList.Count; i++) {
					_objectList.Add (objectFactory.CreateItem (_objectPosList [i], World.GetInstance ().GetCenterTile ().transform, SpawnItemType.MONEY, new SpawnItemInfo ()).GetSpawnInfo ());
				}

				StaticData.playerData.spawnedItemsList.Add (tileID, new List<SpawnItemInfo> ());
				//write to static class
				for (int i = 0; i < _objectList.Count; i++) {
					SpawnItemInfo _info = new SpawnItemInfo ();
					_info.itemType = _objectList [i].itemType;
					_info.itemId = _objectList [i].itemId;
					_info.itemPos = _objectList [i].itemPos;

					StaticData.playerData.spawnedItemsList [tileID].Add (_info);
				}*/
				StaticData.playerData.spawnedItemsList.Add (tileTms, _objectList);
				mFilledTiles.Add (tileTms);

			} else {
				CDebug.Log("load items from cache");
				//read data and spawn objects at pos
				List<SpawnItemInfo> _info = StaticData.playerData.spawnedItemsList [tileTms];

				for (int i = 0; i < _info.Count; i++) {
					Vector2 _pos = new Vector2 (_info [i].itemPos.x, _info [i].itemPos.z);
					objectFactory.CreateItem (_pos, World.GetInstance ().GetCenterTile ().transform, _info [i].itemType, _info [i]); 
				}
			}
		}
	}



	public List<Vector2> RemoveNearbyPoints(List<Vector2> list, float dist){
		List<Vector2> _res = new List<Vector2> ();

		for (int i = 0; i < list.Count; i++) {
			int count = 0;
			for (int j = 0; j < list.Count; j++) {
				
				if (i != j) {
					if (Vector2.Distance (list [i], list [j]) > dist)
						count++;
				}
				if(count == list.Count-1)
					_res.Add(list[i]);			
			}	
		}
		return _res;
	}
	//new generation
		
	Vector2 DivideTileToCells(Vector2 tileSize){
		float _cellSideX = tileSize.x / cellCount;
		float _cellSideY = tileSize.y / cellCount;
		return new Vector2 (_cellSideX, _cellSideY);
	}


	List<List<Vector2>> GeneratePosForItems(Vector2 cellSize, Vector2 tileSize, int itemCountOnCell){
		List<List<Vector2>> _res = new List<List<Vector2>> ();
		Vector2 _begPoint, endPoint;

		for (int i = 0; i < cellCount; i++) {
			for (int j = 0; j < cellCount; j++) {
				_begPoint.x = i * tileSize.x / cellCount - tileSize.x / 2;
				_begPoint.y = j * tileSize.y / cellCount	- tileSize.y / 2;

				endPoint.x = (i + 1) * tileSize.x / cellCount - tileSize.x / 2;
				endPoint.y = (j + 1) * tileSize.y / cellCount - tileSize.y / 2;
				_res.Add (GeneratePoints(_begPoint, endPoint, itemCountOnCell));
			}
		}
		return _res;
	}

	List<Vector2> GeneratePoints(Vector2 beginPoint, Vector2 endPoint, int quantity){
		List<Vector2> _res = new List<Vector2> ();
		Vector2 _pos;
		for (int i = 0; i < quantity; i++) {
			_pos.x = Random.Range (beginPoint.x, endPoint.x);
			_pos.y = Random.Range (beginPoint.y, endPoint.y);
			_res.Add (_pos);
		}
		//remove useless points

		//return
		return RemoveNearbyPoints(_res, distance);;
	}

	public SpawnedItem GenerateBeginCat(Vector2 pos){
		SpawnItemInfo _info = new SpawnItemInfo{ itemId = 1, itemPos = pos, itemType = SpawnItemType.CAT };
		return objectFactory.CreateItem (_info.itemPos, World.GetInstance ().GetCenterTile ().transform, SpawnItemType.CAT, _info);
	}

	public SpawnedItem GenerateCatAtPos(Rarity rare, Vector2 pos){
		CatInfo[] _idsInfo = StaticData.CatParams.Where (x => x.rarity == rare).ToArray();
		int _id = _idsInfo [Random.Range (0, _idsInfo.Length)].id;
		SpawnItemInfo _info = new SpawnItemInfo{ itemId = _id, itemPos = pos, itemType = SpawnItemType.CAT };
		return objectFactory.CreateItem (_info.itemPos, World.GetInstance ().GetCenterTile ().transform, SpawnItemType.CAT, _info);
	}
}
