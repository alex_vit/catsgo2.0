﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class LevelUpPanel : MonoBehaviour {
	[SerializeField]
	Text levelValueText;
	[SerializeField]
	Text xpValueText;

	public void Show(){
		levelValueText.text = Localizator.GetTextValue(_define.levelText) + " " + StaticData.playerInfo.level.ToString();
		xpValueText.text = "+ " + StaticData.playerInfo.level * 10; 
		if (StaticData.levelUp) {
			StaticData.levelUp = false;
			SendInfo ();
		}
	}

	void SendInfo(){
		Analytics.LogEvent (_define.levelUpInfoKey, new Dictionary<string,object> () {
			{ _define.playerMoneyKey, StaticData.playerInfo.money },
			{ _define.playerLevelKey, StaticData.playerInfo.level }, 
			{ _define.playerCatsCollectionKey, new Dictionary<string, object> {
					{ _define.playerCatsCollectionKindKey, StaticData.playerData.playerCatsList.Keys.ToArray () },
					{ _define.playerCatsCollectionCountKey, StaticData.playerData.playerCatsList.Values.ToArray () }
				}
			}, { _define.playerBallsKey, new Dictionary<BallType, int> {
					{ BallType.WOOL, StaticData.playerData.playerBalls [BallType.WOOL] }, 
					{ BallType.SILVER, StaticData.playerData.playerBalls [BallType.SILVER] },
					{ BallType.GOLD, StaticData.playerData.playerBalls [BallType.GOLD] }	
				}
			},

		});
	}
}
