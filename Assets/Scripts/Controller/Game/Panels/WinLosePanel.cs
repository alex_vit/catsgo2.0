﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

public class WinLosePanel : MonoBehaviour {
	[SerializeField]
	Image winLoseLabel;
	[SerializeField]
	Text winLoseLabelText;
	[SerializeField]
	Image catImage;
	[SerializeField]
	Text catImageText;
	[SerializeField]
	Text playerXpLabel;

	public void Show(bool isWon){
		CatInfo _info = StaticData.CatParams.Where(x=> x.id == StaticData.playerData.gameInfo.currentCatId).First();
		winLoseLabel.sprite = StaticData.UIImages[isWon ? _define.wonGameImage : _define.lostGameImage]; 
		catImage.sprite = StaticData.CatImages[StaticData.playerData.gameInfo.currentCatId];
		catImageText.text = Localizator.GetCatName(_info.id);
		if (isWon) {
			winLoseLabelText.text = Localizator.GetTextValue (_define.catchText);
			playerXpLabel.text = "+ " + _info.xp.ToString () + " " + Localizator.GetTextValue(_define.winXpText);
		} else {
			winLoseLabelText.text = Localizator.GetTextValue (_define.lostText);
			playerXpLabel.text = Localizator.GetTextValue (_define.lostXpText);	
		}
	}
}
