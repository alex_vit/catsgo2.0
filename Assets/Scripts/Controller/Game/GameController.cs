﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Advertise;

/* Main controller for Game scene, it's describe play/stop/resume logic
 */

namespace Game{
	public class GameController : MonoBehaviour {
		static GameController _instance;
		[SerializeField]
		VRCamera gameCamera;
		[SerializeField]
		CameraPlane imageCamera;
		[SerializeField]
		AnimateObjectRotator objectRoatator;

		bool isRunning;

		void Awake(){
			if (_instance == null)
				_instance = this;
			else
				Destroy (gameObject);
		}

		void Start () {
			
			ObjectCatchController.GetInstance ().OnObjectCatched += () =>{OnGameEnded(true);};
			ObjectCatchController.GetInstance ().OnObjectEscaped += () =>{OnGameEnded(false);};

			GameViewController.GetInstance ().OnAddBallTimerEnded += () => {
				ObjectCatchController.GetInstance ().ReleaseObject ();
				//ThrowController.GetInstance().Run();
			};

			BallSwitchController.GetInstance ().OnSelected += ThrowController.GetInstance ().SetBall;
			ThrowController.GetInstance ().OnThrowed += () => {
				if(!StaticData.tutorial){
					BallSwitchController.GetInstance ().HideButtons ();
					SpendBall();

				}
				ObjectCatchController.GetInstance().TurnOnTrack();
			};
			ThrowController.GetInstance ().OnBallReturned += () => {
				ObjectCatchController.GetInstance().TurnOffTrack();
				if(!StaticData.tutorial){
					//BallSwitchController.GetInstance ().CheckEnabledBalls();
					BallSwitchController.GetInstance ().ShowButtons();
					if(IsBallEmpty()){
						Pause();
						GameViewController.GetInstance().SetActiveAddBallButton(true);
					}
				}
			};


		}
				
		public void Run(){
			Analytics.LogEvent (_define.gameActionKey, new Dictionary<string, object> () {
				{_define.gameScreenActionKey, true}	
			});
			Play ();
		}

		public static GameController GetInstance(){
			return _instance;
		}

		public void OnBackToMenu(){
			SceneManager.LoadScene ("Menu");
		}

		void OnBackClick(){
			OnBackToMenu ();
		}
			
		#region game lifecycle methods
		public void Play(){
			//Sounder.me.Refresh ();
			isRunning = true;
			imageCamera.Show ();
			gameCamera.Run();
			objectRoatator.Activate (true);

			ThrowController.GetInstance ().Run ();
			ObjectCatchController.GetInstance ().Run ();


			if (StaticData.tutorial) {
				TutorialManager.GetInstance ().ShowGamePart ();
				GameViewController.GetInstance ().HideGameInfo ();
			} else {
				GameViewController.GetInstance ().ShowGameInfo ();
				BallSwitchController.GetInstance ().Run ();
			}
			if(IsBallEmpty()){
				Pause();
				GameViewController.GetInstance().SetActiveAddBallButton(true);
			}else
				GameViewController.GetInstance().SetActiveAddBallButton(false);
		}

		public void Stop(){
			if (StaticData.firstRun)
				StaticData.firstRun = false;
			isRunning = false;
			objectRoatator.Activate (false);
			gameCamera.Stop ();
			imageCamera.Hide ();
			GameViewController.GetInstance ().HideGameInfo ();
		}

		public void Pause(){
			ObjectCatchController.GetInstance().Stop();
			BallSwitchController.GetInstance ().Stop ();
			ThrowController.GetInstance().Stop();
		}

		public void Replay(){
			Play ();
		}

		public void Resume(){
			ThrowController.GetInstance ().Run ();
			ObjectCatchController.GetInstance ().RunFromPause ();
			GameViewController.GetInstance().SetActiveAddBallButton(false);
		}
		/// <summary>
		/// Catchs the object.
		/// </summary>
		/// <param name="objectId">Object identifier.</param>
		void OnGameEnded(bool isCatched){
			Stop ();
			GameViewController.GetInstance().SetActiveAddBallButton(false);
			int _objectId = StaticData.playerData.gameInfo.currentCatId;
			if (isCatched) {
					
				if (StaticData.gameWinBannerCounter++ % 3 == 0)
					AdCernel.GetInstance ().ShowBanner (AdType.INTERSTITIAL);
			
				//save id in playerdata
				StaticData.playerInfo.currentXp += StaticData.CatParams.Where (x => x.id == _objectId).First ().xp;
				StaticData.playerInfo = UpdatePlayerInfo ();
				DailyQuestManager.GetInstance ().TryOpenDailyQuests ();
				//add cat to list
				if (!StaticData.playerData.playerCatsList.ContainsKey (_objectId)) {
					StaticData.playerData.totalCatsCount++;
					StaticData.playerData.playerCatsList.Add (_objectId, 1);
				} else {
					//remove old note
					{
						StaticData.playerData.totalCatsCount++;
						int _count = StaticData.playerData.playerCatsList [_objectId];
						StaticData.playerData.playerCatsList.Remove (_objectId);
						StaticData.playerData.playerCatsList.Add (_objectId, ++_count);
					}
				}
			}
			StaticData.SaveInfo ();
			GameViewController.GetInstance ().ShowWinLosePanel (isCatched);
			AchievmentManager.GetInstance ().CheckAchievmentsDelay (AchieveTimeDelay.GAME);
			//DailyQuestManager.GetInstance ().CheckDailyQuests ();
			SendInfoAboutGame (isCatched);
		}
		/// <summary>
		/// Loses the object.
		/// </summary>
		/// <param name="objectId">Object identifier.</param>
		void LoseObject(int objectId){
			Stop ();
			GameViewController.GetInstance ().ShowWinLosePanel (false);
			StaticData.SaveInfo ();
		}

		void SendInfoAboutGame(bool isCathed){
			PlayerData lastData = ObjectCatchController.GetInstance ().GetLastGameData ();


			Analytics.LogEvent (_define.gameInfoKey, new Dictionary<string,object> () {
				{ _define.currentCatKey, StaticData.playerData.gameInfo.currentCatId },
				{ _define.currentCatCatchedKey, isCathed },
				{ _define.playerTotalCatsCountKey, StaticData.playerData.totalCatsCount },
				{_define.playerSpendBallsKey, new Dictionary<BallType, int> {
						{ BallType.WOOL, lastData.playerBalls [BallType.WOOL] - StaticData.playerData.playerBalls [BallType.WOOL] }, 
						{ BallType.SILVER, lastData.playerBalls [BallType.SILVER] - StaticData.playerData.playerBalls [BallType.SILVER] },
						{ BallType.GOLD, lastData.playerBalls [BallType.GOLD] - StaticData.playerData.playerBalls [BallType.GOLD] }	
					}
				}, {_define.playerBallsKey, new Dictionary<BallType, int> {
						{ BallType.WOOL, StaticData.playerData.playerBalls [BallType.WOOL] }, 
						{ BallType.SILVER, StaticData.playerData.playerBalls [BallType.SILVER] },
						{ BallType.GOLD, StaticData.playerData.playerBalls [BallType.GOLD] }	
					}
				}, { _define.playerCatsCollectionKey, new Dictionary<string, object> {
						{ _define.playerCatsCollectionKindKey, StaticData.playerData.playerCatsList.Keys.ToArray () },
						{ _define.playerCatsCollectionCountKey, StaticData.playerData.playerCatsList.Values.ToArray () }
					}
				}
			});
		}
		#endregion

		void SpendBall(){
			if (!StaticData.tutorial) {
				StaticData.playerData.playerBalls [StaticData.playerData.gameInfo.currentBall]--;
				GameViewController.GetInstance ().UpdatePlayerBalls ();
			}
		}

		public bool IsBallEmpty(){
			if (!StaticData.tutorial) {
				if (StaticData.playerData.playerBalls[BallType.WOOL] <= 0 && StaticData.playerData.playerBalls[BallType.SILVER] <= 0 &&
					StaticData.playerData.playerBalls[BallType.GOLD] <= 0)
					return true;
			}
			return false;
		}

		#region UI methods
		internal void BackToMap(){
			Stop ();
			GameViewController.GetInstance ().ShowUIPanel ();
			CameraSceneController.GetInstance().State = GameState.MAP_VIEW;
		}
		#endregion

		public bool CheckState(){
			return isRunning;	
		}

		PlayerInfo UpdatePlayerInfo (){
			//check if xp more than xp needed for next level than increase level
				PlayerInfo mInfo = StaticData.playerInfo;

			if (mInfo.level == 1)
				mInfo.nextLevelXp = Mathf.RoundToInt (50 * 1.3f);
				//if level up
			CDebug.Log("next xp needed " + mInfo.nextLevelXp.ToString());
			if (mInfo.currentXp >= mInfo.nextLevelXp) {
					mInfo.level++;
					CDebug.Log ("increase level " + mInfo.level);
					mInfo.currentXp -= mInfo.nextLevelXp;
					mInfo.money += (mInfo.level*10);

				//if(mInfo.level == 1)
					//mInfo.nextLevelXp 

					//mInfo.nextLevelXp = 100 + mInfo.level + (mInfo.level + 1) * 13;
					
				if (mInfo.level < 5) {
					mInfo.nextLevelXp = Mathf.RoundToInt (mInfo.nextLevelXp * 1.3f);
				}else
					mInfo.nextLevelXp = Mathf.RoundToInt (mInfo.nextLevelXp * 1.1f);

					StaticData.levelUp = true;
					
				}
			return mInfo;
		}
	}
}