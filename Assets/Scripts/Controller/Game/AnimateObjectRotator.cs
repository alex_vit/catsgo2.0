﻿using UnityEngine;
using System.Collections;

public class AnimateObjectRotator : MonoBehaviour {
	[SerializeField]
	RectTransform animObject;
	[SerializeField]
	VRCamera visionCam;

	bool isRun;
	// Use this for initialization
	
	// Update is called once per frame
	void LateUpdate () {
		if (isRun) {
			animObject.localRotation = visionCam.GetRotation ();
		}		
	}

	public void Activate(bool active){
		isRun = active;
	}
}
