﻿using UnityEngine;
using System.Collections;
using System;

public class BallSwitchController : MonoBehaviour {
	static BallSwitchController _instance;
	[SerializeField]
	AnimatedBall[] pull;

	public int defaultIndex;

	public GameObject rightButton;
	public GameObject leftButton;

	int mCurrIndex;

	public Action<AnimatedBall> OnSelected;
	void Awake(){
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {
		SetDefault ();
		mCurrIndex = defaultIndex;
	}

	public static BallSwitchController GetInstance(){
		return _instance;
	}

	public void Run(){
		mCurrIndex = defaultIndex;
		ShowButtons ();
		SetDefault ();
	}

	public void Stop(){
		HideButtons ();
	}

	void SetDefault(){
		SelectBall (mCurrIndex);
	}

	public void SelectNext(){
		if (mCurrIndex < pull.Length-1) {
			Mathf.Clamp (++mCurrIndex, 0, pull.Length - 1);
			for (int i = 0; i < pull.Length; i++) {
				pull [i].gameObject.SetActive (false);
			}
			SelectBall (mCurrIndex);
		}
	}

	public void SelectPrev(){
		if (mCurrIndex > 0) {
			Mathf.Clamp (--mCurrIndex, 0, pull.Length - 1);
			for (int i = 0; i < pull.Length; i++) {
				pull [i].gameObject.SetActive (false);
			}
			SelectBall (mCurrIndex);
		}
	}

	void SelectBall(int index){
		CDebug.Log ("select balll");
		//CheckEnabledBalls();
		for (int i = 0; i < pull.Length; i++) {
			pull [i].gameObject.SetActive (false);
		}
		pull [index].gameObject.SetActive (true);
		//ThrowController.GetInstance ().SetBall (pull [index]);

		if (OnSelected != null)
			OnSelected (pull [index]);
	}

	public void CheckEnabledBalls(){
		if (mCurrIndex == 0) {
			leftButton.SetActive (false);
		} else if (mCurrIndex == pull.Length - 1) {
			rightButton.SetActive (false);
		} else {
			//check
			if (StaticData.playerData.playerBalls [pull [mCurrIndex - 1].balltype] <= 0)
				leftButton.SetActive (false);
			if (StaticData.playerData.playerBalls [pull [mCurrIndex + 1].balltype] <= 0)
				rightButton.SetActive (false);
		}
	}

	public void ShowButtons(){
		rightButton.SetActive (true);
		leftButton.SetActive (true);
	}

	public void HideButtons(){
		rightButton.SetActive (false);
		leftButton.SetActive (false);
	}
}
