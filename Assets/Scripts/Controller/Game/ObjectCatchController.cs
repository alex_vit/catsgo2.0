﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using Game;
using System.Linq;

public class ObjectCatchController : MonoBehaviour {
	static ObjectCatchController _instance;

	public Action OnObjectCatched;
	public Action OnObjectEscaped;
	public List<Vector2> spawnPostions;
	public Vector2 tutorialPosition;

	public Vector3 offset;

	[SerializeField]
	AnimatedCat catchObject;
	[SerializeField]
	BallDetector detectorObject;
	[SerializeField]
	AnimatedCircle catchCircle;

	WaitForSeconds mWait;
	bool isTrack;
	float _rad;
	float mCurrTimerTick;
	Vector3 ballPos, catPos;
	PlayerData mDataBeforeTheGame;

	void Awake(){
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
		catchObject.OnCatched += NotifyGameThatObjectCatched;
		catchObject.OnEscaped += NotifyGameThatObjectEscaped;
		mWait = new WaitForSeconds (_define.timeStep);
	}

	void Start(){
		ThrowController.GetInstance ().OnThrowed += () => { TurnOnTrack(); UnlightObject ();};
		ThrowController.GetInstance ().OnBallReturned += () => {
			if(isTrack)
			catchObject.Miss ();
		};
		BallSwitchController.GetInstance ().OnSelected += id => {
			UnlightObject ();
		};
	}

	void Update(){
		if (isTrack) {
			AnimatedBall _currBall = ThrowController.GetInstance ().GetCurrentBall ();
			if (StaticData.playerData.gameInfo.currentBall == BallType.GOLD) {
				CancelInvoke ();
				Invoke ("CatchObject", 0.5f);
				isTrack = false;
			} else {
				ballPos = _currBall.GetPosition ();
				catPos = catchObject.GetPosition ();
				if (!StaticData.tutorial)
					_rad = StaticData.BallParams [_currBall.balltype].catchRadius;
				else
					_rad = StaticData.tutorialBallRad;
				{
					if (Mathf.Abs (ballPos.x - catPos.x) <= _rad && Mathf.Abs (ballPos.y - catPos.y) <= _rad) {
						CatchObject ();
					}
				}
			}
		}
	}
	/// <summary>
	/// Gets the instance.
	/// </summary>
	/// <returns>The instance.</returns>
	public static ObjectCatchController GetInstance(){
		return _instance;
	} 

	public void Run(){
		mDataBeforeTheGame.gameInfo = new GameInfo ();
		mDataBeforeTheGame.gameInfo.currentCatId = StaticData.playerData.gameInfo.currentCatId;
		mDataBeforeTheGame.playerBalls = new Dictionary<BallType, int> ();
		mDataBeforeTheGame.playerBalls.Add (BallType.WOOL, StaticData.playerData.playerBalls [BallType.WOOL]);
		mDataBeforeTheGame.playerBalls.Add (BallType.SILVER, StaticData.playerData.playerBalls [BallType.SILVER]);
		mDataBeforeTheGame.playerBalls.Add (BallType.GOLD, StaticData.playerData.playerBalls [BallType.GOLD]);

		//BallsPull.GetInstance ().Run ();
		SetCatchObject ();
		detectorObject.Activate (true);
		if (!StaticData.tutorial)
			StartCoroutine (Timer (StaticData.CatParams.Where(x=> x.id == StaticData.playerData.gameInfo.currentCatId).First().catchTime));
		TurnOnTrack();
	}

	public void RunFromPause(){
		detectorObject.Activate (true);
		if (!StaticData.tutorial) {
			StartCoroutine (Timer (mCurrTimerTick));
		}
		TurnOnTrack ();
	}

	public void Stop(){
		StopAllCoroutines ();
		detectorObject.Activate (false);
	}

	public PlayerData GetLastGameData(){
		return mDataBeforeTheGame;
	}

	void SetCatchObject(){
		Vector2 _spawnPos = spawnPostions [UnityEngine.Random.Range (0, spawnPostions.Count)];

		if (!StaticData.tutorial)
			catchObject.SetPosition (_spawnPos);
		else
			catchObject.SetPosition (tutorialPosition);
		detectorObject.SetPosition (catchObject.transform.position);
		catchObject.Init (StaticData.playerData.gameInfo.currentCatId);
	}

	public void HighLightObject(){
		catchCircle.Show ();
	}

	public void UnlightObject(){
		catchCircle.Hide ();
	}
	/// <summary>
	/// Check if catch object or not
	/// </summary>
	public void CatchObject(){
		isTrack = false;
		UnlightObject ();
		ThrowController.GetInstance ().SetTrackable (false);
		AnimatedBall _currBall = ThrowController.GetInstance ().GetCurrentBall ();

		if (StaticData.BallParams [_currBall.balltype].isEscapeAble && !StaticData.tutorial) {
			float _success =  1f - _currBall.GetStickness () /
				(_currBall.GetStickness () + StaticData.CatParams.Where(x=> x.id == StaticData.playerData.gameInfo.currentCatId).First().agility);
			float _res = UnityEngine.Random.Range (0f, 1f);
			CDebug.Log (_res + " " + _success);
			_currBall.Hide ();
			if(_res >= _success){
				//we catch this cat	
				StopAllCoroutines();
				catchObject.Catch();
				detectorObject.Activate (false);
			}else{
				//cat released from ball
				catchObject.Laugh();
			}
		} else {
			StopAllCoroutines();
			catchObject.Catch();
			detectorObject.Activate (false);
		}
	}

	public void ReleaseObject(){
		catchObject.Escape ();
		detectorObject.Activate (false);
	}

	void NotifyGameThatObjectCatched(){
		DailyQuestManager.GetInstance ().ReceiveMsg (PlayerAction.CATCH_CAT);
		if (!StaticData.tutorial && StaticData.playerInfo.level >= 5) {
			AchievmentManager.GetInstance ().ReceiveMsg (PlayerAction.CATCH_CAT);
		}
		if (GameController.GetInstance ().CheckState ()) {
			if (OnObjectCatched != null)
				OnObjectCatched ();
		}
	}

	void NotifyGameThatObjectEscaped(){
		if (GameController.GetInstance ().CheckState ()) {
			CDebug.Log ("object escaped");
			if (OnObjectEscaped != null)
				OnObjectEscaped ();
		}
	}

	void MissBall(){
		UnlightObject ();
		//StaticData.playerData.playerBalls [StaticData.playerData.gameInfo.currentBall] = Mathf.Clamp (--StaticData.playerData.playerBalls [StaticData.playerData.gameInfo.currentBall], 0, 65536); 
	}

	public void TurnOnTrack(){
			if (!isTrack)
				isTrack = true;
	}

	public void TurnOffTrack(){
		if (isTrack)
			isTrack = false;
	}

	IEnumerator Timer(float time){
		float _time = time;
		while (true) {
			if (_time <= 1) {
				//catchObject.Escape ();
				//detectorObject.Activate (false);
				ReleaseObject();
				break;
			}

			StaticData.playerData.gameInfo.time = --_time;
			mCurrTimerTick = _time;
			GameViewController.GetInstance ().UpdatePlayerTimer ();
			yield return mWait;
		}
	}
}

