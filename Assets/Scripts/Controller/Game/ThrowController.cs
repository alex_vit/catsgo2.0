﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;
using Game;
/// <summary>
/// Throw controller. it describe logic for ball throwing
/// </summary>
public class ThrowController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler{
	static ThrowController _instance;
	public float minDistance;
	[SerializeField]
	AnimatedBall ball;
	[SerializeField]
	AnimatedBall defaultBall;

	//public Text label;

	Vector2 mDir;
	Vector2 mBeginPoint;
	Vector2 mEndPoint;

	Action OnCatched;
	Action OnMissed;

	public Action OnThrowed;
	public Action OnBallReturned;

	public Vector3 lowBounds;
	public Vector3 highBounds;
	Vector3 currBallPos;

	bool isTracking;

	// Use this for initialization
	void Awake(){
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
	}

	void Update(){
		TrackBallPosition ();
	}

	public static ThrowController GetInstance(){
		return _instance;
	}

	public void Run(){
		isTracking = false;
		SetDefault ();
	}

	public void Stop(){
		isTracking = false;
		HideCurrBall ();
	}

	void SetDefault(){
		//label.text = "set default";
		SetBall (defaultBall);
	}

	public AnimatedBall GetCurrentBall(){
		return ball;
	}

	public void SetBall(AnimatedBall newBall){
		this.ball = newBall;
		ball.OnReady += () => {
			SetDefaultThrowState();
		};
		//ball.OnFallen += ObjectCatchController.GetInstance ().MissBall;
		ball.OnCatched += ObjectCatchController.GetInstance ().CatchObject;
		ball.SetDefault ();
		StaticData.playerData.gameInfo.currentBall = ball.balltype;
		CDebug.Log (StaticData.playerData.gameInfo.currentBall);
		GameViewController.GetInstance ().UpdatePlayerBalls ();
		//label.text = "setted default";
	}

	public void ShowCurrBall(){
		ball.gameObject.SetActive (true);
		ball.SetDefault ();
	}

	public void HideCurrBall(){
		ball.gameObject.SetActive (false);
	}

	void SetDefaultThrowState(){
		//if (!StaticData.tutorial) {
			//GameViewController.GetInstance ().ShowBallSelectButtons (true);
		//}
		//GameViewController.GetInstance ().UpdatePlayerBalls ();
		if (OnBallReturned != null)
			OnBallReturned ();
	}

	Vector3 CalculateDir(){
		Vector2 dirNorm = (mEndPoint - mBeginPoint).normalized;
		return new Vector3 (dirNorm.x, dirNorm.y, dirNorm.y);
	}

	void TrackBallPosition(){
		if (isTracking) {
			currBallPos = ball.GetTransform ().localPosition;
			if ((currBallPos.x >= highBounds.x || currBallPos.x <= lowBounds.x) || (currBallPos.y >= highBounds.y || currBallPos.y <= lowBounds.y) ||
			    (currBallPos.z >= highBounds.z || currBallPos.z <= lowBounds.z)) {
				ball.SetDefault ();
				isTracking = false;
			}
		}
	}

	public void SetTrackable(bool isTrack){
		isTracking = isTrack;
	}

	#region IPointerDownHandler implementation
	void IPointerDownHandler.OnPointerDown (PointerEventData eventData)
	{
		if (ball.GetState () == BallState.READY) {
			//label.text = "OnPointerDown";
			mBeginPoint = eventData.position;
			ObjectCatchController.GetInstance ().HighLightObject ();
		}
	}
	#endregion

	#region IPointerUpHandler implementation
	void IPointerUpHandler.OnPointerUp (PointerEventData eventData)
	{
		if (StaticData.playerData.playerBalls [StaticData.playerData.gameInfo.currentBall] > 0 || StaticData.tutorial) {
			//label.text = "Balls count mre than 0";
			if (ball.GetState () == BallState.READY && mBeginPoint != Vector2.zero) {
				//label.text = "balls ready and pos != 0";
				mEndPoint = eventData.position;

				if (Vector2.Distance (mBeginPoint, mEndPoint) >= minDistance) {
					//label.text = "ThrowBall";
					float _force = Vector2.SqrMagnitude (mEndPoint - mBeginPoint);
					ball.Throw (CalculateDir () * _force / 200);

					mBeginPoint = Vector2.zero;
					if (OnThrowed != null)
						OnThrowed ();
					SetTrackable (true);
					DailyQuestManager.GetInstance ().ReceiveMsg (PlayerAction.SPEND_BALL);
					if (!StaticData.tutorial && StaticData.playerInfo.level >= 5) {
						AchievmentManager.GetInstance ().ReceiveMsg (PlayerAction.SPEND_BALL);

						AchievmentManager.GetInstance ().ReceiveMsg (PlayerAction.CATCH_ATTEMPT);
						if (ball.balltype == BallType.GOLD)
							AchievmentManager.GetInstance ().ReceiveMsg (PlayerAction.SPEND_GOLDEN_BALL);
						if (ball.balltype == BallType.SILVER)
							AchievmentManager.GetInstance ().ReceiveMsg (PlayerAction.SPEND_SILVER_BALL);
					}
				} else
					mBeginPoint = Vector2.zero;
			}
		}
	}
	#endregion
}
