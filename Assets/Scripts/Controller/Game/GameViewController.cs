﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System;
using System.IO;
using Advertise;

namespace Game{
public class GameViewController : MonoBehaviour {
	static GameViewController _instance;
		[Header("UI elements")]
		[Header("Other UI")]
		[SerializeField]
		Text gameTimeText;
		[SerializeField]
		Text ballsCountText;
		[SerializeField]
		WinLosePanel winLoseInfo;
		[SerializeField]
		LevelUpPanel lvlUpInfo;
		[SerializeField]
		GameObject addBallsButton;
		[SerializeField]
		TextTimer addBallTimer;
		[SerializeField]
		TakeAchieveWindow achieveWindow;

		[Header("Panels")]
		[SerializeField]
		GameObject uiGamePanel;
		[SerializeField]
		GameObject winLosePanel;
		[SerializeField]
		GameObject lvlUpPanel;

		[Header("Dialog")]
		[SerializeField]
		Dialog achiveView;

		public Action OnAddBallTimerEnded;

		Dialog mActiveDialog; //reference for active dialog window
		GameObject mActivePanel;

		bool isMakingScreenShot;

		int mAdvertiseCounter = 0;
		// Use this for initialization
		void Awake(){
			if (_instance == null)
				_instance = this;
			else
				Destroy (gameObject);
		}

		void OnPostRender(){
			if (isMakingScreenShot) {
				isMakingScreenShot = false;

				Texture2D screen = new Texture2D (720, 1280, TextureFormat.RGB24, false);
				screen.ReadPixels(new Rect(0,0,Screen.width,Screen.height), 0,0);
				screen.Apply ();
				string path = Application.dataPath + "/MyImage.png";
				File.WriteAllBytes(path, screen.EncodeToPNG());
				CDebug.Log ("saved");
			}
		}


		public static GameViewController GetInstance(){
			return _instance;
		}

		public void ShowNewAchieveDialog(int id){
			GameController.GetInstance ().Pause ();
			achieveWindow.Show (id);
			(achiveView as AlertDialog).OkAction += GameController.GetInstance ().Resume;
			ShowDialog (achiveView);
		}

		void ShowDialog(Dialog window){
			HideAll ();
			mActiveDialog = window;
			mActiveDialog.Show ();
		}

		public void HideActiveDialog(){
			if (mActiveDialog != null)
				mActiveDialog.Hide ();
		}

		public void HideAll(){
			HideActiveDialog ();
			achiveView.Hide ();
		}

		#region button handlers
		public void OnUIBackButtonClick(){
			GameController.GetInstance ().BackToMap ();
		}

		public void OnGameBackButtonClick(){
			winLosePanel.SetActive (false);
			if (StaticData.levelUp) {
				ShowLevelUpPanel ();
				return;
			}
			if (StaticData.tutorial) {
				TutorialManager.GetInstance ().ShowFinalPart ();
				return;
			}
			GameController.GetInstance ().BackToMap ();
		}

		public void OnCaptureButtonClick(){
			//Texture2D screen = new Texture2D (720, 1280, TextureFormat.RGB24, false);
			//screen.ReadPixels(new Rect(0,0,Screen.width,Screen.height), 0,0);
			//screen.Apply ();
			//string path = Application.dataPath + "/MyImage.png";
			//File.WriteAllBytes(path, screen.EncodeToPNG());
			//GeneralSharing.GetInstance ().SetImage (screen);
			//GeneralSharing.GetInstance ().OnShareTextWithImage (Localizator.GetTextValue(_define.shareText));
			StartCoroutine(CaptureScreenShot());
		}


		#endregion

		#region work with panel
		public void ShowWinLosePanel(bool isGameWon){
			//set info about item
			winLoseInfo.Show(isGameWon);
			ShowPanel (winLosePanel);
		}

		public void ShowLevelUpPanel(){
			lvlUpInfo.Show ();
			ShowPanel (lvlUpPanel);
		}

		public void ShowUIPanel(){
			ShowPanel (uiGamePanel);
		}

		public void ShowPanel(GameObject panel){
			if (mActivePanel != null)
				mActivePanel.SetActive (false);
			mActivePanel = panel;
			mActivePanel.SetActive (true);
		}
		#endregion

		public void UpdatePlayerTimer(){
			gameTimeText.text = StaticData.playerData.gameInfo.time.ToString();
		}

		public void UpdatePlayerBalls(){
			if(!StaticData.tutorial)
				ballsCountText.text = StaticData.playerData.playerBalls [StaticData.playerData.gameInfo.currentBall].ToString ();
		}

		public void ShowGameInfo(){
			gameTimeText.gameObject.SetActive (true);
			ballsCountText.gameObject.SetActive (true);
		}

		public void HideGameInfo(){
			gameTimeText.gameObject.SetActive (false);
			ballsCountText.gameObject.SetActive (false);
		}

		public void ClickAddBallButton(){
			AdCernel.GetInstance ().ShowVideo (AdType.REWARDED_VIDEO, ShopController.GetInstance().GetFreeBalls, null, null);
			//ShopController.GetInstance ().MakePurchase (_define.tenGoldenBallId);
		}

		public void SetActiveAddBallButton(bool isActive){
			addBallsButton.SetActive (isActive);
			if (isActive) {
				HideGameInfo ();
				//start timer and sign in of time event to hide button, timer and game end;
				addBallTimer.Run();
				addBallTimer.TimeOut += () => {
					SetActiveAddBallButton (false);
					if (OnAddBallTimerEnded != null)
						OnAddBallTimerEnded ();
				};
			} else {
				ShowGameInfo ();
				if(addBallTimer.IsWork())
					addBallTimer.Stop ();
			}
		}

		IEnumerator CaptureScreenShot(){
			while (true) {
				yield return new WaitForEndOfFrame ();
				Texture2D screen = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, false);
				screen.ReadPixels(new Rect(0,0,Screen.width,Screen.height), 0,0);
				screen.Apply ();
				//string path = Application.dataPath + "/MyImage.png";
				//File.WriteAllBytes(path, screen.EncodeToPNG());
				GeneralSharing.GetInstance().SetImage(screen);
				GeneralSharing.GetInstance ().OnShareTextWithImage (Localizator.GetTextValue(_define.shareText));
				CDebug.Log ("saved");
				break;
			}
		}
	}
}
