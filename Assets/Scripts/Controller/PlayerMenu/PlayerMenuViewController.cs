﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using Advertise;
using System.Collections.Generic;

namespace CatMenu{
	public class PlayerMenuViewController : MonoBehaviour {
		static PlayerMenuViewController _instance;

		[Header("UI elements")]
		[Header("Panels")]
		public GameObject menuPanel;
		public GameObject collectionPanel;
		public GameObject storePanel;
		public GameObject achievePanel;
		public GameObject dailyQuestPanel;
		public GameObject dialogPanel;
		[Header("Dialog")]
		public Dialog comingSoonDialog;
		public Dialog enoughMoneyDialog;
		public Dialog showAdDialog;
		public Dialog catDetailDialog;
		[Header("Other UI")]
		[SerializeField]
		Text playerMoneyText;

		[SerializeField]
		Text allCatsCountInfoLabel;
		[SerializeField]
		PlayerBallInfoBar ballInfoBar;
		[SerializeField]
		CatDetailInfoWindow catDetailInfo;
		[SerializeField]
		AchieveDetailWindow achDetailInfo;
		[SerializeField]
		GameObject catCollectionBackButton;
		[SerializeField]
		SwitchButton soundButton;

		GameObject mActivePanel;
		Dialog mActiveDialog;
		// Use this for initialization
		void Awake () {
			if (_instance == null)
				_instance = this;
			else
				Destroy (gameObject);
		}

		void Start(){
			ShowMenuPanel ();
			UpdatePlayerInfo ();
			//freeMoneyTimer.TimeOut += () => {
				//freeMoneyButton.TurnOn ();
			//};
			CheckSound();
			soundButton.OnSwitchState += PlayerMenuController.GetInstance ().TurnOnOffSound;
		}

		public static PlayerMenuViewController GetInstance(){
			return _instance;
		}

		public void BackButtonClick(){
			PlayerMenuController.GetInstance ().BackToMap ();
		}

		public void GetFreeCoinsButtonClick(){
			if (FreeCoinsController.GetInstance ().IsAvailable ()) {
				if (!StaticData.isAdvWindowShowed) {
					(showAdDialog as AlertDialog).OkAction += () => {
						StaticData.isAdvWindowShowed = true;
						AdCernel.GetInstance ().ShowVideo (AdType.REWARDED_VIDEO, SuccessFreeCoinsClick, null, null);
					};
					ShowDialog (showAdDialog);
				}else
					AdCernel.GetInstance ().ShowVideo (AdType.REWARDED_VIDEO, SuccessFreeCoinsClick, null, null);
			}
		}

		public void StoreButtonClick(){
			ShowStorePanel ();
		}

		public void CollectionButtonClick(){
			ShowCollectionPanel ();
		}

		public void AchieveButtonClick(){
			ShowAchivePanel ();
		}

		public void OnVkGroupButtonClick(){
			SocialController.GetInstance ().GoToVkGroup ();
		}

		public void OnDailyQuestButtonClick(){
			ShowDailyQuestPanel ();
		}

		void SuccessFreeCoinsClick(){
			Analytics.LogEvent (_define.advWatchInfoKey, new Dictionary<string, object>(){{_define.advWatchInfoKey, true}});
			ShopController.GetInstance ().GetFreeCoins ();
			UpdatePlayerInfo ();
			//SetFreeMoneyTimer (_define.timerForFreeMoney);
			FreeCoinsController.GetInstance().StartTimer();
		}



		#region work with panels	
		/// <summary>
		/// Shows the menu panel.
		/// </summary>
		public  void ShowMenuPanel(){
			Analytics.LogEvent (_define.gameActionKey, new Dictionary<string, object> () {
				{_define.playerMenuActionKey, true}	
			});
			ShowPanel (menuPanel);
		}
		/// <summary>
		/// Shows the cat store panel.
		/// </summary>
		internal void ShowStorePanel(){
			CDebug.Log ("show store");
			Analytics.LogEvent (_define.gameActionKey, new Dictionary<string, object> () {
				{_define.playerShopActionKey, true}	
			});
			ShowPanel (storePanel);
			FreeCoinsController.GetInstance().State = GameState.PLAYER_MENU;

			Analytics.LogEvent(_define.playerInfoKey, new Dictionary<string, object>(){
				{_define.playerMoneyKey, StaticData.playerInfo.money},
				{_define.ballsCountKey, new Dictionary<BallType, int>(){
						{BallType.WOOL, StaticData.playerData.playerBalls[BallType.WOOL]},
						{BallType.SILVER, StaticData.playerData.playerBalls[BallType.SILVER]},
						{BallType.GOLD, StaticData.playerData.playerBalls[BallType.GOLD]}
					}
				}
			});
		}

		internal  void ShowCollectionPanel(){
			Analytics.LogEvent (_define.gameActionKey, new Dictionary<string, object> () {
				{_define.catCollectionActionKey, true}	
			});
			ShowPanel (collectionPanel);
			allCatsCountInfoLabel.text = StaticData.playerData.totalCatsCount.ToString();
		}

		internal void ShowAchivePanel(){
			if(StaticData.playerInfo.level < 5)
				ShowComingSoonDialog (_define.achieveNotAvailable);
			else
				ShowPanel(achievePanel);
		}

		internal void ShowDailyQuestPanel(){
			if (StaticData.playerInfo.level >= 3 && DailyQuestManager.GetInstance ().isOpened) {
				DailyQuestManager.GetInstance ().SaveQuests ();
				dailyQuestPanel.GetComponent<DailyQuestPanel> ().Init ();
				ShowPanel (dailyQuestPanel);
			} else
				ShowComingSoonDialog (_define.dailyQuestNotAvailable);
		}

		private void ShowPanel(GameObject panel){
			if (mActivePanel != null) {
				mActivePanel.SetActive (false);
					
				if (mActivePanel == storePanel) {
					FreeCoinsController.GetInstance ().State = GameState.LOADING;
					Analytics.LogEvent(_define.playerInfoKey, new Dictionary<string, object>(){
						{_define.playerMoneyKey, StaticData.playerInfo.money},
						{_define.ballsCountKey, new Dictionary<BallType, int>(){
								{BallType.WOOL, StaticData.playerData.playerBalls[BallType.WOOL]},
								{BallType.SILVER, StaticData.playerData.playerBalls[BallType.SILVER]},
								{BallType.GOLD, StaticData.playerData.playerBalls[BallType.GOLD]}
							}
						}
					});
				}
			}
			mActivePanel = panel;
			mActivePanel.SetActive (true);
		}
		#endregion

		#region work with dialogs
		public void ShowNotEnoughMoneyDialog(){
			(enoughMoneyDialog as AlertDialog).OkAction += () => {
				ShopController.GetInstance ().MakePurchase (_define.smallCoinsPackId);
			};
			ShowDialog (enoughMoneyDialog);
		}

		public void ShowCatDetailWindow(int id){
			catCollectionBackButton.SetActive (false);
			catDetailInfo.Show (id);
			ShowDialog (catDetailDialog);
		}

		public void ShowAchiveDetailWindow(int id){
			achDetailInfo.Show (id);
			ShowDialog (achDetailInfo.GetComponent<Dialog>());
		}

		public void SetCatCollectionBackButtonActive(){
			catCollectionBackButton.SetActive (true);
		}

		public void ShowComingSoonDialog(string tag){
			(comingSoonDialog as AlertDialog).SetText (tag);			
			ShowDialog (comingSoonDialog);
		}

		void ShowDialog(Dialog dialog){
			
			if (mActiveDialog != null)
				mActiveDialog.Hide ();
			mActiveDialog = dialog;
			//ShowPanel (dialogPanel);
			mActiveDialog.Show ();
		}
		#endregion

		public void UpdatePlayerInfo(){
			playerMoneyText.text = StaticData.playerInfo.money.ToString ();
			ballInfoBar.UpdateInfo ();
		}

		void CheckSound(){
			soundButton.SwitchState (StaticData.Sound == 1f);
		}
	}
}
