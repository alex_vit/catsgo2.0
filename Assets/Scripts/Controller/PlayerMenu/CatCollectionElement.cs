﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using CatMenu;

public class CatCollectionElement : MonoBehaviour {
	public int catID;
	Image mImage;
	Button mButton;
	ColoredSwitchText mColored;
	[SerializeField]
	Text catName;
	[SerializeField]
	Text catsCount;

	bool isOpened = true;
	// Use this for initialization
	void Awake () {
		mImage = GetComponent<Image> ();
		mButton = GetComponent<Button> ();
		mColored = catName.GetComponent<ColoredSwitchText> ();
		mButton.onClick.AddListener (Click);
	}

	#region show-hide methods
	public void Show(){
		mImage = GetComponent<Image> ();
		mButton = GetComponent<Button> ();
		mColored = catName.GetComponent<ColoredSwitchText> ();
		mButton.onClick.AddListener (Click);

		CatInfo _info = StaticData.CatParams.Where (x => x.id == catID).First ();

		if (StaticData.playerData.playerCatsList.ContainsKey (catID)) {
			isOpened = true;
			mImage.sprite = StaticData.CatCollectionImages [catID];
			//catsCount.text = StaticData.playerData.playerCatsList [catID].ToString ();
			mColored.SetActive(true);
			catName.text = Localizator.GetCatName(_info.id);
			SetCount();
			mButton.interactable = true;
		} else {
			mImage.sprite = StaticData.CatCollectionEmptyImages [catID];
			mButton.interactable = false;
			mColored.SetActive (false);
			catName.text = Localizator.GetCatName(0);
			isOpened = false;
		}
	}
	#endregion

	void Click(){
		if (isOpened)
			PlayerMenuViewController.GetInstance ().ShowCatDetailWindow (catID);
	}

	void SetCount(){
		int _count = StaticData.playerData.playerCatsList [catID];
		int _1kPrefix = _count / 1000;

		catsCount.text = (_1kPrefix == 0 ? _count.ToString () : (_1kPrefix.ToString () + "k"));
	}
}
