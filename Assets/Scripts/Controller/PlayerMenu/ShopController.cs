﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wazzapps.Purchasing;
using CatMenu;
using System;
using Menu;
using Advertise;
using Game;

public class ShopController : MonoBehaviour {
	static ShopController _instance;

	// Use this for initialization
	void Awake () {
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
	}

	public static ShopController GetInstance(){
		return _instance;
	}


	public void MakePurchase(string id){
		PurchaseInfo _info = StaticData.PurchaseItemInfo [id];
		//if it's coins we try spend player coins
		if (_info.GetInfo ().costType == CostType.COINS) {
			if (StaticData.playerInfo.money >= _info.GetInfo ().cost) {
				StaticData.playerInfo.money -= _info.GetInfo ().cost;

				HandleSuccessPurchase (id);
			} else {
				PlayerMenuViewController.GetInstance ().ShowNotEnoughMoneyDialog ();
			}
		}
		//else try spend real money
		else if (_info.GetInfo ().costType == CostType.MONEY) {
			Purchaser.GetInsance ().BuyProductID (id);
		} else if (_info.GetInfo ().costType == CostType.ACTION) {
			PlayerMenuViewController.GetInstance ().GetFreeCoinsButtonClick ();
		}
	}

	public void HandleSuccessPurchase(string id){
		PurchaseInfo _info = StaticData.PurchaseItemInfo [id] as PurchaseInfo;
		CDebug.Log (_info.GetInfo ().cost);

		switch (_info.GetInfo ().type) {
			case PurchaseType.BALL:
			HandleBallPurchase (_info as BallPurchaseInfo);
				break;
			case PurchaseType.COINS:
			HandleCoinPurchase (_info as MoneyPurchaseInfo);	
				break;
			case PurchaseType.ACTION:
			HandleActionPurchase (_info as ActionPurchaseInfo);
				break;
		}
		if(PlayerMenuViewController.GetInstance() != null)
			PlayerMenuViewController.GetInstance ().UpdatePlayerInfo ();

		if (_info.GetInfo ().costType == CostType.MONEY) {
			Analytics.LogEvent (_define.purchaseSucKey, new Dictionary<string, object> () {
				{ _define.purchaseCurrencyKey, _info.GetInfo ().costType },
				{ _define.purchaseCurrencyISOKey, Purchaser.GetInsance ().GetProductLocalizedPriceByID (id) },
				{ _define.itemTypeKey, _info.GetInfo ().type },
				{ id, true }
			});
		} else {
			Analytics.LogEvent (_define.purchaseSucKey, new Dictionary<string, object> () {
				{ _define.purchaseCurrencyKey, _info.GetInfo ().costType },
				{ _define.itemTypeKey, _info.GetInfo ().type },
				{ id, true }
			});
		}
	}

	public void HandleFailedPurchase(string id){
		PurchaseInfo _info = StaticData.PurchaseItemInfo [id] as PurchaseInfo;

		Analytics.LogEvent (_define.purchaseFailKey, new Dictionary<string, object> () {
			{ _define.purchaseCurrencyKey, _info.GetInfo().costType},
			{ _define.itemTypeKey, _info.GetInfo().type},
			{ id, true}
		});
	}

	public void GetFreeCoins(){
		StaticData.playerInfo.money += _define.freeMoneyReward;
		StaticData.SaveInfo ();
	}

	public void GetFreeBalls(){
		StaticData.playerData.playerBalls [BallType.WOOL] += 5;
		if (GameController.GetInstance () != null) {
			GameController.GetInstance ().Resume ();
		}
		StaticData.SaveInfo();
	}

	#region handle concrete purchase types
	void HandleBallPurchase(BallPurchaseInfo info){
		CDebug.Log("purchase ball " + info.GetInfo().currBallType + " " + info.GetInfo().reward);
		StaticData.playerData.playerBalls [info.GetInfo ().currBallType] += info.GetInfo ().reward;

		StaticData.SaveInfo ();
	}

	void HandleCoinPurchase(MoneyPurchaseInfo info){
		StaticData.playerInfo.money += info.GetInfo ().reward;
		StaticData.SaveInfo ();
	}

	void HandleActionPurchase(ActionPurchaseInfo info){
		CDebug.Log ("handle action");
		if (MenuViewController.GetInstance () != null) {
			MenuViewController.GetInstance ().SetTurnOffAdButtonInActive ();
		}
		info.DoAction ();
		StaticData.SaveInfo ();
	}
	#endregion
}
