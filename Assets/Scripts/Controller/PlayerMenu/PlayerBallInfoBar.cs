﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class PlayerBallInfoBar : MonoBehaviour {
	[Header("wool ball")]
	[SerializeField]
	Text woolBallValue;

	[Header("silver ball")]
	[SerializeField]
	Text silverBallValue;

	[Header("gold ball")]
	[SerializeField]
	Text goldenBallValue;

	public void UpdateInfo(){
		woolBallValue.text = StaticData.playerData.playerBalls [BallType.WOOL].ToString();
		silverBallValue.text = StaticData.playerData.playerBalls [BallType.SILVER].ToString();
		goldenBallValue.text = StaticData.playerData.playerBalls [BallType.GOLD].ToString();
	}
}
