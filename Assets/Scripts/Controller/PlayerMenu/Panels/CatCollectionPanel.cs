﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CatCollectionPanel : MonoBehaviour {
	[SerializeField]
	CatCollectionElement[] playerCats;
	// Use this for initialization
	void OnEnable(){
		for (int i = 0; i < playerCats.Length; i++) {
			playerCats [i].Show ();
		}
	}
}
