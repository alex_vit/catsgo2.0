﻿using UnityEngine;
using System.Collections;

public class AchivePanel : MonoBehaviour {
	[SerializeField]
	AchievementElement[] playerAchievs;
	// Use this for initialization
	void OnEnable(){
		for (int i = 0; i < playerAchievs.Length; i++) {
			playerAchievs [i].Show ();
		}
	}
}
