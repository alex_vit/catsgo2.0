﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public class AchieveDetailWindow : MonoBehaviour {
	[SerializeField]
	Text achNameText;
	[SerializeField]
	Image achImage;
	[SerializeField]
	ImageSlider achProgress;
	[SerializeField]
	Text achDescrText;
	[SerializeField]
	Text progressText;

	public void Show(int id){
		achNameText.text = Localizator.GetAchiveName (id);
		achImage.sprite = StaticData.AchievementImages [id];
		achProgress.SetFillColor(StaticData.AchieveColors[id]);

		if (CheckIsCompleted (id)) {
			achProgress.SetValue (1f);
			progressText.text = Localizator.GetTextValue (_define.achieveProgressCompletedText);
		}
		else{
			Achieve _ach = AchievmentManager.GetInstance ().GetIncompletedAchievs ().Where (x => x.id == id).First ();
			achProgress.SetValue (_ach.GetProgress ());
			progressText.text = _ach.currCount.ToString () + "/" + _ach.necessCount.ToString ();
		}
		achDescrText.text = Localizator.GetAchiveText(id);
	}

	bool CheckIsCompleted(int id){
		return AchievmentManager.GetInstance().GetIncompletedAchievs().Where(x=> x.id == id).Count() == 0;
	}
}
