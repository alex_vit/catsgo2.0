﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public class CatDetailInfoWindow : MonoBehaviour {
	[SerializeField]
	Text catNameText;
	[SerializeField]
	Image catImage;
	[SerializeField]
	Text rarityText;
	[SerializeField]
	Text quanityText;

	public void Show(int catId){
		Rarity _rare = StaticData.CatParams.Where (x => x.id == catId).First ().rarity;
		catNameText.text = Localizator.GetCatName (catId);
		catImage.sprite = StaticData.CatCollectionImages [catId];
		rarityText.text = Localizator.GetRarityText(_rare);
		quanityText.text = StaticData.playerData.playerCatsList [catId].ToString();
	}
}
