﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace CatMenu{
	public class PlayerMenuController : MonoBehaviour {
		static PlayerMenuController _instance;
		//public CatCollectionElement[] playerCats;
		// Use this for initialization
		void Awake () {
			if (_instance == null)
				_instance = this;
			else
				Destroy (gameObject);
		}
		
		public static PlayerMenuController GetInstance(){
			return _instance;	
		}

		internal void BackToMap(){
			SceneManager.LoadScene ("Map");
		}

		internal void TurnOnOffSound(bool enable){
			StaticData.Sound = (enable ? 1f :  0f);
			CDebug.Log("sound " + StaticData.Sound);
		}
	}
}
