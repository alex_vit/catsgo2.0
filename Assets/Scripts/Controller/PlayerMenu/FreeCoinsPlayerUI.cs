﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FreeCoinsPlayerUI : MonoBehaviour {
	static FreeCoinsPlayerUI _instance;

	[SerializeField]
	FreeMoneyButton playerFreeMoneyButton;
	[SerializeField]
	ImageTimer playerFreeMoneyTimer;
	[SerializeField]
	Image freeCoinsField;
	[SerializeField]
	Text freeCoinsTitle;
	[Header("Timer UI")]
	[SerializeField]
	Image timerImage;
	[SerializeField]
	Text timerText;
	// Use this for initialization
	void Awake () {
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
	}

	public static FreeCoinsPlayerUI GetInstance(){
		return _instance;
	}

	public void Activate(){
		playerFreeMoneyButton.TurnOn ();
		freeCoinsField.sprite = StaticData.UIImages ["free_field_light"];
		playerFreeMoneyTimer.gameObject.SetActive (false);
		freeCoinsTitle.gameObject.SetActive (false);
	}

	public void Desactivate(){
		playerFreeMoneyButton.TurnOff ();
		freeCoinsField.sprite = StaticData.UIImages ["free_field_dark"];
		playerFreeMoneyTimer.gameObject.SetActive (true);
		freeCoinsTitle.gameObject.SetActive (true);
	}

	public void UpdateInfo(int currMin, int currSec, float fillRate){
		timerText.text = currMin.ToString () + "m";
		freeCoinsTitle.gameObject.SetActive (true);
		freeCoinsTitle.text = currMin.ToString () + ":" + (currSec < 10 ? "0" + currSec.ToString () : currSec.ToString ());
		timerImage.fillAmount = fillRate;
	}
}
