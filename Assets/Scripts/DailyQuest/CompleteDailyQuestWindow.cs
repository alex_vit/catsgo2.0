﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CompleteDailyQuestWindow : MonoBehaviour {
	[SerializeField]
	Image questIcon;
	[SerializeField]
	Text questTitle;
	[SerializeField]
	Image rewardIcon;
	[SerializeField]
	Text rewardText;

	DailyQuest _currQuest;

	public void Show(DailyQuest dc){
		_currQuest = dc;
		questIcon.sprite = StaticData.DailyQuestImage [dc.id];
		questTitle.text = Localizator.GetQuestText (dc.id); 
		//questProgress.text = dc.currCount + "/" + dc.necessCount;
		rewardText.text = dc.GetReward ().rewardCount.ToString();
		rewardIcon.sprite = StaticData.BallImages [dc.GetReward ().rewardType];

		long _divideTime = (ConnectionManager.GetGlobalTime() - dc.beginTime); 
		long _currHours = _divideTime / 3600;
		long _currMin = (_divideTime/60 - _currHours*60);

		Analytics.LogEvent(_define.dailyQuestInfo, new Dictionary<string, object>(){
			{_define.dailyQuestId, dc.id},
			{_define.dailyQuestTime, _currHours.ToString() + ":" + _currMin.ToString()}
		});
	}

	public void TakeReward(){
		if (_currQuest != null) {
			DailyQuestReward _reward = _currQuest.GetReward ();
			StaticData.playerData.playerBalls [_reward.rewardType] += _reward.rewardCount;
		}
	}
}
