﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DailyQuestPanel : MonoBehaviour {
	public RectTransform content;
	public int itemHeight;
	public List<DailyQuestUIItem> items;

	List<DailyQuest> itemInfo;

	long _currentTime;
	public void Init(){
		itemInfo = DailyQuestManager.GetInstance ().GetQuests ();
		for (int i = 0; i < items.Count; i++) {
			items [i].gameObject.SetActive (false);
		}
		_currentTime = ConnectionManager.GetGlobalTime ();
	}

	void OnEnable(){
		CalculateContentHeight (itemInfo.Count);
		for (int i = 0; i < itemInfo.Count; i++) {
			items [i].gameObject.SetActive (true);		
			items [i].Show (itemInfo [i], _currentTime);
		}
	}

	void CalculateContentHeight(int count){
		content.sizeDelta = new Vector2 (content.sizeDelta.x, count * itemHeight + 100);	
	}
}
