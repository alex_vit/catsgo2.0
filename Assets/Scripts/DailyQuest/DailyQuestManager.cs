﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Menu;

public class DailyQuestManager : MonoBehaviour {
	static DailyQuestManager _instance;

	public int maxDailyQuestCount = 0;

	List<DailyQuest> dailyQuest;
	int dailyQuestCount;

	public bool isOpened;

	// Use this for initialization
	void Awake(){
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
	}

	public static DailyQuestManager GetInstance(){
		return _instance;
	}

	public void LoadQuests(){
		isOpened = StaticData.lastDailyQuestTime != 0;

		dailyQuest = Serializator.LoadDailyQuests ();
		dailyQuestCount = dailyQuest.Count;
		//Serializator.SaveDailyQuests (dailyQuest);
	}

	public void TryOpenDailyQuests(){
		if (StaticData.playerInfo.level >= 3 && StaticData.lastDailyQuestTime == 0) {
			isOpened = true;
			StaticData.lastDailyQuestTime = ConnectionManager.GetGlobalTime ();

			dailyQuest = new List<DailyQuest> ();
			CreateNewDailyQuest ();

		}
	}

	public void CheckDailyQuests(){
		if (isOpened) {
			CheckExperienceDate ();
			if (ConnectionManager.GetGlobalTime() - StaticData.lastDailyQuestTime >= _define.dailyQuestCdTime) {
				CreateNewDailyQuest ();
				//Serializator.SaveDailyQuests (dailyQuest);
			}
		}
	}
	public void CheckExperienceDate(){
		if (ConnectionManager.CheckInternetConnection ()) {
			long _currTime = ConnectionManager.GetGlobalTime ();
			for (int i = 0; i < dailyQuest.Count; i++) {
				//if quest experied we remove it from bar
				if (_currTime - dailyQuest [i].beginTime >= _define.dailyQuestLifeTime) {
					dailyQuest [i].isExpired = true;
					CDebug.Log ("expired");
				}
			}
			dailyQuest.RemoveAll (x => x.isExpired);
		}
	}

	public void ReceiveMsg(PlayerAction act){
		if (isOpened) {
			bool isSmthCompleted = false;
			for (int i = 0; i < dailyQuest.Count; i++) {
				if (dailyQuest [i].Check (act))
					isSmthCompleted = true;		
			}
			if (isSmthCompleted) {
				CompleteQuests ();		
			}
		}
	}

	public void ReceiveMsgWithParam(PlayerAction act, int count){
		if (isOpened) {
			bool isSmthCompleted = false;
			for (int i = 0; i < dailyQuest.Count; i++) {
				if (dailyQuest [i].CheckWithParam (act, count))
					isSmthCompleted = true;		
			}
			if (isSmthCompleted) {
				CompleteQuests ();		
			}
		}
	}

	void CompleteQuests(){
		for (int i = 0; i < dailyQuest.Count; i++) {
			if (dailyQuest [i].isComplete) {
				GeneralUIController.GetInstance ().ShowQuestCompleteWindow (dailyQuest [i]);
			}
		}
		dailyQuest.RemoveAll (x => x.isComplete);
		Serializator.SaveDailyQuests (dailyQuest);
	}

	DailyQuest CreateNewDailyQuest(){
		//it's stub and will be rework
		int _id;

		_id = Random.Range (0, StaticData.QuestStore.Count);

		if(dailyQuest.Count != 0){
			if (_id == dailyQuest [0].id ) {
				_id = Mathf.Clamp (_id + Random.Range (0, dailyQuest.Count), 0, dailyQuest.Count);
			} 
		}
		DailyQuest _dc = new SimpleDailyQuest(StaticData.QuestStore[_id]);
		//show info window about it
		if (CameraSceneController.GetInstance () != null)
			GeneralUIController.GetInstance ().ShowNewQuestWindow (_dc);
		else
			MenuViewController.GetInstance ().ShowNewDailyQuestWindow (_dc);
		//add quest
		dailyQuest.Add(_dc);
		StaticData.lastDailyQuestTime = _dc.beginTime;
		Serializator.SaveDailyQuests (dailyQuest);
		return _dc;
	}

	public List<DailyQuest> GetQuests(){
		return dailyQuest;
	}

	public void SaveQuests(){
		Serializator.SaveDailyQuests (dailyQuest);
	}
	void OnApplicationPause(bool pauseStatus){
		if (isOpened) {
			if(dailyQuest != null)
				Serializator.SaveDailyQuests (dailyQuest);
		}
	}
}
