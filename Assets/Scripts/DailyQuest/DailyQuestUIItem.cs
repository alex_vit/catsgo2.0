﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DailyQuestUIItem : MonoBehaviour {
	[SerializeField]
	Image itemIcon;
	[SerializeField]
	Text itemTitle;
	[SerializeField]
	Text itemProgressText;
	[SerializeField]
	Text itemTimeText;
	[SerializeField]
	Text rewardValueText;
	[SerializeField]
	Image rewardIcon;

	public void Show(DailyQuest dc, long currTime){
		itemIcon.sprite = StaticData.DailyQuestImage [dc.id];
		itemTitle.text = Localizator.GetQuestText (dc.id);

		CDebug.Log ("quest progress " + dc.currCount + " " + dc.necessCount);
		itemProgressText.text = dc.currCount + "/" + dc.necessCount;

		long _divideTime = _define.dailyQuestLifeTime - (currTime - dc.beginTime); 
		long _currHours = _divideTime / 3600;
		long _currMin = (_divideTime/60 - _currHours*60);
		long _currSec = _divideTime % 60;


		itemTimeText.text = _currHours + ":" + _currMin;
		rewardValueText.text = dc.GetReward ().rewardCount.ToString();
		rewardIcon.sprite = StaticData.BallImages [dc.GetReward ().rewardType];
	}
}