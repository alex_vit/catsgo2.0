﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DailyQuest{
	public int id;
	public bool isComplete;
	public int necessCount;
	public int currCount;

	public long beginTime;
	public long currTime;

	public bool isExpired = false;

	protected QuestLevel level;
	protected DailyQuestReward reward;

	public virtual void Open(){	}
	public virtual bool Check (PlayerAction act){ 
		return false;
	}
	public virtual bool CheckWithParam (PlayerAction act, int count){ 
		return false;
	}
	public virtual void Complete(){ }
	public virtual void GetProgress(){ }
	public virtual DailyQuestReward GetReward(){
		return new DailyQuestReward ();
	}
	//define quest level
	protected void SetQuestLevel(){
		int _rnd = Random.Range (1, 11);

		if (_rnd % 10 == 0) {
			level = QuestLevel.BEST;
			return;
		}
		if (_rnd % 5 == 0) {
			level = QuestLevel.HIGH;
			return;
		}
		if (_rnd % 3 == 0) {
			level = QuestLevel.MIDDLE;
			return;
		}
		if (_rnd % 2 == 0) {
			level = QuestLevel.INTERMEDIATE;
			return;
		}
		level = QuestLevel.LOW;
	}
	protected virtual void CalculateNecessaryValues(DailyQuestInfo info){
	}
	protected virtual void CalculateReward(){
	}
}

[System.Serializable]
public class SimpleDailyQuest : DailyQuest{
	PlayerAction action;

	public SimpleDailyQuest(DailyQuestInfo info){
		SetQuestLevel ();
		this.id = info.id;
		this.action = info.act;
		this.isComplete = false;
		this.reward = info.reward;

		CalculateNecessaryValues (info);
		CalculateReward ();

		beginTime = ConnectionManager.GetGlobalTime ();
		currTime = _define.dailyQuestLifeTime;
	}

	public override bool Check(PlayerAction act){
		if (action == act && !isComplete) {
			if (++currCount >= necessCount) {
				//complete daily quest
				isComplete = true;
				return true;
			}
		}
		return false;
	}

	public override bool CheckWithParam(PlayerAction act, int count){
		if (action == act && !isComplete) {
			currCount += count;
			if (currCount >= necessCount) {
				//complete daily quest
				isComplete = true;
				return true;
			}
		}
		return false;
	}

	public override DailyQuestReward GetReward(){
		return this.reward;	
	}

	protected override void CalculateNecessaryValues(DailyQuestInfo info){
		switch (action) {
		case PlayerAction.FIND_MONEY:
			necessCount = info.necessCount * StaticData.playerInfo.level * 2 * (int)level;
			break;
		default:
			necessCount = info.necessCount * (int)level;
			break;
		}	
	}

	protected override void CalculateReward(){
		if (level == QuestLevel.BEST) {
			reward.rewardCount = 1;
			reward.rewardType = BallType.GOLD;
		} else {
			if (level == QuestLevel.HIGH)
				reward.rewardCount = 10;
			if (level == QuestLevel.MIDDLE)
				reward.rewardCount = 7;
			if (level == QuestLevel.INTERMEDIATE)
				reward.rewardCount = 5;
			if (level == QuestLevel.LOW)
				reward.rewardCount = 3;
			reward.rewardType = BallType.SILVER;
		}
	}
}
[System.Serializable]
public struct DailyQuestInfo{
	public int id;
	public PlayerAction act;
	public int necessCount;
	public DailyQuestReward reward;
}

[System.Serializable]
public struct DailyQuestReward{
	public BallType rewardType;
	public int rewardCount;
}

public enum QuestLevel{
	LOW = 1, 
	INTERMEDIATE = 2,
	MIDDLE = 3,
	HIGH = 4,
	BEST = 5
}