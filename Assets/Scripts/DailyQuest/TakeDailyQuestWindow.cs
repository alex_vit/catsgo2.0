﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TakeDailyQuestWindow : MonoBehaviour {
	[SerializeField]
	Image questIcon;
	[SerializeField]
	Text questTitle;
	[SerializeField]
	Text questProgress;
	[SerializeField]
	Image rewardIcon;
	[SerializeField]
	Text rewardText;

	public void Show(DailyQuest dc){
		questIcon.sprite = StaticData.DailyQuestImage [dc.id];
		questTitle.text = Localizator.GetQuestText (dc.id); 
		questProgress.text = dc.currCount + "/" + dc.necessCount;
		rewardText.text = dc.GetReward ().rewardCount.ToString();
		rewardIcon.sprite = StaticData.BallImages [dc.GetReward ().rewardType];
	}
}
