﻿using UnityEngine;
using System.Collections;

public class MovableTest : MonoBehaviour {
	Transform mTransform;
	public float offset;
	// Use this for initialization
	void Awake(){
		mTransform = transform;
	}

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}

	void Move(){
		#if UNITY_EDITOR
		if(Input.GetKey(KeyCode.W))
			mTransform.position += new Vector3(offset, 0,0);
		if(Input.GetKey(KeyCode.S))
			mTransform.position -= new Vector3(offset, 0,0);
		if(Input.GetKey(KeyCode.A))
			mTransform.position += new Vector3(0,0,offset);
		if(Input.GetKey(KeyCode.D))
			mTransform.position -= new Vector3(0,0,offset);
		#endif
	}

}
