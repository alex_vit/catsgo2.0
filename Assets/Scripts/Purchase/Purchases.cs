﻿/*
Список покупок и коллбеки для них
*/

using UnityEngine;
using System.Collections.Generic;
using System;
using Advertise;

namespace Wazzapps.Purchasing {
	/// <summary>
	/// Список покупок в приложении. Взаимодействует с юнити-покупками
	/// </summary>
	public class Purchases {

		public static Purchase[] list = {
			new Purchase(_define.noAdsId,0,false,0,() => {
				ShopController.GetInstance().HandleSuccessPurchase(_define.noAdsId); 
				//AdManager.GetInstance().TurnOffAdv();
			}, () => {ShopController.GetInstance().HandleFailedPurchase(_define.noAdsId);}, null),
			new Purchase(_define.tenGoldenBallId,0,true,0,() => {ShopController.GetInstance().HandleSuccessPurchase(_define.tenGoldenBallId);}, () => {ShopController.GetInstance().HandleFailedPurchase(_define.tenGoldenBallId);}, null),
			new Purchase(_define.fiftyGoldenBallId,0,true,0,() => {ShopController.GetInstance().HandleSuccessPurchase(_define.fiftyGoldenBallId);},() => {ShopController.GetInstance().HandleFailedPurchase(_define.fiftyGoldenBallId);}, null),
			new Purchase(_define.smallCoinsPackId,0,true,0,() => {ShopController.GetInstance().HandleSuccessPurchase(_define.smallCoinsPackId);}, () => {ShopController.GetInstance().HandleFailedPurchase(_define.smallCoinsPackId);}, null),
			new Purchase(_define.middleCoinsPackId,0,true,0,() => {ShopController.GetInstance().HandleSuccessPurchase(_define.middleCoinsPackId);}, () => {ShopController.GetInstance().HandleFailedPurchase(_define.middleCoinsPackId);}, null),
			new Purchase(_define.largeCoinsPackId,0,true,0,() => {ShopController.GetInstance().HandleSuccessPurchase(_define.largeCoinsPackId);}, () => {ShopController.GetInstance().HandleFailedPurchase(_define.largeCoinsPackId);}, null)
		};

		public class Purchase {
			private static int currentId = 0;

			public int group = 0;
			public int id = 0;
			public int count = 0;
			public string title;
			public System.Action callback = null, failCallback = null, failInitCallback = null;
			public bool consumable = false;
			public string priceStr = "";
			private Sprite m_Sprite = null;

			public Purchase(string title, int group, bool consumable, int count, System.Action callback, System.Action failCallback = null, System.Action failInitCallback = null) {
				this.title = title;
				this.group = group;
				this.consumable = consumable;
				this.callback = callback;
				this.failCallback = failCallback;
				this.failInitCallback = failInitCallback;
				this.count = count;

				this.id = currentId++;
			}

			public Sprite GetSprite() {
				if (m_Sprite == null) {
					m_Sprite = Resources.Load<Sprite> ("Shop/"+title);
				}
				return m_Sprite;
			}

			public void SetPrice(string price) {
				priceStr = price;
			}

			public void Success() {
				if (callback != null) {
					callback ();
				}
			}

			public void Fail() {
				if (failCallback != null) {
					failCallback ();
				}
			}

			public void FailInit() {
				if (failInitCallback != null) {
					failInitCallback ();
				}
			}
		}
	}
}

