﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Analytics;


namespace Wazzapps.Purchasing {

	/// <summary>
	/// Обертка Unity покупок. Необходимо добавить на активный GameObject
	/// </summary>
	public class Purchaser : MonoBehaviour, IStoreListener
	{
		static Purchaser _instance;
		public const string AD_PREFS_KEY = "fsf5fs!dggregg43"; // для хранения покупок в памяти телефона (iOS)
		private static IStoreController m_StoreController;            
		private static IExtensionProvider m_StoreExtensionProvider;   

		void Awake(){
			if (_instance == null)
				_instance = this;
			else
				Destroy (gameObject);
		}
		void Start()
		{
			if (m_StoreController == null)
			{
				InitializePurchasing();
			}
		}

		public static Purchaser GetInsance(){
			return _instance;
		}

		public void InitializePurchasing() 
		{
			if (IsInitialized())
			{
				return;
			}


			var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance());
			foreach(Purchases.Purchase purchase in Purchases.list) {
				builder.AddProduct(purchase.title, 
					(purchase.consumable == true) ? ProductType.Consumable : ProductType.NonConsumable, 
					new IDs(){{ purchase.title, AppleAppStore.Name },{ purchase.title, GooglePlay.Name },});
			}
			UnityPurchasing.Initialize(this, builder);
		}


		private bool IsInitialized()
		{
			return m_StoreController != null && m_StoreExtensionProvider != null;
		}

		public void BuyProductID(string productId)
		{
			try
			{
				if (IsInitialized())
				{
					Product product = m_StoreController.products.WithID(productId);

					if (product != null && product.availableToPurchase)
					{
						Debug.Log ("Purchasing product asychronously: " + product.definition.id);
						m_StoreController.InitiatePurchase(product);
					}
					else
					{
						Debug.Log ("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
					}
				}
				else
				{
					Debug.Log("BuyProductID FAIL. Not initialized.");
				}
			}
			catch (Exception e)
			{
				Debug.Log ("BuyProductID: FAIL. Exception during purchase. " + e);
			}
		}

		public string GetProductLocalizedPriceByID(string productId){
			try
			{
				if (IsInitialized())
				{
					Product product = m_StoreController.products.WithID(productId);

					if (product != null && product.availableToPurchase)
					{
						string _res = product.metadata.localizedPriceString;
						return _res;
					}
					else
					{
						Debug.Log ("GetProductPriceByID: FAIL. Not purchasing product, either is not found or is not available for purchase");
						return null;
					}
				}
				else
				{
					Debug.Log("GetProductPriceByID FAIL. Not initialized.");
					return null;
				}
			}
			catch (Exception e)
			{
				Debug.Log ("BuyProductID: FAIL. Exception during purchase. " + e);
			}
			return null;
		}

		// Восстановить покупки (нужно только для iOS)
		public void RestorePurchases()
		{
			if (!IsInitialized())
			{
				Debug.Log("RestorePurchases FAIL. Not initialized.");
				return;
			}

			if (Application.platform == RuntimePlatform.IPhonePlayer || 
				Application.platform == RuntimePlatform.OSXPlayer)
			{
				Debug.Log("RestorePurchases started ...");

				var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
				apple.RestoreTransactions((result) => {
					Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
				});
			}
			else
			{
				Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
			}
		}

		public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
		{
			Debug.Log("OnInitialized: PASS");

			m_StoreController = controller;
			m_StoreExtensionProvider = extensions;

			foreach(Purchases.Purchase purchase in Purchases.list) {
				if (purchase.consumable == false) {
					if (controller.products.WithID (purchase.title).hasReceipt) {
						Debug.Log ("Purchase YES " + purchase.title);
						purchase.Success ();
					} else {
						Debug.Log ("Purchase NO " + purchase.title);
						purchase.Fail ();
					}
				}
				purchase.SetPrice(controller.products.WithID (purchase.title).metadata.localizedPriceString);
			}
		}


		public void OnInitializeFailed(InitializationFailureReason error)
		{
			Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
			foreach(Purchases.Purchase purchase in Purchases.list) {
				purchase.FailInit ();
			}
		}


		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
		{
			foreach (Purchases.Purchase purchase in Purchases.list) {
				if (String.Equals (args.purchasedProduct.definition.id, purchase.title, StringComparison.Ordinal)) {
					Debug.Log ("ProcessPurchase: PASS. Product: " + args.purchasedProduct.definition.id);

					Analytics.Transaction (args.purchasedProduct.definition.id, args.purchasedProduct.metadata.localizedPrice, args.purchasedProduct.metadata.isoCurrencyCode, args.purchasedProduct.receipt, null);

					/*Analytics.CustomEvent ("Purchase", new Dictionary<string, object> () { 
						{"Item", args.purchasedProduct.definition.id}, 
						{"Coins", PlayerInfo.Get (PlayerInfo.Param.Coins)},
						{"Crystals", PlayerInfo.Get (PlayerInfo.Param.Crystals)},
						{"Energy", PlayerInfo.Get (PlayerInfo.Param.Energy)},
						{"LastLevel", Levels.GetLastOpened()}
					});
					Analytics.LogEvent (_define.purchaseSucKey, new Dictionary<string, object> () {
						{ _define.noAdsKey, StaticData.availableAd }
					});*/
					purchase.Success ();
				}
			}
			return PurchaseProcessingResult.Complete;
		}


		public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
		{
			Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '"+product.definition.storeSpecificId+"', PurchaseFailureReason: "+failureReason));

			foreach (Purchases.Purchase purchase in Purchases.list) {
				if (String.Equals (product.definition.id, purchase.title, StringComparison.Ordinal)) {
					Debug.Log ("ProcessPurchase: FAIL. Product: " + product.definition.id);
					/*Analytics.CustomEvent ("PurchaseFail", new Dictionary<string, object> () { 
						{"Item", product.definition.id}, 
						{"Coins", PlayerInfo.Get (PlayerInfo.Param.Coins)},
						{"Crystals", PlayerInfo.Get (PlayerInfo.Param.Crystals)},
						{"Energy", PlayerInfo.Get (PlayerInfo.Param.Energy)},
						{"LastLevel", Levels.GetLastOpened()}
					});
					Analytics.LogEvent (_define.purchaseFailKey, new Dictionary<string, object> () {
						{ _define.noAdsKey, StaticData.availableAd }
					});*/
					purchase.Fail ();
				}
			}
		}
	}
}
