﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class ObjectFactory : MonoBehaviour {
	public float height;

	public SpawnedItem CreateItem(Vector2 pos, Transform map, SpawnItemType type, SpawnItemInfo info){
		switch (type) {
		case SpawnItemType.CAT:
			return CreateCat (pos, map, info);
		case SpawnItemType.BALL:
			return CreateBall (pos, map, info);
		case SpawnItemType.MONEY:
			return CreateMoney (pos, map);
		}
		return null;
	}


	SpawnedItem CreateCat(Vector2 pos, Transform map, SpawnItemInfo info){
		//define rarity by random
		CatInfo _info;
		if (info.itemId == 0) {
			Rarity _rarity = DefineRarity (Random.Range (0, 1000));
			List<CatInfo> _infoList = StaticData.CatParams.Where (x => x.rarity == _rarity).ToList ();
			_info = _infoList [Random.Range (0, _infoList.Count)];
		} else {
			_info = StaticData.CatParams.Where (x => x.id == info.itemId).First();
		}

		SpawnedItem _item = Instantiate (StaticData.Prefab [_define.catTag]).GetComponent<SpawnedItem> ();
		_item.Spawn (new Vector3(pos.x, height, pos.y), map);
		(_item as Cat).Init (_info);
		_item.GetComponent<Catchable> ().SetAvailable (true);
		return _item;

		/*Cat _cat = Instantiate (StaticData.Prefab [_define.catTag]).GetComponent<Cat> ();
		_cat.transform.SetParent (map);
		//_cat.transform.position = new Vector3 (pos.x, height, pos.y);
		_cat.transform.localPosition = new Vector3 (pos.x, height, pos.y);
		_cat.GetComponent<Catchable> ().SetPosition (_cat.transform.localPosition);
		_cat.Init (_info);
		return _cat;*/
	}

	SpawnedItem CreateBall(Vector2 pos, Transform map, SpawnItemInfo info){
		BallInfo _info = new BallInfo();
		_info.ballType = BallType.WOOL;

		SpawnedItem _item = Instantiate (StaticData.Prefab [_define.ballTag]).GetComponent<SpawnedItem> ();
		_item.Spawn (new Vector3(pos.x, height, pos.y), map);
		(_item as Ball).Init (_info);
		_item.GetComponent<Catchable> ().SetAvailable (true);
		return _item;

		/*Catchable _ball = Instantiate (StaticData.Prefab [_define.ballTag]).GetComponent<Catchable> ();
		_ball.transform.SetParent (map);
		_ball.transform.localPosition = new Vector3 (pos.x, height, pos.y);
		_ball.SetPosition (_ball.transform.localPosition);
		return _ball;*/
	}

	SpawnedItem CreateMoney(Vector2 pos, Transform map){
		SpawnedItem _item = Instantiate (StaticData.Prefab [_define.moneyTag]).GetComponent<SpawnedItem> ();
		_item.Spawn (new Vector3(pos.x, height, pos.y), map);
		(_item as Coin).Init ();
		_item.GetComponent<Catchable> ().SetAvailable (true);
		return _item;
	}


	Rarity DefineRarity(int value){
		if (value % 200 == 0)
			return Rarity.LEGENDARY;
		if (value % 50 == 0)
			return Rarity.EPIC;
		if (value % 25 == 0)
			return Rarity.RARE;
		return Rarity.COMMON;
	}
}