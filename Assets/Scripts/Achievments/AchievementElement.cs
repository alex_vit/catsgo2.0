﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using CatMenu;

public class AchievementElement : MonoBehaviour {
	public int id;

	Button mButton;
	[SerializeField]
	Image image;
	[SerializeField]
	ImageSlider progressBar;


	bool isOpened = true;
	// Use this for initialization
	void Awake () {
		mButton = GetComponent<Button> ();
		mButton.onClick.AddListener (Click);
	}

	#region show-hide methods
	public void Show(){
		mButton = GetComponent<Button> ();
		mButton.onClick.AddListener (Click);
		image.sprite = StaticData.AchievementImages [id];
		progressBar.SetFillColor (StaticData.AchieveColors [id]);

		Achieve _ach = AchievmentManager.GetInstance ().GetAchievs ().Where (x => x.id == id).First ();
		isOpened = _ach.isComplete;
		SetProgress (_ach);
		mButton.interactable = true;
	}
	#endregion

	void Click(){
		PlayerMenuViewController.GetInstance ().ShowAchiveDetailWindow (id);
	}

	void SetProgress(Achieve ach){
		//already complete 
		if (isOpened) {
			progressBar.SetValue (1f);
		} else {
			CDebug.Log ("progress " + ach.GetProgress ());
			progressBar.SetValue (ach.GetProgress ());
		}
	}
}
