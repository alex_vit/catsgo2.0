﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Game;
using Map;
/// <summary>
/// Achievment manager. Class for work with achivs
/// </summary>
public class AchievmentManager : MonoBehaviour {
	static AchievmentManager _instance;

	List<Achieve> totalAchiveList;
	List<Achieve> incompletedAchiveList;



	void Awake(){
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
	}

	public static AchievmentManager GetInstance(){
		return _instance;
	}

	public void Init(){
		totalAchiveList = new List<Achieve> ();
		incompletedAchiveList = new List<Achieve> ();

		totalAchiveList = Serializator.LoadAchivs ();
		incompletedAchiveList = totalAchiveList.Where (x => !x.isComplete).ToList ();
	}

	public void ReceiveMsg(PlayerAction act){
		bool _isSmthCompleted = false;
		CDebug.Log ("receive " + incompletedAchiveList.Count.ToString());
		for (int i = 0; i < incompletedAchiveList.Count; i++) {
			//if (incompletedAchiveList [i].Check (act)) {
				//CompleteAchive (incompletedAchiveList [i]);
			//}
			if(incompletedAchiveList [i].Check (act)){
				_isSmthCompleted = true;
			}
		}
		if (_isSmthCompleted) {
			CompleteAchieves ();
		}
	}

	public List<Achieve> GetAchievs(){
		return totalAchiveList;
	}

	public List<Achieve> GetIncompletedAchievs(){
		return incompletedAchiveList;
	}
		
	void CompleteAchive(Achieve ach){
		CDebug.Log ("complete_achive " + ach.id);
		totalAchiveList.Where (x => x.id == ach.id).First ().isComplete = true;
		incompletedAchiveList.Remove (ach);
		//get reward
		StaticData.playerData.playerBalls[ach.GetReward().typeOfBall] += ach.GetReward().ballsCount;

	}

	void CompleteAchieves(){
		for (int i = 0; i < incompletedAchiveList.Count; i++) {
			Achieve _ach = incompletedAchiveList [i];
			if (_ach.isComplete) {
				StaticData.playerData.playerBalls[_ach.GetReward().typeOfBall] += _ach.GetReward().ballsCount;
				totalAchiveList.Where (x => x.id == _ach.id).First ().isComplete = true;

				GeneralUIController.GetInstance ().ShowAchieveCompleteWindow (_ach);

				Analytics.LogEvent(_define.achieveInfo, new Dictionary<string, object>(){
					{_define.achieveId, _ach.id}
				});
			}
		}
		incompletedAchiveList.RemoveAll (x => x.isComplete);
		Serializator.SaveAchivs (totalAchiveList);
	}

	public void CheckAchievmentsDelay(AchieveTimeDelay delay){
		if(incompletedAchiveList != null){ 
		List<Achieve> _ach = incompletedAchiveList.Where (x => x.achiveTimeType == delay).ToList ();
		for (int i = 0; i < _ach.Count; i++) {
			_ach [i].RemoveProgress ();
		}
		}
	}

	void OnApplicationPause(bool pauseStatus){
		if(totalAchiveList != null)		
			Serializator.SaveAchivs (totalAchiveList);
	}

	void OnApplicationQuit(){
		CheckAchievmentsDelay (AchieveTimeDelay.SESSION);
	}
}


