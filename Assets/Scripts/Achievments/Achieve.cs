﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Achieve{
	public int id;
	public int timeOut;
	public int timeSpend;
	public bool isComplete;
	public int necessCount;
	public int currCount;
	public AchieveReward reward;
	public AchieveTimeDelay achiveTimeType;

	public virtual bool Check(PlayerAction act){
		return false;
	}

	public virtual float GetProgress(){
		return 0;
	}

	public virtual void RemoveProgress(){
		
	}

	public virtual AchieveReward GetReward(){
		return reward;
	}

	public void SetComplete(){
		isComplete = true;
	}
}
[System.Serializable]
public class ComplexAchive : Achieve{
	Dictionary<PlayerAction, int> actions;
	Dictionary<PlayerAction, int> actionsInfo;

	public ComplexAchive(int ID, AchieveTimeDelay type, int timeOut, Dictionary<PlayerAction, int> needsActions, Dictionary<PlayerAction, int> needsActionsInfo, int necessCount, AchieveReward award, bool isCompleted){
		this.id = ID;
		this.achiveTimeType = type;
		this.timeOut = timeOut;
		this.actions = needsActions;
		this.actionsInfo = needsActionsInfo;
		this.reward = award;
		this.isComplete = isCompleted;
		this.necessCount = necessCount;
	}  

	public override bool Check(PlayerAction act){
		
		if (actions.ContainsKey (act)) {
			if (achiveTimeType == AchieveTimeDelay.TIME) {
				timeSpend += (int)(ConnectionManager.GetGlobalTime () - StaticData.playerData.lastTimePlayed);
				//if time for achive more than needs, remove progress
				if (timeSpend >= timeOut) {
					RemoveProgress ();
				}
			}
			actions [act]++;

			int _currProgressCount = 0;
			//check actions count
			for(int i=0; i < actions.Keys.Count; i++){
				PlayerAction _curKey = actions.Keys.ElementAt(i);
				if (actions [_curKey] == actionsInfo [_curKey]) {
					//if all actions count more than needs we increase global counter
					if (++_currProgressCount >= actions.Keys.Count) {
						if (++currCount >= necessCount && !isComplete) {
							isComplete = true;
							return true;
						}
						return false;
					}
				}
			}
		}
		return false;
	}

	public override float GetProgress ()
	{
		float _currProg = 0f;
		for(int i=0; i< actions.Keys.Count; i++){
			PlayerAction _curKey = actions.Keys.ElementAt(i);
			_currProg += (float)actions [_curKey] / actionsInfo [_curKey];
		}

		return (float)currCount / necessCount;
	}

	public override void RemoveProgress(){
		for (int i = 0; i < actions.Keys.Count; i++) {
			PlayerAction _curKey = actions.Keys.ElementAt (i);
			actions [_curKey] = 0;
		}
		timeSpend = 0;
	}

	public override AchieveReward GetReward ()
	{
		return reward;
	}
}

[System.Serializable]
public class SimpleAchive : Achieve{
	PlayerAction action;

	public SimpleAchive(int ID, AchieveTimeDelay type, int timeOut, PlayerAction needsAction, int needsActionCount, AchieveReward award, bool isCompleted){
		this.id = ID;
		this.achiveTimeType = type;
		this.timeOut = timeOut;
		this.action = needsAction;
		this.necessCount = needsActionCount;
		this.reward = award;
		this.isComplete = isCompleted;
	}  

	public override bool Check(PlayerAction act){
		
		if (action == act) {
			if (achiveTimeType == AchieveTimeDelay.TIME) {
				timeSpend += (int)(ConnectionManager.GetGlobalTime () - StaticData.playerData.lastTimePlayed);
				//if time for achive more than needs, remove progress
				if (timeSpend >= timeOut) {
					RemoveProgress ();	
				}
			}
			CDebug.Log("action handled " + id);
			//if more than needs
			if (++currCount == necessCount && !isComplete) {
				isComplete = true;
				return true;
			}
			return false;
		}
		return false;
	}

	public override float GetProgress ()
	{
		return (float)currCount / necessCount;
	}

	public override void RemoveProgress(){
		currCount = 0;
		timeSpend = 0;
	}

	public override AchieveReward GetReward ()
	{
		return reward;
	}
}
[System.Serializable]
public struct AchieveReward{
	public BallType typeOfBall;
	public int ballsCount; 
}

//enum describe type of achieve progreess, per all time, per session, per game, per time
public enum AchieveTimeDelay{
	BASE,
	TIME,
	SESSION,
	GAME
}
