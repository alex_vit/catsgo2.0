﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public class TakeAchieveWindow : MonoBehaviour {
	[SerializeField]
	Text achieveName;
	[SerializeField]
	Text achieveText;
	[SerializeField]
	Image achieveImage;
	[SerializeField]
	Image rewardImage;
	[SerializeField]
	Text rewardText;

	public void Show(int id){
		Achieve _ach = AchievmentManager.GetInstance ().GetAchievs ().Where (x => x.id == id).First ();

		achieveName.text = Localizator.GetAchiveName (id);
		achieveText.text = Localizator.GetAchiveText (id);
		achieveImage.sprite = StaticData.AchievementImages[id];
		rewardText.text = _ach.GetReward ().ballsCount.ToString();
		rewardImage.sprite = StaticData.BallImages [_ach.GetReward ().typeOfBall];

	}
}
