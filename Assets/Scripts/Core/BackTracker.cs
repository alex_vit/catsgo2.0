using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class BackTracker : MonoBehaviour
{
	public static event UnityAction OnBackClick;

	void Update ()
	{
		if (Input.GetKeyUp (KeyCode.Escape)) {
			if (OnBackClick != null) {
				OnBackClick ();
			}
			SendMessage ("OnBackClick");
		}
	}
}
