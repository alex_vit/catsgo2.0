﻿using UnityEngine;
using System.Collections;

namespace Advertise{
	public class AdManager : MonoBehaviour {
		public string bannerId;
		public string fullScreenBannerId;
		public string unityAdId;
		public bool testMode;

		static AdManager _instance;
		// Use this for initialization
		void Awake(){
			if (_instance == null)
				_instance = this;
			else
				Destroy (gameObject);
		}

		void Start(){
			CDebug.Log ("Init ad " + StaticData.availableAd);
			InitAdv ();
		}

		public static AdManager GetInstance(){
			return _instance;
		}

		void InitAdv(){
			if (StaticData.availableAd) {
				AdCernel.GetInstance ().InitAd (bannerId, fullScreenBannerId, unityAdId, testMode);
			}
		}

		public void TurnOffAdv(){
			Debug.Log ("TurnOffAdv");
			StaticData.availableAd = false;
			AdCernel.GetInstance ().HideAd (AdType.BANNER);
			StaticData.SaveInfo ();
		}
	}
}
