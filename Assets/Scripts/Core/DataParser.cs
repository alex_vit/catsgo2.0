﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;
using System.Linq;

public class DataParser
{
	const char outSeparanor = ';';
	const char innerSeparanor = '|';

	public static string WriteIntMass (int[] incomeMass)
	{
		StringBuilder tmpString = new StringBuilder ();
		foreach (var _value in incomeMass) {
			tmpString.Append (_value);
			tmpString.Append (innerSeparanor);
		}
		tmpString.Remove (tmpString.Length - 1, 1);
		return tmpString.ToString ();
	}
	public static int[] ReadIntMass (string inputString)
	{
		string[] stringMass = inputString.Split (innerSeparanor);
		int[] tmpMass = new int[stringMass.Length];
		//string[] tmpString;
		for (int i = 0; i < stringMass.Length; i++) {
			tmpMass [i] = Convert.ToInt32 (stringMass [i]);
		}
		return tmpMass;
	}

	public static string WriteInt64Mass(Int64[] incomeMass){
		StringBuilder tmpString = new StringBuilder ();
		foreach (var _value in incomeMass) {
			tmpString.Append (_value);
			tmpString.Append (innerSeparanor);
		}
		tmpString.Remove (tmpString.Length - 1, 1);
		return tmpString.ToString ();
	}

	public static Int64[] ReadInt64Mass(string inputString){
		string[] stringMass = inputString.Split (innerSeparanor);
		Int64[] tmpMass = new Int64[stringMass.Length];
		//string[] tmpString;
		for (int i = 0; i < stringMass.Length; i++) {
			tmpMass [i] = Convert.ToInt64 (stringMass [i]);
		}
		return tmpMass;
	}

	public static string WriteDictionaryIntInt(Dictionary<int, int> source){
			int[] _keys = source.Keys.ToArray ();
			int[] _values = source.Values.ToArray ();

			StringBuilder _res = new StringBuilder ();

			string _keysStr = WriteIntMass (_keys);
			string _valuesStr = WriteIntMass (_values);

			_res.Append (_keysStr);
			_res.Append (outSeparanor);
			_res.Append (_valuesStr);
			_res.Append (outSeparanor);

			_res.Remove (_res.Length - 1, 1);
			return _res.ToString ();
	}

	public static Dictionary<int, int> ReadDictionaryIntInt(string inputString){
		if (inputString == null || inputString == "") {
			return new Dictionary<int, int> ();
		} else {
			string[] _stringMass = inputString.Split (outSeparanor);

			CDebug.Log (_stringMass.Length);

			if (_stringMass.Length <= 1) {
				return new Dictionary<int, int> ();
			} else {

				string[] _keys = _stringMass [0].Split (innerSeparanor);
				CDebug.Log ("_keys " + _keys.Length);
				string[] _values = _stringMass [1].Split (innerSeparanor);
				CDebug.Log ("_values " + _values.Length);

				if (_keys.Length != _values.Length) {
					CDebug.LogError ("Dictionary not correct, data was lost");
					return null;
				} else {
					Dictionary<int, int> _tmp = new Dictionary<int, int> ();
					int _key, _value;
					for (int i = 0; i < _values.Length; i++) {
						_key = Convert.ToInt32 (_keys [i]);
						CDebug.Log ("key " + _key);
						_value = Convert.ToInt32 (_values [i]);
						CDebug.Log ("value " + _value);
						_tmp.Add (_key, _value);
					}

					return _tmp;
				}
			}
		}
	}
	/*public static string WriteFloatMass (float[] incomeMass)
	{
		StringBuilder tmpString = new StringBuilder ();
		foreach (var _value in incomeMass) {
			tmpString.Append (_value);
			tmpString.Append (innerSeparanor);
		}
		tmpString.Remove (tmpString.Length - 1, 1);
		return tmpString.ToString ();
	}*/
	public static float[] ReadFloatMass (string inputString)
	{
		string[] stringMass = inputString.Split (innerSeparanor);
		float[] tmpMass = new float[stringMass.Length];
		//string[] tmpString;
		for (int i = 0; i < stringMass.Length; i++) {
			tmpMass [i] = Convert.ToSingle (stringMass [i]);
		}
		return tmpMass;
	}

	public static string WriteDictionaryValueIntInt(Dictionary<int, int> source){
		int[] _values = source.Values.ToArray ();

		StringBuilder _res = new StringBuilder ();
	
		string _valuesStr = WriteIntMass (_values);

		_res.Append (_valuesStr);
		_res.Append (outSeparanor);

		_res.Remove (_res.Length - 1, 1);
		return _res.ToString ();
	}
}
