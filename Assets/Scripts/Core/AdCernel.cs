﻿using UnityEngine;
using System;
using System.Net;
using System.IO;
using System.Collections;
using GoogleMobileAds.Api;
using UnityEngine.Advertisements;

/*
 * namespace for advertise scripts
 */
namespace Advertise{
	public delegate void UnityAdsCallBack<T>(T value);
	//interface for advetising 
	internal interface InitableAd : TemplateAd<AdType>{
		void InitAd (string adKey);
		void InitAd (string bannerKey, string interstitalKey);
		void InitAd (string bannerKey, string interstitalKey, string unityKey);
		void InitAd (string bannerKey, string interstitalKey, string unityKey, bool isTestMode);
	}

	public interface TemplateAd<T>{
		void HideAd (T type);
		void ShowBanner (T type);
		void ShowVideo (AdType videoType, Action onFinishedCallback, Action onSkippedCallback, Action onFailedCallBack);
	}
	//cernel class for advertisement, init and showing
	public class AdCernel : InitableAd{
		bool isShowed;
		private static BannerView mBanner;
		private static InterstitialAd mInterstitial;
		private static AdCernel _instance;

		private AdCernel(){
		}

		private void Init(string bannerId, string fullScreenId, string unityId, bool testMode){
			//init ad with this data
			if(bannerId != null){
				mBanner = new BannerView (bannerId, AdSize.Banner, AdPosition.Bottom);
				AdRequest _bannerReq = new AdRequest.Builder ().Build ();
				mBanner.LoadAd (_bannerReq);
			}if (fullScreenId != null) {
				mInterstitial = new InterstitialAd (fullScreenId);
				AdRequest _interReq = new AdRequest.Builder ().Build ();
				mInterstitial.LoadAd (_interReq);
				mInterstitial.OnAdClosed += (sender, args) => {mInterstitial.LoadAd(_interReq);};
			}if (unityId != null) {
				if (Advertisement.isSupported) { // If runtime platform is supported...
					Advertisement.Initialize (unityId, testMode); // ...initialize.
				} else
					Debug.Log ("Platform doesn't supported");
			}
		}

		public static AdCernel GetInstance(){
			if (_instance == null)
				_instance = new AdCernel ();
			return _instance;
		}
		//method for initialization
		public void InitAd (string adKey){
			Init (adKey, null, null, false);
		}
		public void InitAd(string bannerKey, string interstitialKey){
			Init (bannerKey, interstitialKey, null, false);
		}
		public  void InitAd(string bannerKey, string interstitialKey, string unityKey){
			Init (bannerKey, interstitialKey, unityKey, false);
		}
		public  void InitAd(string bannerKey, string interstitialKey, string unityKey, bool isTestMode){
			Init (bannerKey, interstitialKey, unityKey, isTestMode);
		}
		public void ShowBanner(AdType type){
			if (StaticData.availableAd) {
				//show ad with this type
				if (type == AdType.INTERSTITIAL) {
					try {
						if (Wazzapps.WazzappsAds.isAdReady)
							Wazzapps.WazzappsAds.ShowAd ();
						else
							mInterstitial.Show ();
					} catch (Exception e) {
						mInterstitial.Show ();	
					}
				} else
					mBanner.Show ();
			}
		}

		public void HideAd(AdType adtype){
			//hide ad with concrete type
			if (adtype == AdType.INTERSTITIAL) {
				if (mInterstitial != null)
					mInterstitial.Destroy ();
			} else {
				if (mBanner != null)
					mBanner.Hide ();
			}
		}



		public void ShowVideo(AdType videoType, Action onFinishedCallback, Action onSkippedCallback, Action onFailedCallBack){
			if (StaticData.availableAd) {
				if (ConnectionManager.CheckInternetConnection()) {
					CDebug.Log ("has internet connection");
					if (videoType == AdType.REWARDED_VIDEO) {
						ShowVideoForReward (delegate(UnityAdState state) {
							CDebug.Log ("result state " + state);
							if (state == UnityAdState.FINISHED) {
								if (onFinishedCallback != null)
									onFinishedCallback ();
							}
							if (state == UnityAdState.SKIPPED) {
								if (onSkippedCallback != null)
									onSkippedCallback ();
							}
							if (state == UnityAdState.FAILED) {
								if (onFailedCallBack != null)
									onFailedCallBack ();
							}
						});
					}
				} else {
					CDebug.Log ("no internet connection");
					if (onFailedCallBack != null)
						onFailedCallBack ();
				}
			} else {
				if (onFinishedCallback != null)
					onFinishedCallback ();
			}
		}

		void ShowSkippableVideo(){
			//show video
			Advertisement.Show();
		}

		void ShowVideoForReward(UnityAdsCallBack<UnityAdState> onVideoPlayed){
			string zone = "rewardedVideoZone";

			ShowOptions options = new ShowOptions {
				resultCallback = result => {
					CDebug.Log("result is :" + result);
					switch (result) {
					case ShowResult.Failed:
						onVideoPlayed (UnityAdState.FAILED);
						break;
					case ShowResult.Finished:
						onVideoPlayed (UnityAdState.FINISHED);
						break;
					case ShowResult.Skipped:
						onVideoPlayed (UnityAdState.SKIPPED);
						break;
					}
				}
			};
			if (Advertisement.IsReady (zone))
				Advertisement.Show (zone, options);
		}
	}

	static class Connection{
		internal static bool HasInternetConnection(){
			WebClient _client = null;
			Stream _stream = null;

			try{
				_client = new WebClient();
				_stream = _client.OpenRead("http://google.com");
				return true;
			}catch(Exception e){
				return false;
			}finally{
				if(_client != null) {_client.Dispose ();}
				if(_stream != null) {_stream.Dispose ();}
			}
		}
	}
}

public enum AdType{
	BANNER,
	BANNER_BOTTOM,
	BANNER_TOP,
	BANNER_CENTER,
	VIDEO,
	REWARDED_VIDEO,
	INTERSTITIAL
}

public enum UnityAdState{
	FAILED = 0, 
	FINISHED = 1, 
	SKIPPED = 2
}