﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Analytics : MonoBehaviour {
	public string key;
	private static bool isInited = false;
	private static Amplitude amplitude = null;

	void Awake(){
		Init (key);
	}

	public static void Init(string key) {
		amplitude = Amplitude.Instance;
//		amplitude.logging = true;
		amplitude.init(key);

		isInited = true;
	}

	public static void LogEvent(string eventName, Dictionary<string,object> values = null) {
		//if (isInited == false) {
			//Init ();
		//}

		amplitude.logEvent (eventName, values);
		UnityEngine.Analytics.Analytics.CustomEvent (eventName, values);
	}

	public static void Transaction(string productId, decimal price, string currency, string receipt, string signature) {
		//if (isInited == false) {
			//Init ();
		//}

		amplitude.logRevenue (productId, 1, (double)price);
	}
}
