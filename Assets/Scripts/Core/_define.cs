﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class _define{
	//tags for objects
	public static readonly string floorTag = "Floor";
	public static readonly string catTag = "Cat";
	public static readonly string mapTag = "Map";
	public static readonly string wallTag = "Wall";
	public static readonly string areaTag = "Area";
	public static readonly string ballTag = "Ball";
	public static readonly string moneyTag = "Coin";

	public static readonly int adsPeriod = 3;
	//number constants
	public static readonly int timeForAd = 30; //a time for addialog
	public static readonly int timeStep = 1; //timeStep for timer;
	public static readonly int timerForGame = 90;
	public static readonly int timerForFreeMoney = 600;
	public static readonly int freeMoneyReward = 50;
	public static readonly int splashScreenTimer =  10;
	public static readonly int timeForAdBalls = 10;

	public static readonly long dailyQuestCdTime = 43200;
	public static readonly long dailyQuestLifeTime = 129600;
	//keys for Localizator
	public static readonly string noStarsText = "no_stars";
	public static readonly string noAvailableLevelText = "no_level";
	public static readonly string adPurchaseText = "buy_ad";
	public static readonly string tutorialText = "go";
	public static readonly string noConnection = "no_inet";
	public static readonly string shareText = "share";
	public static readonly string catchText = "catch_cat";
	public static readonly string lostText = "lost_cat";
	public static readonly string lostXpText = "lose_game_xp";
	public static readonly string winXpText = "win_game_xp";
	public static readonly string levelText = "level";
	public static readonly string missText = "miss";

	public static readonly string commonCat = "common_cat";
	public static readonly string rareCat = "rare_cat";
	public static readonly string epicCat = "epic_cat";
	public static readonly string legendCat = "legend_cat";
	public static readonly string appName = "app_name";
	public static readonly string menuWatchAdButton = "menu_watch_ad";
	public static readonly string menuWatchAdButtonInactive = "menu_watch_ad_in";
	public static readonly string achieveProgressCompletedText = "ach_completed";

	public static readonly string achieveNotAvailable = "achieve_stub";
	public static readonly string dailyQuestNotAvailable = "daily_quest_stub";

	public static readonly string finalTutorialText = "fin_tutorial";

	public static readonly string notAvailableGPS = "no_gps";
	public static readonly string notAvailableInternet = "no_internet";
	public static readonly string notAvailableGPSAndInternet = "no_gps_internet";
	//keys for StaticData
	public static readonly string soundKey = "volume";
	public static readonly string firstRunKey = "IsFirstRun";
	public static readonly string advInitKey = "IsAdShow";
	public static readonly string wonGameImage = "won_image";
	public static readonly string lostGameImage = "lose_image";
	public static readonly string playerCatsKey = "cats_list";
	public static readonly string levelKey = "levelKey";
	public static readonly string ballsCountKey = "ballsKey";
	public static readonly string moneyKey = "moneyKey";
	public static readonly string xpKey = "xpKey";
	public static readonly string nextLevelXpKey = "nextXpKey";
	public static readonly string woolBallKey = "woolBallKey";
	public static readonly string silverBallKey = "silverBallKey";
	public static readonly string goldBallKey = "goldBallKey";
	public static readonly string timerTickKey = "timerKey";
	public static readonly string lastTimeKey = "lastTimeKey";
	public static readonly string questTime = "questTime";
	public static readonly string tutorialKey = "tutorial";
	public static readonly string totalCatsKey = "total_count";
	public static readonly string totalSessionCount = "session_number";
	public static readonly string achiveKey = "ach";

	public static readonly string catNotificationKey = "cat_notify";
	public static readonly string catNotificationIdKey = "cat_notify_id";
	public static readonly string notificationCounterKey = "notify_counter";
	public static readonly string freeCoinsWindowShowedKey = "fc_win_show";
	//keys for Analytics
	public static readonly string purchaseSucKey = "PurchaseSuccess";
	public static readonly string purchaseFailKey = "PurchaseFail";
	public static readonly string purchaseCurrencyKey = "Currency";
	public static readonly string purchaseCurrencyISOKey = "CurrencyISO";
	public static readonly string itemTypeKey = "ItemType";

	public static readonly string mapLoadKey = "MapDrawed";
	public static readonly string mapSelectItem = "MapSelectItem";

	public static readonly string playerInfoKey = "PlayerInfo";
	public static readonly string gameActionKey = "GameAction";

	public static readonly string gameInfoKey = "GameEndedInfo";
	public static readonly string levelUpInfoKey = "LevelUpInfo";
	public static readonly string playerMoneyKey = "PlayerCoins";
	public static readonly string playerBallsKey = "PlayerBalls";
	public static readonly string playerLevelKey = "PlayerLevel";
	public static readonly string playerCatsCollectionKey = "PlayerCollection";
	public static readonly string playerCatsCollectionKindKey = "KindOfCats";
	public static readonly string playerCatsCollectionCountKey = "CountOfKindCats";
	public static readonly string playerTotalCatsCountKey = "TotalCatsCount";
	public static readonly string playerSpendBallsKey = "PlayerSpendedBalls";

	public static readonly string currentCatKey = "CurrentCat";
	public static readonly string currentCatCatchedKey = "IsCatched";

	public static readonly string advWatchInfoKey = "WatchAd";
	public static readonly string startSessionKey = "StartSession";
	public static readonly string endSessionKey = "EndSession";

	public static readonly string mainMenuActionKey = "MainMenuOpened";
	public static readonly string mapActionKey = "MapScreenOpened";
	public static readonly string gameScreenActionKey = "GameScreenOpened";
	public static readonly string playerMenuActionKey = "PlayerMenuOpened";
	public static readonly string playerShopActionKey = "ShopOpened";
	public static readonly string catCollectionActionKey = "CatCollectionOpened";

	public static readonly string tutorialActionKey = "TutorialAction";
	public static readonly string tutorialBeginKey = "BeginTutorial";
	public static readonly string tutorialEndedKey = "EndTutorial";
	public static readonly string tutorialMapStageKey = "TutorialMapStep";
	public static readonly string tutorialGameStageKey = "TutorialGameStep";

	public static readonly string achieveInfo = "AchieveInfo";
	public static readonly string achieveId = "ID";

	public static readonly string dailyQuestInfo = "DailyQuestInfo";
	public static readonly string dailyQuestLevel = "DailyQuestLevel";
	public static readonly string dailyQuestId = "ID";
	public static readonly string dailyQuestTime = "SpendTime";
	//PurchasesID
	public static readonly string noAdsId = "no_ads";
	public static readonly string oneGoldenBallId = "gold_1";
	public static readonly string tenGoldenBallId = "gold_10";
	public static readonly string fiftyGoldenBallId = "gold_50";
	public static readonly string smallCoinsPackId = "coins_500";
	public static readonly string middleCoinsPackId = "coins_2k";
	public static readonly string largeCoinsPackId = "coins_10k";

	public static readonly string oneSimpleBallId = "one_ball";
	public static readonly string tenSimpleBallId = "ten_ball";
	public static readonly string fiftySimpleBallId = "fifty_ball";
	public static readonly string oneSilverBallId = "one_silver_ball";
	public static readonly string tenSilverBallId = "ten_silver_ball";
	public static readonly string fiftySilverBallId = "fifty_silver_ball";
	public static readonly string freeCoinsId = "free_coins";
	//GPS constants
	public static readonly int gps_update_time = 1;
	//API Keys
	public static readonly string[] mapKeys = new string[] {
		"mapzen-FHXMvQT",
		"mapzen-6sWtoqc",
		"mapzen-WHnrrkP",
		"mapzen-ozibNiT",
		"mapzen-Smj9f7M"
	};
	public static readonly string mapDebugKey = "mapzen-F4NUHAE";
}
/// <summary>
/// Field type.
/// </summary>
public enum FieldType{
	ATTACK, 
	DEFENCE
}
/// <summary>
/// Element tag.
/// </summary>
public enum ElementTag{
	NONE,
	ROCKET,
	NMD_CIRCLE
}

public enum GameState{
	LOADING,
	MENU,
	PLAYER_MENU,
	MAP_VIEW,
	GAME_VIEW
}

public enum PlayerAction{
	NONE,
	SPEND_BALL,
	SPEND_SILVER_BALL,
	SPEND_GOLDEN_BALL,
	CATCH_CAT,
	PASS_DIST,
	CATCH_ATTEMPT,
	FIND_BALL,
	FIND_MONEY
}

//info about player, his level, params, name, xp
public class PlayerInfo{
	public int level = 1;
	public int currentXp = 0;
	public string name = "Player";
	public int money = 0;
	public int nextLevelXp = 1;
	public bool isFreeCoinAvailable = true;
}
//player data, linked objects, current gameState
public struct PlayerData{
	public Dictionary<Vector2, List<SpawnItemInfo>> spawnedItemsList;
	public Dictionary<int, int> playerCatsList;
	public Dictionary<BallType, int> playerBalls;
	public GameInfo gameInfo;

	public Int64 lastTimePlayed;
	public Int64 dailyTimePlayed;

	public int freeMoneyTick;
	public int totalCatsCount;
}

public struct CatInfo{
	public int id;
	public string name;
	public float agility;
	public float catchTime;
	public float catchCircle;
	public Rarity rarity;
	public int xp;
}

public struct BallInfo{
	public BallType ballType;
	public float catchRadius;
	public bool isEscapeAble;
}

public struct GameInfo{
	public float time;
	public int ballsCount;
	public BallType currentBall;
	public int currentCatId;
}

public struct SpawnItemInfo{
	public Vector3 itemPos;
	public int itemId;
	public SpawnItemType itemType;
}

public struct MapItemInfo{
	public int cats;
	public int balls;
	public int coins;
}

public struct ShopItemInfo{
	public CostType costType;
	public PurchaseType type; //kind of item of purchase
	public BallType currBallType;
	public int reward; //value for increasing purchases
	public int cost; //cost for inner purchases that uses coins
	public Action rewardAction; //if reward isn't countable
}

public struct NotificationInfo{
	public long delay;
	public string title;
	public string msg;
}

public enum SpawnItemType{
	CAT,
	MONEY,
	BALL
}

public enum CostType{
	COINS,
	MONEY,
	ACTION
}

public enum PurchaseType{
	BALL,
	COINS,
	ACTION
}

public enum BallType{
	NONE,
	WOOL,
	SILVER,
	GOLD
}
