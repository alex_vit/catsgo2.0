﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LocalizedImage : Image {
	[SerializeField]
	private string spriteKey;

	void Start(){
		sprite = Localizator.GetSpriteValue (spriteKey);
	}
}
