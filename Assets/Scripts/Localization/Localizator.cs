﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

//class for localization in Application
public static class Localizator{
	public static Language activeLanguage;
	#region general and ui
	private static Dictionary<Language, Dictionary<string, string>> _localizedTexts = new Dictionary<Language, Dictionary<string, string>>(){
		{ Language.RUS, new Dictionary<string, string>()
			{
				{"play", "Играть"},
				{"ad", "Отключение рекламы"},
				{"buy_ad", "Вы хотите отключить рекламу"}, 
				{"more_games", "Больше игр"},
				{"update", "Обновить"},
				{"no_stars", "Недостаточно звезд"},
				{"no_level", "Этот уровень недоступен, хотите открыть его?"},
				{"go", "Вперед!"},
				{"no_inet", "Проверьте свое интернет-соединение"},
				{"tutorial_hit", "Ударь по краю"},
				{"watch_ad", "Хотите посмотреть видео, и нанести еще один удар?"},
				{"result_dmg", "Общие повреждения"},
				{"reward", "Награда"},
				{"get_reward", "Получить награду"},
				{"one_kill", "Убийство"},
				{"double_kill", "Двойное убийство"},
				{"tripple_kill", "Тройное убийство!"},
				{"quadro_kill", "Мульти-убийство!!!"},
				{"fatality_kill", "!!ФАТАЛИТИ!!"},
				{"loading", "Ищем котиков"},
				{"catch_cat", "Кот пойман"},
				{"lost_cat", "Кот удрал"},
				{"one_ball", "Обычный клубок"},
				{"ten_ball", "10 клубков"},
				{"fifty_ball", "50 клубков"},
				{"one_silver_ball", "Серебряный клубок\nЭффективность x2"},
				{"ten_silver_ball", "10 серебряных клубков"},
				{"fifty_silver_ball", "50 серебряных клубков"},
				{"one_golden_ball", "Золотой клубок\n100% попадание"},
				{"ten_golden_ball", "5 золотых клубков"},
				{"fifty_golden_ball", "25 золотых клубков"},
				{"coins_small", "500 монет"},
				{"coins_midle", "2000 монет"},
				{"coins_large", "10000 монет"},
				{"no_gps", "\nВключите GPS и нажмите \"Обновить\""},
				{"no_internet", "Включите Интернет и нажмите \"Обновить\""},
				{"no_gps_internet", "Включите GPS и Интернет, и нажмите \"Обновить\""},
				{"no_coins", "Недостаточно  монет для покупки, хотите приобрести монеты?"},
				{"coming_soon", "Доступно с 5 уровня"},
				{"cat_1", "Мяу-Мяу!"},
				{"cat_2", "Мурр"},
				{"cat_3", "Ха-Ха!"},
				{"cat_4", "Не поймаешь"},
				{"cat_5", "Хочу сосиски!"},
				{"cat_6", "А рыба есть?"},
				{"cat_7", "Поцарапаю!!"},
				{"cat_8", "Что принес?"},
				{"cat_9", "Погладь меня!"},
				{"cat_10", "Промазал"},
				{"miss", "Мимо!"},
				{"map_1", "Раскидываем монеты..."},
				{"map_2", "Раскидываем клубки..."},
				{"map_3", "Ищем когтеточку..."},
				{"map_4", "Воруем сосиски..."},
				{"map_5", "Рассаживаем котов..."},
				{"map_6", "Гладим котов..."},
				{"map_7", "Ищем единокота..."},
				{"map_8", "Слушаем муррчание..."},
				{"map_9", "Покупаем корм..."},
				{"map_10", "Точим когти о мебель..."},
				{"show_ad", "Хотите посмотреть видео и получить " + _define.freeMoneyReward.ToString() + " монет?"},
				{"my_cats", "Мои котята"},
				{"lose_game_xp", "Попытайтесь снова"},
				{"win_game_xp", "опыта"},
				{"level", "Уровень"},
				{"lvl_up_header", "Поздравляем"},
				{"lvl_up_title", "Новый уровень!"},
				{"detail_cat_count", "Таких котиков у вас:"},
				{"free_coins", "Бесплатные монеты"},
				{"buy_balls", "5 клубков"},
				{"splash_1", "Не забудь\nвключить\nинтернет!"},
				{"splash_2", "Он нужен\nдля игры!"},
				{"splash_3", "И не забудь\nвключить\nGPS!"},
				{"splash_4", "Чтобы\nнайти\nменя!"},
				{"share", "Игра GO Cats! Ходить по городу и ловить котов!\n#gamedev #indiedev #banana4apps #gocats"},
				{"common_cat", "Рядом с вами гуляет кот!"},
				{"rare_cat", "Рядом с вами гуляет редкий кот!"},
				{"epic_cat", "Рядом с вами гуляет очень редкий кот!"},
				{"legend_cat", "Рядом с вами гуляет неуловимый кот!"},
				{"app_name", "Игра GO Cats! Ходить по городу и ловить котов!"},
				{"menu_watch_ad", "за просмотр рекламы"},
				{"menu_watch_ad_in", "Доступно через "},
				{"menu_watch_ad_title", "Бесплатные монеты"},
				{"achieve_detail_title", "Новое достижение"},
				{"achieve_detail_reward", "Награда"},
				{"achieve_panel_title", "Достижения"},
				{"ach_completed", "Завершено"},
				{"achieve_stub", "Доступно с уровня 5"},
				{"daily_quest", "Задания"},
				{"daily_quest_title","Новое задание"},
				{"daily_quest_param", "Необходимо"},
				{"daily_quest_stub", "Доступно с уровня 3"},
				{"quest_get_reward", "Забрать награду"},
				{"quest_time_progress", "Осталось времени"},
				{"quest_value_progress", "Осталось выполнить"},
				{"fin_tutorial", "Теперь найди всех котят! Выходи на улицу и лови котят в своём городе"}
			}
		},
		{ Language.ENG, new Dictionary<string, string>()
			{
				{"play", "Play"},
				{"ad", "Turn off ad"},
				{"buy_ad", "Do you want switch off AD"}, 
				{"more_games", "More games"},
				{"update", "Update"},
				{"no_stars", "You haven't enough stars for this level"},
				{"no_level", "This level isn't available. Do you want open it?"},
				{"go", "Go!"},
				{"no_inet", "Check your internet connection"},
				{"tutorial_hit", "Strikes on the edge"},
				{"watch_ad", "Do you want show AD and take one more hit?"},
				{"result_dmg", "Total damage"},
				{"reward", "Your reward"},
				{"get_reward", "Get reward"},
				{"one_kill", "One Kill"},
				{"double_kill", "Double Kill"},
				{"tripple_kill", "Tripple Kill!"},
				{"quadro_kill", "Quadro Kill!!!"},
				{"fatality_kill", "!!FATALITY!!"},
				{"loading", "Find cats"},
				{"catch_cat", "Cat catched"},
				{"lost_cat", "Cat lost"},
				{"one_ball", "Usual ball"},
				{"ten_ball", "10 balls"},
				{"fifty_ball", "50 balls"},
				{"one_silver_ball", "1 silver ball\nEfficiency x2"},
				{"ten_silver_ball", "10 silver balls"},
				{"fifty_silver_ball", "50 silver balls"},
				{"one_golden_ball", "1 golden ball\n100% hit!"},
				{"ten_golden_ball", "5 golden balls"},
				{"fifty_golden_ball", "25 golden balls"},
				{"coins_small", "500 coins"},
				{"coins_midle", "2000 coins"},
				{"coins_large", "10000 coins"},
				{"no_gps", "Turn on the GPS and click \"Refresh\""},
				{"no_internet", "Turn on the Internet and click \"Refresh\""},
				{"no_gps_internet", "Turn on GPS and Internet, and then click \"Refresh\""},
				{"no_coins", "You don't have enough money, do you wanna buy coins?"},
				{"coming_soon", "Available since level 5"},
				{"cat_1", "Meow-Meow!"},
				{"cat_2", "Purrr"},
				{"cat_3", "Ha-Ha!"},
				{"cat_4", "Get away"},
				{"cat_5", "I want sausages!"},
				{"cat_6", "Give me a fish!"},
				{"cat_7", "I'll scratch!!"},
				{"cat_8", "It's for me?"},
				{"cat_9", "Stroke me!"},
				{"cat_10", "You missed"},
				{"miss", "Missed!"},
				{"map_1", "Throw the coins..."},
				{"map_2", "Throw the balls..."},
				{"map_3", "Looking for scratcher..."},
				{"map_4", "Steal the sausages..."},
				{"map_5", "Place the cats..."},
				{"map_6", "Caress the cats..."},
				{"map_7", "Find a unicorn cat..."},
				{"map_8", "Hear a purr-purr.."},
				{"map_9", "Buy a catfood..."},
				{"map_10", "Sharp claws at the furniture..."},
				{"show_ad", "Do you want show video ad and take " + _define.freeMoneyReward.ToString() + " coins?"},
				{"my_cats", "My cats"},
				{"lose_game_xp", "Sorry, try again"},
				{"win_game_xp", "points"},
				{"level", "Level"},
				{"lvl_up_header", "Gongratulations"},
				{"lvl_up_title", "New level!"},
				{"detail_cat_count", "Qauntity of this cat:"},
				{"free_coins", "Free coins"},
				{"buy_balls", "buy 5 balls"},
				{"splash_1", "Don't forget\nturn on\ninternet!"},
				{"splash_2", "It needs\nfor the game!"},
				{"splash_3", "And don't forget\nturn on\nGPS!"},
				{"splash_4", "To find\nme!"},
				{"share", "Game GO Cats! Walk around town and catch the cats!\n#gamedev #indiedev #banana4apps #gocats"},
				{"common_cat", "Next to you is walking a cat!"},
				{"rare_cat", "Next to you is walking a rare cat!"},
				{"epic_cat", "Next to you is walking a very rare cat!"},
				{"legend_cat", "Next to you is walking a elusive cat!"},
				{"app_name", "Game GO Cats! Walk around town and catch the cats!"},
				{"menu_watch_ad", "For ad watching"},
				{"menu_watch_ad_in", "Available trough "},
				{"menu_watch_ad_title", "Free coins"},
				{"achieve_detail_title", "New achievment"},
				{"achieve_detail_reward", "Reward"},
				{"achieve_panel_title", "Achievments"},
				{"ach_completed", "Completed"},
				{"achieve_stub", "Available since level 5"},
				{"daily_quest", "Daily quests"},
				{"daily_quest_title","New daily quest"},
				{"daily_quest_param", "Necessary count"},
				{"daily_quest_stub", "Availbale since level 3"},
				{"quest_get_reward", "Get reward"},
				{"quest_time_progress", "Time left"},
				{"quest_value_progress", "Items left"},
				{"fin_tutorial", "Let's find all cats! Take your phone, walk and catch all the cats in your city"}
			}
		}
	};
	#endregion
	#region cats
	private static Dictionary<Language, Dictionary<int, string>> _localizedCats = new Dictionary<Language, Dictionary<int, string>>(){
		{ Language.RUS, new Dictionary<int, string>()
			{
				{0, "Неизвестно"},
				{1, "Рыжик"},
				{2, "Бродяга"},
				{3, "Наивняха"}, 
				{4, "Черный Бель"},
				{5, "Серега"},
				{6, "Ночной воришка"},
				{7, "Ниндзя котик"},
				{8, "Морячок"},
				{9, "Сиамец"},
				{10, "Разбойник"},
				{11, "Кот со шрамом"},
				{12, "Пятнашка"},
				{13, "Милаха"},
				{14, "Печалька"},
				{15, "Морской кот"},
				{16, "Хмурняш"},
				{17, "Котосвин"},
				{18, "Синоби кот"},
				{19, "Везунчик"},
				{20, "Кот Сильвер"},
				{21, "Скептяра"},
				{22, "Пляжная киска"},
				{23, "Капитан Кот"},
				{24, "Дэдмур"},
				{25, "Единорожек"},
				{26, "Пандочка"},
				{27, "Кот с бантом"},
				{28, "Синий кот"},
				{29, "Япокот"},
				{30, "Рыбачу"},
				{31, "Пончик"},
				{32, "Злой Мяу"},
				{33, "Пикакот"},
				{34, "Джек Хеллоуин"},
				{35, "Котенок Эш"}
			}
		},
		{ Language.ENG, new Dictionary<int, string>()
			{
				{0, "Hidden cat"},
				{1, "Red cat"},
				{2, "Street cat"},
				{3, "Silly cat"}, 
				{4, "B&W cat"},
				{5, "Grey cat"},
				{6, "Black cat"},
				{7, "Ninja cat"},
				{8, "Sailor cat"},
				{9, "Siamese cat"},
				{10, "Rogue cat"},
				{11, "Scarcat"},
				{12, "Spotted cat"},
				{13, "Little cat"},
				{14, "Crying cat"},
				{15, "Seacat"},
				{16, "Grumpy cat"},
				{17, "The Pig cat"},
				{18, "Shinobi cat"},
				{19, "Lucky cat"},
				{20, "Pirate cat"},
				{21, "Sceptic cat"},
				{22, "Beach cat"},
				{23, "Captain Cat"},
				{24, "Deadpurr"},
				{25, "Unicorn cat"},
				{26, "Panda cat"},
				{27, "Cat with bow"},
				{28, "Blue cat"},
				{29, "Japacat"},
				{30, "Fishchu"},
				{31, "Donut cat"},
				{32, "Angry Meow"},
				{33, "Pikacat"},
				{34, "Jack Halloween"},
				{35, "Kitten Ash"}
			}
		}
	};

	private static Dictionary<Language, Dictionary<Rarity, string>> _localizedRarity = new Dictionary<Language, Dictionary<Rarity, string>>(){
		{ Language.RUS, new Dictionary<Rarity, string>()
			{
				{Rarity.COMMON, "Обычный"},
				{Rarity.RARE, "Редкий"},
				{Rarity.EPIC, "Очень редкий"}, 
				{Rarity.LEGENDARY, "Неуловимый"}
			}
		},
		{ Language.ENG, new Dictionary<Rarity, string>()
			{
				{Rarity.COMMON, "Base"},
				{Rarity.RARE, "Rare"},
				{Rarity.EPIC, "Very rare"}, 
				{Rarity.LEGENDARY, "Elusive"}
			}
		}
	};
	#endregion
	#region sprites
	private static Dictionary<Language, Dictionary<string, Sprite>> _localizedSprites = new Dictionary<Language, Dictionary<string, Sprite>>(){
		{Language.RUS, new Dictionary<string, Sprite>()
			{
				{"play_btn", Resources.Load<Sprite>("Image/btn_start_ru")},
				{"more_games_btn", Resources.Load<Sprite>("Image/btn_more_games_ru")}
			}		
		},
		{Language.ENG, new Dictionary<string, Sprite>()
			{
				{"play_btn", Resources.Load<Sprite>("Image/btn_start")},
				{"more_games_btn", Resources.Load<Sprite>("Image/btn_more_games")}
			}
		}
	};
	#endregion
	#region achievs
	private static Dictionary<Language, Dictionary<int, string>> _localizedAchiveNames = new Dictionary<Language, Dictionary<int, string>>(){
		{ Language.RUS, new Dictionary<int, string>()
			{
				{0, "Неизвестно"},
				{1, "Первое золото"},
				{2, "Первое серебро"},
				{3, "А мог быть шарфик"}, 
				{4, "А мог быть свитер"},
				{5, "Много свитеров"},
				{6, "Котейка"},
				{7, "Это целая коллекция!"},
				{8, "Не останавливайся!.."},
				{9, "Снайпер"},
				{10, "Виртуоз"},
				{11, "Невероятный"},
				{12, "Любитель"},
				{13, "Исследователь"},
				{14, "Путешественник"},
				{15, "Неудержимый"}
			}
		},
		{ Language.ENG, new Dictionary<int, string>()
			{
				{0, "Unknown"},
				{1, "The first gold!"},
				{2, "The first silver!"},
				{3, "A scarf can be"}, 
				{4, "Could be a sweater"},
				{5, "Too much sweaters"},
				{6, "Kitty!"},
				{7, "It's the whole collection!"},
				{8, "Don't stop!.."},
				{9, "Sniper"},
				{10,"Virtuoso"},
				{11,"Incredible"},
				{12, "Newbie"},
				{13, "Researcher"},
				{14,"Traveler"},
				{15,"Irrepressible"}
			}
		}
	};

	private static Dictionary<Language, Dictionary<int, string>> _localizedAchivesText = new Dictionary<Language, Dictionary<int, string>>(){
		{ Language.RUS, new Dictionary<int, string>()
			{
				{0, "Неизвестно"},
				{1, "Используйте золотой клубок"},
				{2, "Используйте серебряный клубок"},
				{3, "Потратьте 30 клубков"}, 
				{4, "Потратьте 500 клубков"},
				{5, "Потратьте 2000 клубков"},
				{6, "Поймайте кота"},
				{7, "Поймайте 40 котов"},
				{8, "Поймайте 200 котов"},
				{9, "Поймайте котика с первого раза"},
				{10, "Поймайте котика с 10го раза"},
				{11, "Изловите 50 котиков зa день"},
				{12, "Пройдите 10 км"},
				{13, "Пройдите 100 км"},
				{14, "Пройдите 500 км"},
				{15, "Пройдите 20 км за день"}
			}
		},
		{ Language.ENG, new Dictionary<int, string>()
			{
				{0, "Unknown"},
				{1, "Use the golden ball"},
				{2, "Use the silver ball"},
				{3, "Spend 30 any balls"}, 
				{4, "Spend 500 any balls"},
				{5, "Spend 2000 any balls"},
				{6, "Catch a cat"},
				{7, "Catch 40 cats"},
				{8, "Catch 200 cats"},
				{9, "Catch a cat with first attempt"},
				{10, "Catch a cat with 10th attempt"},
				{11, "Catch 50 cats per one day"},
				{12, "Pass 10 km"},
				{13, "Pass 100 km"},
				{14, "Pass 500 km"},
				{15, "Pass 20 km per one day"}
			}
		}
	};
	#endregion
	#region daily_quests
	private static Dictionary<Language, Dictionary<int, string>> _localizedDailyQuestNames = new Dictionary<Language, Dictionary<int, string>>(){
		{ Language.RUS, new Dictionary<int, string>()
			{
				{0, "Неизвестно"},
				{2, "Собери клубки"},
				{3, "Собери монеты"},
				{1, "Потрать клубки"}, 
				{4, "Найди котиков"},
				{5, "Пройди растояние"}
			}
		},
		{ Language.ENG, new Dictionary<int, string>()
			{
				{0, "Unknown"},
				{2, "Collect balls"},
				{3, "Collect cois"},
				{1, "Spend balls"}, 
				{4, "Collect cats"},
				{5, "Walk"}
			}
		}
	};
	#endregion

	public static string GetTextValue(string key){
		string _res = "";
		try{
			_res = _localizedTexts[activeLanguage][key];
		}catch(Exception e){
			CDebug.Log (e.StackTrace.ToString ());
		}
		return _res;
	}

	public static Sprite GetSpriteValue(string key){
		try{
			return _localizedSprites[activeLanguage][key];
		}catch(Exception e){
			CDebug.Log (e.StackTrace.ToString ());
			return null;
		}
	}

	public static string GetCatName(int id){
		string _res = "";
		try{
			_res = _localizedCats[activeLanguage][id];
		}catch(Exception e){
			CDebug.Log (e.StackTrace.ToString ());
		}
		return _res;
	}

	public static string GetRarityText(Rarity type){
		string _res = "";
		try{
			_res = _localizedRarity[activeLanguage][type];
		}catch(Exception e){
			CDebug.Log (e.StackTrace.ToString ());
		}
		return _res;
	}

	public static string GetAchiveName(int id){
		string _res = "";
		try{
			_res = _localizedAchiveNames[activeLanguage][id];
		}catch(Exception e){
			CDebug.Log (e.StackTrace.ToString ());
		}
		return _res;
	}

	public static string GetAchiveText(int id){
		string _res = "";
		try{
			_res = _localizedAchivesText[activeLanguage][id];
		}catch(Exception e){
			CDebug.Log (e.StackTrace.ToString ());
		}
		return _res;
	}

	public static string GetQuestText(int id){
		string _res = "";
		try{
			_res = _localizedDailyQuestNames[activeLanguage][id];
		}catch(Exception e){
			CDebug.Log (e.StackTrace.ToString ());
		}
		return _res;
	}
}

public enum Language{
	ENG = 0,
	RUS = 1
}
