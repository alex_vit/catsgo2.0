﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LocalizedText : Text{
	public string key;
	// Use this for initialization
	void Start () {
		if(!key.Equals(""))
			text = Localizator.GetTextValue (key);
	}
}
