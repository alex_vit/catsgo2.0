﻿using System;
using System.Collections.Generic;
using Assets;
using Assets.Helpers;
using Assets.Models;
using UnityEngine;
using System.Collections;
using Assets.Models.Factories;
using Map;
using System.Linq;
using UniRx;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class World : MonoBehaviour
{
	private static World _instance;
	public bool isInited;
	public Action OnInited;

    [SerializeField] 
	private Settings _settings;
	[SerializeField] 
	Text gpsData;
	[SerializeField] 
	Text playerData;
	[SerializeField] 
	Rect _centerCollider;

	public Vector2 tileSize = new Vector2(612f,612f);

    private TileManager _tileManager;
    private BuildingFactory _buildingFactory;
	private RoadFactory _roadFactory;
	private Vector2 _tileSize;
	private AnimatedPlayer _player;

	int _centerTileID;

	int tilesCounter = 0;

	void Awake(){
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
	}

	public void Init (AnimatedPlayer p)
    {
        _buildingFactory = GetComponentInChildren<BuildingFactory>();
        _roadFactory = GetComponentInChildren<RoadFactory>();    
        _tileManager = gameObject.AddComponent<TileManager>();
		_player = p;
		#if (!UNITY_EDITOR)
		EnableLocation();
		#else
		DrawMap(_settings);
		#endif


	}
	/// <summary>
	/// Gets the instance.
	/// </summary>
	/// <returns>The instance.</returns>
	public static World GetInstance(){
		return _instance;
	}

	public void Stop(){
		_tileManager.Stop ();
	}
		
	private Settings SetLocation(Vector2 pos){
		Settings _set = new Settings ();
		_set.Lat = (float)pos.x;
		_set.Long = (float)pos.y;
		_set.LoadImages = false;
		_set.Range = 1;
		_set.DetailLevel = 16;
		return _set;
	}

	public void IncreaseTileCounter(){
		gpsData.text = "Current loaded tiles " + (++tilesCounter).ToString ();
	}

	public Vector3 GetTilesCenter(){
		return _tileManager.GetCenterTileParams().GetInfo().TileCenter;
	}

	public int GetCenterTileID(){
		return _tileManager.GetCenterTileParams ().ID;
	}

	public Bounds GetCenterTileBounds(){
		return _tileManager.GetCenterTileParams ().GetBounds ();
	}

	public Tile GetCenterTile(){
		return _tileManager.GetCenterTileParams ();
	}

	#region enable location
	void EnableLocation(){
		if (GPSReciever.GetInstance ().TryEnableGPS ()) {
			//gpsData.text = (float)(GPSReciever.GetInstance ().coords.x) + " " + (float)(GPSReciever.GetInstance ().coords.y);
			Invoke ("DefinePosition", 2f);
		}
	}
		
	void DefinePosition(){
		Settings _set = SetLocation (new Vector2 (GPSReciever.GetInstance ().coords.x, GPSReciever.GetInstance ().coords.y));
		DrawMap (_set);
	}
	#endregion

	void SetPlayer(){
		//_player.GetComponent<Player> ().SetOnMap ();
		//_player.OnTileChanged += UpdateTiles;
		//StartCoroutine (UpdatePlayerPos());
		isInited = true;
		if (OnInited != null)
			OnInited ();
		playerData.text = "Center tile " + GetCenterTileID ();
	}

	void DrawMap(Settings point){
		gpsData.text = (float)(point.Lat) + " " + (float)(point.Long);
		_tileManager.Init (_buildingFactory, _roadFactory, point);
		_tileSize = _tileManager.GetListTile ().Values.First ().rect.size;
		_centerCollider = new Rect (Vector2.zero - _tileManager.GetListTile ().Values.First ().rect.size / 2, _tileManager.GetListTile ().Values.First ().rect.size);
		Invoke ("SetPlayer", 10f);
	}

	public Vector2 CalculateNewPlayerPos(){
		Vector3 _tilePos = GetTilesCenter ();
		#if(!UNITY_EDITOR)
		Vector2 _playerPos = GPSReciever.GetInstance().coords;
		#else
		Vector2 _playerPos = new Vector2(_settings.Long, _settings.Lat);
		#endif
		playerData.text = _playerPos.ToString();
		//Vector2 _playerPos = pos;
		var dotMerc = GeoConverter.LatLonToMeters ((float)_playerPos.x, (float)_playerPos.y);
		var resPos = new Vector2 (dotMerc.x - _tilePos.x, dotMerc.y - _tilePos.y);
		//_player.transform.position = new Vector3 (localMercPos.x, _player.transform.position.y, localMercPos.y);
		return resPos;
		//playerData.text = player.transform.position.ToString();
	}

	public void UpdateTiles()
	{//player movement in TMS tiles
		var tileDif = GetMovementVector();
		//move locals
		Centralize(tileDif);
		//create new tiles
		_tileManager.LoadTiles(_tileManager.centerTms, _tileManager.centerInMercator);
		playerData.text = "Update tiles";
		//UnloadTiles(CenterTms);
		Invoke ("SetPlayer", 2f);
	}



	private Vector2 GetMovementVector()
	{
		var dif = _player.GetTransform().position.ToVector2xz();
		var tileDif = Vector2.zero;
		if (dif.x < Math.Min(_centerCollider.xMin, _centerCollider.xMax))
			tileDif.x = -1;
		else if (dif.x > Math.Max(_centerCollider.xMin, _centerCollider.xMax))
			tileDif.x = 1;

		if (dif.y < Math.Min(_centerCollider.yMin, _centerCollider.yMax))
			tileDif.y = 1;
		else if (dif.y > Math.Max(_centerCollider.yMin, _centerCollider.yMax))
			tileDif.y = -1; //invert axis  TMS vs unity
		return tileDif;
	}

	private void Centralize(Vector2 tileDif)
	{
		//move everything to keep current tile at 0,0
		foreach (var tile in _tileManager.GetListTile().Values)
		{
			tile.transform.position -= new Vector3(tileDif.x * _tileSize.x, 0, tileDif.y * _tileSize.y);
		}
		_tileManager.centerTms += tileDif;
		_tileManager.centerInMercator = GeoConverter.TileBounds(_tileManager.centerTms, _settings.DetailLevel).center;
		var difInUnity = new Vector3(tileDif.x*_tileSize.x, 0, tileDif.y*_tileSize.y);
		_player.GetTransform().position -= difInUnity;
	}

	[Serializable]
	public class Settings
	{
		[SerializeField]
		public float Lat = 39.921864f;
		[SerializeField]
		public float Long = 32.818442f;
		[SerializeField]
		public int Range = 1;
		[SerializeField]
		public int DetailLevel = 16;
		[SerializeField]
		public bool LoadImages = false;
	}
}
