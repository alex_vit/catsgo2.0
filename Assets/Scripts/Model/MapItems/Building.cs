﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Helpers;
using UnityEngine;

namespace Assets.Models
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class Building : MonoBehaviour
    {
        private List<Vector3> _verts;

        public Vector3 Center;
        public string LanduseKind;
        public string Name;

        public void Init(Vector3 buildingCenter, List<Vector3> buildingCorners, string kind)
        {
            LanduseKind = kind;
            Center = buildingCenter;
            _verts = buildingCorners;
            GetComponent<MeshFilter>().mesh = CreateMesh(_verts);
			/*if (LanduseKind != "")
				GetComponent<MeshRenderer> ().material = Resources.Load<Material> ("Textures/Material/" + LanduseKind);
			else
				GetComponent<MeshRenderer> ().material = Resources.Load<Material> ("Textures/Material/NoName");*/
			GetComponent<MeshRenderer> ().material = StaticData.MapMaterial ["Building"];
        }
        
        private static Mesh CreateMesh(List<Vector3> verts)
        {
            var tris = new Triangulator(verts.Select(x => x.ToVector2xz()).ToArray());
            var mesh = new Mesh();

            var vertices = verts.Select(x => new Vector3(x.x, 0, x.z)).ToList();
            var indices = tris.Triangulate().ToList();
            //var uv = new List<Vector2>();

            var n = vertices.Count;
            for (int index = 0; index < n; index++)
            {
                var v = vertices[index];
                vertices.Add(new Vector3(v.x, 0, v.z));
            }

            for (int i = 0; i < n - 1; i++)
            {
                indices.Add(i);
                indices.Add(i + n);
                indices.Add(i + n + 1);
                indices.Add(i);
                indices.Add(i + n + 1);
                indices.Add(i + 1);
            }

            indices.Add(n - 1);
            indices.Add(n);
            indices.Add(0);

            indices.Add(n - 1);
            indices.Add(n + n - 1);
            indices.Add(n);



            mesh.vertices = vertices.ToArray();
            mesh.triangles = indices.ToArray();

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            return mesh;
        }

		private static Mesh Create3DMesh(List<Vector3> verts)
		{
			var lowVerts = verts;
			//create mass of verts with height
			var highVerts = new Vector3[lowVerts.Count];
			for (int i = 0; i < lowVerts.Count; i++) {
				highVerts [i].x = lowVerts [i].x;
				highVerts [i].y = 50;
				highVerts [i].z = lowVerts [i].z;
			}
				
			//v//ar trisLow = new Triangulator(verts.Select(x => x.ToVector2xz()).ToArray());
			Mesh wallMesh = new Mesh();

			//calculate triangles count as we know polygon tris = N -2;
			int tris_count = (lowVerts.Count - 2)*2 + lowVerts.Count*2;
			List<int> indexes = new List<int> ();
			//result vertices mass
			var wallVertices = new List<Vector3>();
			var count_verts = lowVerts.Count;

			/*for (int i = 0; i < count_verts; i++) {
				totalVertices [i] = lowVerts [i];
			}
			for (int i = 0; i < count_verts; i++) {
				totalVertices [count_verts + i] = highVerts [i];
			}*/
			wallVertices.AddRange (lowVerts.ToList ());
			wallVertices.AddRange (highVerts.ToList ());
			//totalVertices.AddRange (highVerts.ToList ());
			//init indexes;

			//draw n-1 side
			for(int i=0; i< lowVerts.Count -1; i++){
				indexes.Add (i); 
				indexes.Add (i + 1);
				indexes.Add (i + lowVerts.Count + 1);
				indexes.Add (i);
				indexes.Add (i + lowVerts.Count + 1);
				indexes.Add (i + lowVerts.Count);
			}
			//draw last side
			indexes.Add (lowVerts.Count-1);
			indexes.Add (0);
			indexes.Add (lowVerts.Count);
			indexes.Add (lowVerts.Count -1);
			indexes.Add (lowVerts.Count);
			indexes.Add (lowVerts.Count * 2-1);
			//draw roof

			//wallMesh.vertices = totalVertices.ToArray ();
			//wallMesh.triangles = indexes.ToArray ();

			var num = lowVerts.Count;
			/*for (int i = 0; i < lowVerts.Count - 1; i++)
			{
				indexes.Add(i + lowVerts.Count);
				indexes.Add(i + 1 + lowVerts.Count);
				indexes.Add(num - 1 + lowVerts.Count);
				indexes.Add(i + lowVerts.Count);
				indexes.Add(num - 1 + lowVerts.Count);
				indexes.Add(num + lowVerts.Count);
			}*/

			/*for (int i = 0; i < lowVerts.Count - 1; i++) {
				indexes.Add(i + lowVerts.Count);
				indexes.Add(i + 1 + lowVerts.Count);
				indexes.Add(num - 1 + lowVerts.Count);
				indexes.Add(i + lowVerts.Count);
				indexes.Add(num - 1 + lowVerts.Count);
				indexes.Add(num + lowVerts.Count);
			}

			indexes.Add (num - 1 + lowVerts.Count);
			indexes.Add (num + lowVerts.Count);
			indexes.Add (lowVerts.Count);

			indexes.Add(2*lowVerts.Count - 1);
			indexes.Add(3*lowVerts.Count - 1);
			indexes.Add(2*lowVerts.Count);*/

		//draw roof mesh
			Mesh roofMesh = new Mesh();

			var tris = new Triangulator(highVerts.Select(x => new Vector2(x.x, x.z)).ToArray());

			//var vertices = highVerts.Select(x => new Vector3(x.x, x.y, x.z)).ToList();
		List<Vector3> vetsData = new List<Vector3>();
		for (int i = 0; i < highVerts.Count(); i++) {
			vetsData.Add( new Vector3 (highVerts [i].x, highVerts [i].y, highVerts [i].z));
		}
		var roofVertices = vetsData.ToList();
			var indices = tris.Triangulate().ToList();
			//var uv = new List<Vector2>();

			var n = roofVertices.Count;
			for (int index = 0; index < n; index++)
			{
			var v = roofVertices[index];
			roofVertices.Add(new Vector3(v.x, v.y, v.z));
			}

			for (int i = 0; i < n - 1; i++)
			{
				indices.Add(i);
				indices.Add(i + n);
				indices.Add(i + n + 1);
				indices.Add(i);
				indices.Add(i + n + 1);
				indices.Add(i + 1);
			}

			indices.Add(n - 1);
			indices.Add(n);
			indices.Add(0);

			indices.Add(n - 1);
			indices.Add(n + n - 1);
			indices.Add(n);

		//indexes.Add(2*lowVerts.Count - 1);
		//indexes.Add(3*lowVerts.Count - 1);
		//indexes.Add(2*lowVerts.Count);

			//totalVertices.AddRange (vertices);
			//indexes.AddRange (indices);

		roofMesh.vertices = roofVertices.ToArray();
		roofMesh.triangles = indices.ToArray();

		//up roof mesh;
		for(int i=0; i< roofMesh.normals.Length; i++){
			roofMesh.normals [i] = new Vector3 (roofMesh.normals [i].x, 100, roofMesh.normals [i].z);
		}

			/*CombineInstance[] combine = new CombineInstance[2];
			
			combine [0].mesh = wallMesh;
			combine [1].mesh = roofMesh;*/
		Mesh result = new Mesh ();
			List<Vector3> totalVerts = new List<Vector3> ();
			totalVerts.AddRange (wallVertices);
			totalVerts.AddRange (roofVertices);

			List<int> totalIndices = new List<int> ();
			totalIndices.AddRange (indexes);
			totalIndices.AddRange (indices);
			
			result.vertices = totalVerts.ToArray();
			result.triangles = totalIndices.ToArray ();

		//roofMesh.RecalculateNormals();
			result.RecalculateBounds();

			return result;
		}
    }
}
