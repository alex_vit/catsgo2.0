﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Assets.Helpers;
using Assets.Models;
using Assets.Models.Factories;
using UnityEngine;

namespace Map
{
    public class TileManager : MonoBehaviour
    {
        private BuildingFactory _buildingFactory;
        private RoadFactory _roadFactory;
        private Transform _tileHost;

        private bool _loadImages;
        private int _zoom = 16; //detail level of TMS system
        private string _key = "vector-tiles-5sBcqh6"; //try getting your own key if this doesn't work
        private Dictionary<Vector2, Tile> _tiles; //will use this later on
		//private Dictionary<int, Tile> _tiles;
        public Vector2 centerTms; //tms tile coordinate
        public Vector2 centerInMercator; //this is like distance (meters) in mercator
		private Tile _centerTile;
		World.Settings _settings;

        public void Init(BuildingFactory buildingFactory, RoadFactory roadFactory, World.Settings settings)
        {
			_settings = settings;
            var v2 = GeoConverter.LatLonToMeters(settings.Lat, settings.Long);
            var tile = GeoConverter.MetersToTile(v2, settings.DetailLevel);

            _tileHost = new GameObject("Tiles").transform;
            _tileHost.SetParent(transform, false);

            _buildingFactory = buildingFactory;
            _roadFactory = roadFactory;
			_tiles = new Dictionary<Vector2, Tile>();
            centerTms = tile;
			Debug.Log ("center " + centerTms);
            centerInMercator = GeoConverter.TileBounds(centerTms, _zoom).center;
			CDebug.Log (_settings.DetailLevel);
            _zoom = settings.DetailLevel;
            _loadImages = settings.LoadImages;

			var index = 0;
            for (int i = -settings.Range; i <= settings.Range; i++)
            {
                for (int j = -settings.Range; j <= settings.Range; j++)
                {  
					var v = new Vector2(centerTms.x + i, centerTms.y + j);
                    var _centerInMercator = GeoConverter.TileBounds(centerTms, _zoom).center;
					bool isCenter = i == 0 && j == 0;
					StartCoroutine(CreateTile(isCenter,index++, v, _centerInMercator));
                }
            }

			_centerTile = _tiles [centerTms];
			CDebug.Log ("Center tile id " + _centerTile.ID);
        }

		public void LoadTiles(Vector2 tms, Vector2 center)
		{
			var index = _tiles.Count;
			for (int i = -_settings.Range; i <= _settings.Range; i++)
			{
				for (int j = -_settings.Range; j <= _settings.Range; j++)
				{  
					var v = new Vector2(tms.x + i, tms.y + j);
					bool isCenter = i == 0 && j == 0;
					if (_tiles.ContainsKey (v))
						continue;
					StartCoroutine(CreateTile(isCenter,index++, v, center));
				}
			}

			_centerTile = _tiles [centerTms];
		}

		public Tile GetCenterTileParams(){
			return _centerTile;
		}

		private IEnumerator CreateTile(bool isCentral, int id, Vector2 tileTms, Vector2 centerInMercator)
        {
            var rect = GeoConverter.TileBounds(tileTms, _zoom);
            var tile = new GameObject("tile " + tileTms.x + "-" + tileTms.y).AddComponent<Tile>();
			tile.OnLoaded += CheckLoad;
            //_tiles.Add(tileTms, tile);
			_tiles.Add(tileTms, tile);
            tile.transform.SetParent(_tileHost, true);

			tile.CreateTile(id, _buildingFactory, _roadFactory, 
				new Tile.Settings()
				{
					Zoom = _zoom,
					TileTms = tileTms,
					TileCenter = rect.center,
					LoadImages = _loadImages
				});
						
			//if (isCentral) {
				//_centerTile = tile;
			//}
            tile.transform.position = (rect.center - centerInMercator).ToVector3xz();
			//tile.transform.position = centerInMercator.ToVector3xz ();
            yield return null;
        }

		public Dictionary<Vector2, Tile> GetListTile(){
			return _tiles;		
		}

		public void CheckLoad(){
			World.GetInstance ().IncreaseTileCounter ();
		}

		public void Stop(){
			//foreach(Tile _tile in _tiles){
				//_tile.StopAllCoroutines ();
			//}
		}
    }


}
