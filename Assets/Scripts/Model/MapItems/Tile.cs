﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Assets.Helpers;
using Assets.Models;
using Assets.Models.Factories;
using UnityEngine;
using UniRx;

namespace Map
{
    public class Tile :MonoBehaviour
    {
		public int ID;
		public Rect rect;

        private BuildingFactory _buildingFactory;
        private RoadFactory _roadFactory;

        private Settings _settings;
		private Bounds mapRect;

		public Action OnLoaded;

        public void CreateTile(int id, BuildingFactory buildingFactory, RoadFactory roadFactory, Settings settings)
        {
			ID = id;
            _buildingFactory = buildingFactory;
            _roadFactory = roadFactory;
            _settings = settings;
            
            rect = GeoConverter.TileBounds(_settings.TileTms, _settings.Zoom);
			#if UNITY_EDITOR
			Load(_define.mapDebugKey, _settings.Zoom, _settings.TileTms);
			#else
			Load(_define.mapKeys[UnityEngine.Random.Range(0, _define.mapKeys.Length)], _settings.Zoom, _settings.TileTms);
			#endif
        }

		public Tile.Settings GetInfo(){
			return _settings;
		}

		public Bounds GetBounds(){
			//Debug.Log ("bounds " + mapRect.min.x + " " + mapRect.max.x + " " + mapRect.min.z + " " + mapRect.max.z);
 			return mapRect;
		}

        private void Load(string key, int zoom, Vector2 tileTms)
        {
			bool isSuccessLoadMap;
			var template = "http://82.196.15.209/osm/{0}/{1}/{2}/{3}.{4}?api_key={5}";
            var layers = "buildings,roads";
            var format = "json";

            var url = string.Format(template, layers, zoom, tileTms.x, tileTms.y, format, key);
			//isSuccessLoadMap = LoadData (url);
			Debug.Log(url);
			LoadData (url);
        }

		private void LoadData(string url){
			if (StaticData.TilesData.ContainsKey (_settings.TileTms)) {
				//construct tile from data
				CDebug.Log ("already loaded");
				StartCoroutine (ConstructTile (_settings.TileTms, _settings.TileCenter, StaticData.TilesData [_settings.TileTms]));
			} else {
				//load data from WWW and store it to StaticData dictionary
				//Debug.Log(url);
				ObservableWWW.Get(url)
					.Subscribe(
						osmJson =>
						{
							StartCoroutine(ConstructTile(_settings.TileTms, _settings.TileCenter, osmJson));
						}, //success
						Debug.LogException);				
			}
		}

        private IEnumerator ConstructTile(Vector2 tilePos, Vector2 mercPos, string text)
		{
			if (text != "") {
				CDebug.Log ("tile build");
				if (!StaticData.TilesData.ContainsKey (tilePos)) {
					StaticData.TilesData.Add (tilePos, text);
				}
				JSONObject mapData;
				string url;
				mapData = new JSONObject (text);

            
				var go = GameObject.CreatePrimitive (PrimitiveType.Plane);
				go.name = "map";
				go.tag = _define.mapTag;

				Transform _goTrans = go.transform;
				Renderer _goRend = go.GetComponent<Renderer> ();

				_goTrans.localScale = new Vector3 (rect.width / 10, 1, rect.width / 10); 
				_goTrans.rotation = Quaternion.AngleAxis (180, new Vector3 (0, 1, 0));
				_goTrans.parent = this.transform;
				_goTrans.localPosition = Vector3.zero;
				_goTrans.localPosition -= new Vector3 (0, 10, 0);
				mapRect = _goRend.bounds;
				//go.GetComponent<Renderer>().material.mainTexture = new Texture2D(512, 512, TextureFormat.DXT5, false);
				//go.GetComponent<Renderer>().material.color = new Color(.1f, .1f, .1f, 1f);
				_goRend.material = StaticData.MapMaterial ["Map"];
				if (_settings.LoadImages) {
					Texture2D _tex = new Texture2D (64, 64);
					_goRend.material.color = new Color (1f, 1f, 1f, 1f);
					//url = "http://b.tile.openstreetmap.org/" + _settings.Zoom + "/" + tilePos.x + "/" + tilePos.y + ".png";
					url = "http://c.tiles.wmflabs.org/osm-no-labels/" + _settings.Zoom + "/" + tilePos.x + "/" + tilePos.y + ".png";
					CDebug.Log (url);
					ObservableWWW.GetWWW (url).Subscribe (x => {
						//x.LoadImageIntoTexture ((Texture2D)_goRend.material.mainTexture);
						x.LoadImageIntoTexture(_tex);
					});
					_goRend.material.mainTexture = _tex;
				}
				yield return null;
				Debug.Log ("mercPos " + mercPos);
				StartCoroutine (CreateBuildings (mapData ["buildings"], mercPos));
				StartCoroutine (CreateRoads (mapData ["roads"], mercPos));

				if (OnLoaded != null)
					OnLoaded ();

				/*Analytics.LogEvent (_define.mapLoadKey, new Dictionary<string, object> () {
					{ _define.mapLoadKey, true}
				});*/
			} else {
				Analytics.LogEvent (_define.mapLoadKey, new Dictionary<string, object> () {
					{ _define.mapLoadKey, false}
				});
			}
		}
			

        private IEnumerator CreateBuildings(JSONObject mapData, Vector2 tileMercPos)
        {
            foreach (var geo in mapData["features"].list.Where(x => x["geometry"]["type"].str == "Polygon"))
            {
                _buildingFactory.CreateBuilding(tileMercPos, geo, transform);
                yield return null;
            }
        }

        private IEnumerator CreateRoads(JSONObject mapData, Vector2 tileMercPos)
        {
            for (int index = 0; index < mapData["features"].list.Count; index++)
            {
                var geo = mapData["features"].list[index];
                _roadFactory.CreateRoad(tileMercPos, geo, index, transform);
                yield return null;
            }
        }

        public class Settings
        {
            public int Zoom { get; set; }
            public Vector2 TileTms { get; set; }
            public Vector3 TileCenter { get; set; }
            public bool LoadImages { get; set; }
        }
    }
}
