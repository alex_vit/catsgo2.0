﻿using UnityEngine;
using System.Collections;

public class BallsPull : MonoBehaviour {
	static BallsPull _instance;
	[SerializeField]
	AnimatedBall[] pull;
	public float distance;
	public int defaultIndex;

	int mCurrIndex;

	bool isLock;
	void Awake(){
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {
		SetDefault ();
		mCurrIndex = defaultIndex;
	}

	public static BallsPull GetInstance(){
		return _instance;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Run(){
		mCurrIndex = defaultIndex;
		SetDefault ();
	}

	void SetDefault(){
		SelectBall (mCurrIndex);
	}

	public void SelectNext(){
		if (mCurrIndex < pull.Length-1) {
			Mathf.Clamp (++mCurrIndex, 0, pull.Length - 1);
			for (int i = 0; i < pull.Length; i++) {
				pull [i].gameObject.SetActive (false);
			}
			SelectBall (mCurrIndex);
		}
	}

	public void SelectPrev(){
		if (mCurrIndex > 0) {
			Mathf.Clamp (--mCurrIndex, 0, pull.Length - 1);
			for (int i = 0; i < pull.Length; i++) {
				pull [i].gameObject.SetActive (false);
			}
			SelectBall (mCurrIndex);
		}
	}

	void SelectBall(int index){
		for (int i = 0; i < pull.Length; i++) {
			pull [i].gameObject.SetActive (false);
		}
		pull [index].gameObject.SetActive (true);
		ThrowController.GetInstance ().SetBall (pull [index]);
	}
}
