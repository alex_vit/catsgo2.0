﻿using UnityEngine;
using System.Collections;
using System;

public class Player {
	PlayerInfo mInfo;

	int _curXp;
	public int Xp{
		get{ 
			return _curXp;
		}
		set{ 
			_curXp = value;
			CheckStats ();
		}
	}

	public Action  OnInfoChanged;

	public Player(){
		mInfo = new PlayerInfo ();	
	}

	public Player(PlayerInfo info){
		mInfo = new PlayerInfo ();
		mInfo.level = info.level;
		mInfo.name = info.name;
		mInfo.currentXp  = info.currentXp;
	}

	public PlayerInfo GetInfo(){
		return mInfo;		
	}

	void CheckStats (){
		//check if xp more than xp needed for next level than increase level
		{
			mInfo.nextLevelXp = 100 + mInfo.level + (mInfo.level + 1) * 13;

			if (_curXp >= mInfo.nextLevelXp) {
				mInfo.level++;
				CDebug.Log ("increase level " + mInfo.level);
				_curXp = 0;
				mInfo.money += 10;
				mInfo.nextLevelXp = 100 + mInfo.level + (mInfo.level + 1) * 13;
			}

			if (OnInfoChanged != null) {
				OnInfoChanged ();
			}
		}
	}
}


