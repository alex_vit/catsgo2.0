﻿using UnityEngine;
using System.Collections;
using Map;
using System;

public class AnimatedPlayer : MonoBehaviour {
	Rigidbody mBody;
	Transform mTransform;
	public Action OnTileChanged;
	RaycastHit[] mInfo;
	Vector3 mRayDirection = new Vector3(0,-1,0);
	[SerializeField]
	GameObject detectArea;

	public Action OnSetActive;
	// Use this for initialization
	void Awake(){
		//mBody = GetComponent<Rigidbody> ();
		mTransform = transform;
		SetInActive ();
	}

	public Transform GetTransform(){
		return mTransform;
	}

	public void SetActive(){
		detectArea.SetActive (true);
		if (OnSetActive != null)
			OnSetActive ();
	}

	public void SetInActive(){
		detectArea.SetActive (false);
	}

	/*void OnCollisionEnter(Collision coll){
		if (coll.gameObject.tag == _define.mapTag) {
			if (coll.transform.parent.GetComponent<Tile> ().ID != World.GetInstance ().GetCenterTileID ()) {
				CDebug.Log ("change tile");
				if (OnTileChanged != null)
					OnTileChanged ();
			}
		}
	}*/

	public void CheckPosition(){
		mInfo = Physics.RaycastAll (mTransform.position, mRayDirection, 100);

		for (int i = 0; i < mInfo.Length; i++) {
			if (mInfo [i].collider.gameObject.tag == _define.mapTag) {
				if (mInfo [i].collider.transform.parent.GetComponent<Tile> ().ID != World.GetInstance ().GetCenterTileID ()) {
					CDebug.Log ("change tile");
					if (OnTileChanged != null)
						OnTileChanged ();
				}
			}
		}
	}
}
