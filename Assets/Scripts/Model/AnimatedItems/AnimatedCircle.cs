﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnimatedCircle : MonoBehaviour {
	RectTransform mTransform;
	Animator mAnim;
	Image mImage;
	WaitForFixedUpdate mFixedUpdate;

	void Awake(){
		mTransform = GetComponent<RectTransform> ();
		mAnim = GetComponent<Animator> ();
		mImage = GetComponent<Image> ();
		//circleImage.enabled = false;
	}

	public void Show(){
		mImage.enabled = true;
		mAnim.SetBool ("isPulse", true);
	}

	public void Hide(){
		mAnim.SetBool ("isPulse", false);
		mImage.enabled = false;
	}
}
