﻿using UnityEngine;
using System;
using System.Collections;

public class AnimatedBall : MonoBehaviour {
	bool isDraggable = false;
	#region cached
	Rigidbody mRigid;
	Transform mTransform;
	#endregion
	Vector3 mDefaultPos;
	Quaternion mDefaultRot;
	BallState mState;

	public BallType balltype;

	public Action OnCatched;
	public Action OnFallen;
	public Action OnReady;

	int stickness = 5;
	// Use this for initialization
	void Awake () {
		mRigid = GetComponent<Rigidbody> ();
		mTransform = transform;
		mDefaultPos = mTransform.position;
		mDefaultRot = mTransform.rotation;
		gameObject.SetActive (false);
	}

	/// <summary>
	/// Sets the default.
	/// </summary>
	public void SetDefault(){
		gameObject.SetActive (true);
		mRigid.isKinematic = true;
		mTransform.position = mDefaultPos;
		mTransform.rotation = mDefaultRot;
		mState = BallState.READY;

		if (OnReady != null)
			OnReady ();
	}

	public void UpdateDefault(){
		mDefaultPos = new Vector3 (mTransform.position.x, mDefaultPos.y, mDefaultPos.z);
	}
	/// <summary>
	/// Takes the ball.
	/// </summary>
	void Take(){
		isDraggable = true;
	}

	public BallState GetState(){
		return mState;
	}

	public int GetStickness(){
		return stickness;
	}
	/// <summary>
	/// Releases the ball. and  throw it in direction
	/// </summary>
	public void Throw(Vector3 forceValue){
		mRigid.isKinematic = false;
		mRigid.AddForce (mTransform.forward * forceValue.z + mTransform.right *forceValue.x  + Vector3.up * forceValue.y, ForceMode.Impulse);
		mState = BallState.FLY;
		//Invoke ("Fall", 2f);
		//mRigid.AddForce (mTransform.right *forceValue.x  + Vector3.up * forceValue.y, ForceMode.Impulse);
	}

	public Vector3 GetPosition(){
		return mTransform.position;
	}

	public Transform GetTransform(){
		return mTransform;
	}

	void OnCollisionEnter(Collision coll){
		//if ball falls on floor sets it to default position
		if (coll.gameObject.tag == _define.floorTag && mState == BallState.FLY) {
			mState = BallState.LAY;
			Fall ();
		}	
	}

	void OnTriggerEnter(Collider coll){
		//if (coll.gameObject.tag == _define.catTag && mState == BallState.FLY) {
			//if(coll.GetComponent<BallDetector>().IsAvailable())
				//Catch ();
		//}
		if (coll.gameObject.tag == _define.wallTag && mState == BallState.FLY) {
			Fall ();
		}
	}
	/// <summary>
	/// Catch this instance.
	/// </summary>
	void Catch(){
		if (OnCatched != null)
			OnCatched ();
	}

	void Fall(){
		if (OnFallen != null)
			OnFallen ();
		SetDefault ();
	}

	public void Show(){
		gameObject.SetActive (true);
	}

	public void Hide(){
		gameObject.SetActive (false);
		Invoke ("Fall", 0.2f);
	}
}

public enum BallState{
	READY,
	FLY,
	LAY
}


