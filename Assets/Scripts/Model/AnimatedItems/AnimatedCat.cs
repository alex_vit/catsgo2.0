﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class AnimatedCat : MonoBehaviour {
	[SerializeField]
	Text verbalText;

	RectTransform mTransform;
	Vector2 mDefaultPos;
	Animator mAnim;
	Image mImage;

	Vector2 escapeDir;

	public Action OnEscaped;
	public Action OnCatched;

	Vector2 screenBorder = new Vector2(360, 640);
	// Use this for initialization
	void Awake () {
		mTransform = GetComponent<RectTransform>();
		mDefaultPos = mTransform.anchoredPosition;
		mImage = GetComponent<Image> ();
		mAnim = GetComponent<Animator> ();
		mAnim.enabled = false;
	}

	public void Init(int ID){
		Debug.Log ("init drawed cat");
		mImage.sprite = StaticData.CatImages [ID];
	}

	public void SetPosition(Vector2 pos){
		mTransform.anchoredPosition = pos;
		mAnim.enabled = true;
	}

	public void Catch(){
		mAnim.enabled = false;
		StopAllCoroutines ();
		StartCoroutine(Catch(StaticData.CatImages[-1]));
	}

	public void Escape(){
		//escapeDir.x = (UnityEngine.Random.Range (1000, 5000) | UnityEngine.Random.Range(-1000, -5000));
		//escapeDir.y = (UnityEngine.Random.Range (1000, 5000) | UnityEngine.Random.Range (-1000, -5000));
		//StopAllCoroutines ();
		//StartCoroutine (Move (escapeDir));
		StopAllCoroutines();
		if (OnEscaped != null)
			OnEscaped ();
	}

	public void Laugh(){
		verbalText.text = Localizator.GetTextValue ("cat_" + UnityEngine.Random.Range (0, 11));
		verbalText.gameObject.SetActive (true);
		Invoke ("HideText", 1.5f);
	}

	public void Miss(){
		verbalText.text = Localizator.GetTextValue (_define.missText);
		verbalText.gameObject.SetActive (true);
		Invoke ("HideText", 0.5f);
	}

	void HideText(){
		verbalText.gameObject.SetActive(false);
	}

	public Vector3 GetPosition(){
		return mTransform.position;
	}

	public void RotateToCam(Quaternion rot){
		mTransform.localRotation = rot;
	}

	IEnumerator Move(Vector2 pos){
		Vector2 _pos;
		while (true) {
			if (Math.Abs (mTransform.anchoredPosition.x) >= screenBorder.x || Math.Abs (mTransform.anchoredPosition.y) >= screenBorder.y) {
				if (OnEscaped != null)
					OnEscaped ();
				break;
			}
			_pos.x = Mathf.Lerp (mTransform.anchoredPosition.x, pos.x, Time.deltaTime*2);
			_pos.y = _pos.x / 3;
			mTransform.anchoredPosition = _pos;
			yield return new WaitForFixedUpdate ();
		}
	}

	IEnumerator Catch(Sprite catchSprite){
		float _step = 0.05f;
		Color _color = new Color();
		while (true) {
			if (mImage.color.a >= 0.9f && mImage.sprite == catchSprite) {
				_color = new Color (mImage.color.r, mImage.color.g, mImage.color.b, 1f);
				mImage.color = _color;
				if (OnCatched != null)
					OnCatched ();
				break;
			}
			if (mImage.sprite != catchSprite) {
				_color = new Color (mImage.color.r, mImage.color.g, mImage.color.b, (mImage.color.a - _step));
				mImage.color = _color;
				if (mImage.color.a <= 0)
					mImage.sprite = catchSprite;
			} else {
				_color = new Color (mImage.color.r, mImage.color.g, mImage.color.b, (mImage.color.a + _step));
				mImage.color = _color;
			}
			yield return new WaitForFixedUpdate ();
		}
	}
}
