﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wazzapps.Purchasing;

public class Store : MonoBehaviour {
	static Store _instance;
	[SerializeField]
	StoreElement[] storeElements;

	public static readonly Dictionary<int, string> PurchasesId = new Dictionary<int, string>(){
		{1, _define.oneSimpleBallId},
		{2, _define.tenSimpleBallId},
		{3, _define.fiftySimpleBallId},
		{4, _define.oneSilverBallId},
		{5, _define.tenSilverBallId},
		{6, _define.fiftySilverBallId},
		{7, _define.oneGoldenBallId},
		{8, _define.tenGoldenBallId},
		{9, _define.fiftyGoldenBallId},
		{10, _define.freeCoinsId},
		{11, _define.smallCoinsPackId},
		{12, _define.middleCoinsPackId},
		{13, _define.largeCoinsPackId},
		{14, _define.noAdsId}
	};

	void Awake(){
		if(_instance == null){
			_instance = this;
		}else
			Destroy(gameObject);
	}

	void Start(){
		FillCost ();
	}

	public void FillCost(){
		string _tmpId;
		for (int i = 0; i < storeElements.Length; i++) {
			_tmpId = PurchasesId[storeElements[i].buttonID];
			if (storeElements [i].costType == CostType.COINS)
				storeElements [i].SetCost (StaticData.PurchaseItemInfo[_tmpId].GetInfo().cost);
			else if (storeElements [i].costType == CostType.MONEY)
				storeElements [i].SetCost (Purchaser.GetInsance().GetProductLocalizedPriceByID(_tmpId));
		}
	}
}
