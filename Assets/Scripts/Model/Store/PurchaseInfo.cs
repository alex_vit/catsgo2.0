﻿using UnityEngine;
using System.Collections;
using System;

public class PurchaseInfo {
	protected ShopItemInfo mInfo;

	public virtual ShopItemInfo GetInfo(){
		return mInfo;	
	}
}

public class ActionPurchaseInfo : PurchaseInfo{
	Action mRewardAction;

	public ActionPurchaseInfo(CostType costType, PurchaseType type, int cost, Action action){
		mInfo.costType = costType;
		mInfo.type = type;
		mInfo.cost = cost;
		mRewardAction = action;
	}

	public override ShopItemInfo GetInfo(){
		return mInfo;
	}

	public void DoAction(){
		if (mRewardAction != null)
			mRewardAction ();
	}
}

public class BallPurchaseInfo : PurchaseInfo{
	public BallPurchaseInfo(CostType costType, PurchaseType type, BallType balltype, int cost, int reward){
		mInfo.costType = costType;
		mInfo.type = type;
		mInfo.currBallType = balltype;
		mInfo.cost = cost;
		mInfo.reward = reward;
	}

	public override ShopItemInfo GetInfo(){
		return mInfo;
	}
}

public class MoneyPurchaseInfo : PurchaseInfo{
	public MoneyPurchaseInfo(CostType costType, PurchaseType type, int cost, int reward){
		mInfo.costType = costType;
		mInfo.type = type;
		mInfo.currBallType = BallType.NONE;
		mInfo.cost = cost;
		mInfo.reward = reward;
	}

	public override ShopItemInfo GetInfo(){
		return mInfo;
	}
}


