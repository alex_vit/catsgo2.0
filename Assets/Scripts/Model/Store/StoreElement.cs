﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StoreElement : MonoBehaviour {
	public int buttonID;
	public CostType costType;

	[SerializeField]
	Image itemIcon;
	[SerializeField]
	Text itemTitle;
	[SerializeField]
	Text itemCost;

	public Button itemButton;

	void Start(){
		itemButton.onClick.AddListener (() => {
			ShopController.GetInstance ().MakePurchase (Store.PurchasesId [buttonID]);
		});
		itemCost.gameObject.SetActive (false);
	}

	public void SetCost(int cost){
		itemCost.gameObject.SetActive (true);
		itemCost.text = cost.ToString ();
	}

	public void SetCost(string cost){
		itemCost.gameObject.SetActive (true);
		itemCost.text = cost;
	}


}
