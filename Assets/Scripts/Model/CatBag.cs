﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//class for cat store
public class CatBag{
	static CatBag _instance;
	public static CatBag Instance{
		get{
			if (_instance == null)
				_instance = new CatBag ();
			return _instance;
		}
	}
	List<Cat> mCatList;
	// Use this for initialization
	public CatBag(){
		mCatList = new List<Cat> ();			
	}
	public void Init(){
		mCatList = new List<Cat> ();
	}
	/// <summary>
	/// Load this instance. Load cats from file storing at device
	/// </summary>
	void Load(){
		
	}
	/// <summary>
	/// Gets the items.
	/// </summary>
	/// <returns>The items.</returns>
	public List<Cat> GetItems(){
		return mCatList;
	}
	/// <summary>
	/// Adds the item.
	/// </summary>
	/// <param name="item">Item.</param>
	void AddItem(Cat item){
		mCatList.Add (item);
	} 
	/// <summary>
	/// Removes the item.
	/// </summary>
	/// <param name="item">Item.</param>
	void RemoveItem(Cat item){
		mCatList.Remove (item);
	}
}
