﻿using UnityEngine;
using System.Collections;

public class BallDetector : MonoBehaviour {
	Transform mTransform;
	bool mIsAvailable;
	// Use this for initialization
	void Awake () {
		mTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	/// <summary>
	/// Sets the position.
	/// </summary>
	/// <param name="pos">Position.</param>
	public void SetPosition(Vector3 pos){
		mTransform.position = new Vector3 (pos.x, pos.y, mTransform.position.z);
	}

	public bool IsAvailable(){
		return mIsAvailable;
	}

	public void Activate(bool active){
		mIsAvailable = active;
	}
}

