﻿using UnityEngine;
using System.Collections;

public class Detectable : MonoBehaviour {
	SpawnedItem mItem;
	Catchable mCatch;

	// Use this for initialization
	void Awake () {
		mItem = GetComponent<SpawnedItem> ();
		mCatch = GetComponent<Catchable> ();
	}

	void OnTriggerEnter(Collider coll){
		/*if (coll.gameObject.tag == _define.areaTag) {
			if (gameObject.tag == _define.catTag) {
				(mItem as Cat).Show ();
				mCatch.SetAvailable (true);
				Handheld.Vibrate ();
			} if(gameObject.tag == _define.ballTag){
				(mItem as Ball).Show ();
			} if (gameObject.tag == _define.moneyTag) {
				(mItem as Coin).Show ();
			}
			mCatch.SetAvailable (true);
		}*/

		if (coll.gameObject.tag == _define.areaTag) {
			Handheld.Vibrate();
			mItem.Show ();
			mCatch.SetAvailable (true);
		}
	}

	void OnTriggerExit(Collider coll){
		/*if (coll.gameObject.tag == _define.areaTag) {
			if (mCat != null) {
				mCat.Hide ();
				mCatch.SetAvailable (false);
			} if(mBall != null){
				mBall.Hide ();
				mCatch.SetAvailable (false);;
			}
		}*/
		if (coll.gameObject.tag == _define.areaTag) {
			mItem.Hide ();
			mCatch.SetAvailable (false);
		}
	}
}
