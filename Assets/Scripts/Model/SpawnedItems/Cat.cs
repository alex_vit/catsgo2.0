﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Cat : SpawnedItem {
	[SerializeField]
	SpriteRenderer imageRenderer;

	int mId;
	Rarity mPower;
	string mName = "";
	int mCurrentLevel = 0;
	Transform mTransform;
	// Use this for initialization

	public void Init(CatInfo info){
		mId = info.id;
		mPower = info.rarity;
		mName = info.name;

		if (imageRenderer != null) {
			imageRenderer.sprite = StaticData.CatImages [mId];
		}

		//init spawn info
		mInfo.itemId = mId;
		mInfo.itemType = SpawnItemType.CAT;
		Hide ();
	}

	public override void Spawn(Vector3 spawnPos, Transform parent){
		mTransform = transform;
		mTransform.SetParent (parent);
		mTransform.localPosition = spawnPos;
		mInfo.itemPos = spawnPos;
	}

	public override SpawnItemInfo GetSpawnInfo ()
	{
		return mInfo;
	}

	public CatInfo GetInfo(){
		CatInfo _info = new CatInfo ();
		_info.id = mId;
		_info.rarity = mPower;
		_info.name = mName;

		return _info;
	}

	public override void Show(){
		if (imageRenderer != null) {
			imageRenderer.gameObject.SetActive (true);
		}
	}

	public override void Hide(){
		if (imageRenderer != null) {
			imageRenderer.gameObject.SetActive (false);
		}
	}

}

public enum Rarity{
	COMMON,
	RARE,
	EPIC,
	LEGENDARY
}
