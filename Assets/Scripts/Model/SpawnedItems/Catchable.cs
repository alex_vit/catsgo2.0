﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using Map;

public class Catchable : MonoBehaviour {
	public SpawnItemType itemType;
	bool isAvailable;
	SpawnedItem mItem;
	Vector3 mCurrentPosition;

	public Action OnCatch;
	// Use this for initialization
	void Awake(){
		mItem = GetComponent<SpawnedItem> ();
	}

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetPosition(Vector3 pos){
		mCurrentPosition = pos;
	}

	public Vector3 GetPosition(){
		return mCurrentPosition;
	}

	public SpawnItemInfo GetItemInfo(){
		return mItem.GetSpawnInfo ();
	}

	public void Catch(){
		SetAvailable (false);
		gameObject.SetActive (false);
	}

	public void SetAvailable(bool available){
		isAvailable = available;
	}

	public bool IsAvailable(){
		return isAvailable;
	}
}
