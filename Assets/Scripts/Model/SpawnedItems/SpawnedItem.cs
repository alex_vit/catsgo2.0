﻿using UnityEngine;
using System.Collections;

public class SpawnedItem : MonoBehaviour {
	protected SpawnItemInfo mInfo;

	public virtual SpawnItemInfo GetSpawnInfo(){
		return mInfo;
	}

	public virtual void Spawn(Vector3 pos, Transform parent){
	
	}

	public virtual void Show(){
		
	}

	public virtual void Hide(){
		
	}
}
