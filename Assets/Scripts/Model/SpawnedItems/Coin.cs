﻿using UnityEngine;
using System.Collections;

public class Coin : SpawnedItem {
	[SerializeField]
	SpriteRenderer imageRenderer;
	Transform mTransform;
	// Use this for initialization

	public void Init(){
		if (imageRenderer != null) {
			imageRenderer.sprite = StaticData.UIImages[_define.moneyTag];
		}
		mInfo.itemType = SpawnItemType.MONEY;
		mInfo.itemId = 1;
		Hide ();
	}

	public override void Spawn(Vector3 spawnPos, Transform parent){
		mTransform = transform;
		mTransform.SetParent (parent);
		mTransform.localPosition = spawnPos;
		mInfo.itemPos = spawnPos;
	}

	public override void Show(){
		if (imageRenderer != null) {
			imageRenderer.gameObject.SetActive (true);
		}
	}

	public override void Hide(){
		if (imageRenderer != null) {
			imageRenderer.gameObject.SetActive (false);
		}
	}
}
