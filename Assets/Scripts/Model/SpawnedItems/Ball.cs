﻿using UnityEngine;
using System.Collections;

public class Ball : SpawnedItem {
	[SerializeField]
	SpriteRenderer imageRenderer;
	Transform mTransform;

	BallType mType;
	// Use this for initialization

	public void Init(BallInfo info){
		if (imageRenderer != null) {
			imageRenderer.sprite = StaticData.UIImages[_define.ballTag];
		}
		mType = info.ballType;
		mInfo.itemType = SpawnItemType.BALL;
		mInfo.itemId = 2;
		Hide ();
	}

	public override void Spawn(Vector3 spawnPos, Transform parent){
		mTransform = transform;
		mTransform.SetParent (parent);
		mTransform.localPosition = spawnPos;
		mInfo.itemPos = spawnPos;
	}

	public override void Show(){
		if (imageRenderer != null) {
			imageRenderer.gameObject.SetActive (true);
		}
	}

	public override void Hide(){
		if (imageRenderer != null) {
			imageRenderer.gameObject.SetActive (false);
		}
	}

	public BallInfo GetInfo(){
		BallInfo res = new BallInfo ();
		res.ballType = mType;
		return res;
	}
}
