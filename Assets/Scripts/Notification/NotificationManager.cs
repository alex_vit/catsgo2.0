﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class NotificationManager : MonoBehaviour {
	Dictionary<Rarity, NotificationInfo> CatNotificationsInfo;

	static NotificationManager _instance;
	Action OnNotify;
	// Use this for initialization
	void Awake () {
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
	}

	public static NotificationManager GetInstance(){
		return _instance;
	}

	public void Init(){
		CatNotificationsInfo = new Dictionary<Rarity, NotificationInfo>{
			{Rarity.COMMON, new NotificationInfo(){title = Localizator.GetTextValue(_define.appName), msg = Localizator.GetTextValue(_define.commonCat), delay = StaticData.catPushTimeValues[Rarity.COMMON]}},	
			{Rarity.RARE, new NotificationInfo(){title = Localizator.GetTextValue(_define.appName), msg = Localizator.GetTextValue(_define.rareCat), delay = StaticData.catPushTimeValues[Rarity.RARE]}},
			{Rarity.EPIC, new NotificationInfo(){title = Localizator.GetTextValue(_define.appName), msg = Localizator.GetTextValue(_define.epicCat), delay = StaticData.catPushTimeValues[Rarity.EPIC]}}
		};

		CheckNotifications();
	}

	void CheckNotifications(){
		Int64 currTime = ConnectionManager.GetGlobalTime ();

		for (int i = 0; i < Enum.GetNames (typeof(Rarity)).Length-1; i++) {
			if (StaticData.catPushTime [i] == -1) {
				SendMsg ((Rarity)i);
				Debug.Log ("send notify " + (Rarity)i);
			}else {
				//check time when notification was sent and if it more than needs we handle msg
				if (currTime - StaticData.catPushTime [i] >= StaticData.catPushTimeValues [(Rarity)i]) {
					HandleMsg (i);
					Debug.Log ("handle notify");
					SendMsg ((Rarity)i);
				} else {
					ReSendMsg (i);
					Debug.Log ("notify cancelled");	
				}
			}
		}
	}
	
	void SendMsg(Rarity rare){
		NotificationInfo _info = CatNotificationsInfo [rare];
		if(rare == Rarity.EPIC)
			LocalNotification.SendRepeatingNotification (++StaticData.notifyCount, _info.delay, StaticData.notifyRepeatTime,  _info.title, _info.msg, Color.white);
		else	
			LocalNotification.SendNotification (++StaticData.notifyCount, _info.delay, _info.title, _info.msg, Color.white);
		StaticData.catPushId [(int)rare] = StaticData.notifyCount;
		StaticData.catPushTime [(int)rare] = ConnectionManager.GetGlobalTime ();
		StaticData.SaveInfo ();
	}

	void HandleMsg(int rewardId){
		StaticData.currReward = rewardId;
		StaticData.catPushTime [rewardId] = -1;
		StaticData.catPushId [rewardId] = -1;
		StaticData.isRewarded = true;
		StaticData.SaveInfo ();
	}

	void ReSendMsg(int number){
		if (StaticData.catPushId [number] != -1) {
			LocalNotification.CancelNotification (StaticData.catPushId [number]);
			StaticData.catPushId [number] = -1;
		}
		SendMsg ((Rarity)number);
	}
}
