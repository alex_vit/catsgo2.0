﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using System.Collections;

public class AlertDialog : Dialog {
	[SerializeField]
	private LocalizedText selfText;
	public Action OkAction;
	public Action CancelAction;

	public void SetText(string text){
		selfText.text = Localizator.GetTextValue(text);	
	}
	public void OnOkClick(){
		CDebug.Log ("OnOkClick");
		if(OkAction != null)
			OkAction ();
		Hide ();
	}
	public void OnCancelClick(){
		if(CancelAction != null)
			CancelAction ();
		Hide ();
	}
}
