﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;

public class Dialog : MonoBehaviour {
	public Action OnShowed;
	public Action OnHide;

	public virtual void Show(){
		gameObject.SetActive (true);
		if (OnShowed != null) {
			OnShowed ();
		}
	}
	public virtual void Hide (){
		gameObject.SetActive (false);
		if (OnHide != null) {
			OnHide ();
		}
	}
}
