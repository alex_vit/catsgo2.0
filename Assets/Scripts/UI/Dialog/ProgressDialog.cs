﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ProgressDialog : Dialog {
	public Text title;
	Animator mAnim;

	List<int> textIds;
	// Use this for initialization
	void Awake () {
		mAnim = GetComponent<Animator> ();	
	}

	public override void Show ()
	{
		base.Show ();
		mAnim.Rebind ();
		mAnim.enabled = true;
		SetDefault ();
		StartCoroutine (ChangeTitle ());
	}

	public override void Hide(){
		StopAllCoroutines ();
		base.Hide ();
		mAnim.enabled = false;
	}

	void SetDefault(){
		textIds = new List<int>{ 1, 2, 3, 4, 5, 6, 8, 9, 10 };
	}

	IEnumerator ChangeTitle(){
		int currIndex = 0;
		while (true) {
			currIndex = textIds[UnityEngine.Random.Range(0,textIds.Count)];
			title.text = Localizator.GetTextValue("map_" + currIndex);
			textIds.Remove (currIndex);
			yield return new WaitForSeconds(UnityEngine.Random.Range(4f, 8f));
		}
	}
}
