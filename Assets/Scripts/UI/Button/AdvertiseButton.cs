﻿using UnityEngine;
using System.Collections;

public class AdvertiseButton : MonoBehaviour {
	// Use this for initialization
	void OnEnable () {
		CheckEnabled ();
	}
		

	void CheckEnabled(){
		if (StaticData.availableAd == true)
			gameObject.SetActive (true);
		else
			gameObject.SetActive (false);
	}
}
