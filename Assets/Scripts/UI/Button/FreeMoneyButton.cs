﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class FreeMoneyButton : MonoBehaviour {
	[SerializeField]
	Image backGround;
	[SerializeField]
	Image moneyIcon;
	[SerializeField]
	Text buttonText;
	[SerializeField]
	Sprite activeSprite;
	[SerializeField]
	Sprite inactiveSprite;

	Button _button;

	public Action OnClick;
	bool isActive;
	// Use this for initialization
	void Awake () {
		_button = GetComponent<Button> ();
	}

	public void TurnOn(){
		isActive = true;
		_button.enabled = true;
		if(backGround != null)
			backGround.sprite = activeSprite;
		if(moneyIcon != null)
			moneyIcon.sprite = StaticData.UIImages ["free_coin_light"];
	}

	public void TurnOff(){
		isActive = false;
		_button.enabled = false;
		if(backGround != null)
			backGround.sprite = inactiveSprite;
		if(moneyIcon != null)
			moneyIcon.sprite = StaticData.UIImages ["free_coin_dark"];
	}

	public void OnClicked(){
		if (isActive && OnClick != null)
			OnClick ();
	}
}
