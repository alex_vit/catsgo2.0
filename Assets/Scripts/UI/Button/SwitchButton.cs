﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class SwitchButton : Button {
	public Sprite generalState;
	public Sprite pressedState;

	public Action<bool> OnSwitchState;

	bool mState;

	void Awake(){
		onClick.AddListener(() => {
			mState = !mState;
			SwitchState(mState); 
			if (OnSwitchState != null) {
				OnSwitchState (mState);
		}});
	}

	public void SwitchState(bool isPressed){
		CDebug.Log ("isPressed " + isPressed);
		if (isPressed)
			image.sprite = generalState;
		else
			image.sprite = pressedState;
		mState = isPressed;
	}

}
