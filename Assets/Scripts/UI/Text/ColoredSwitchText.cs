﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColoredSwitchText : MonoBehaviour {
	Text mText;

	public Color activeColor;
	public Color inactiveColor;

	// Use this for initialization
	void Awake () {
		mText = GetComponent<Text> ();
	}

	public void SetActive(bool active){
		mText = GetComponent<Text> ();
		mText.color = (active ? activeColor : inactiveColor);
	}
}
