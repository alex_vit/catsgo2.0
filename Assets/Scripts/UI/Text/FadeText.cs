﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class FadeText : Text {
	public float offset;
	public Action OnFaded;

	RectTransform mTransform;
	Vector2 mDestPoint;
	Vector3 mDefaultPos; 

	void Awake(){
		mTransform = GetComponent<RectTransform> ();
		mDefaultPos = mTransform.position;
	}

	void OnDisable(){
		if (enabled) {
			mTransform.position = mDefaultPos;
			color = new Color (color.r, color.g, color.b, 0f);
		}
	}
		
	public void ShowText(string msg, Vector3 beginPos, Vector3 destPos){
		mTransform.position = beginPos;
		mDestPoint = destPos;
		text = msg;
		StopAllCoroutines ();
		if (gameObject.activeSelf) {
			StartCoroutine (Move ());
		}
	}
		
	void Hide(){
		gameObject.SetActive (false);
	}

	IEnumerator Move(){
		color = new Color (color.r, color.g, color.b, 1f);
		Vector2 _pos;
		while (true) {
			if (mTransform.position.x >= mDestPoint.x - offset && mTransform.position.y >= mDestPoint.y - offset) {
				StartCoroutine (ShowFade ());
				break;
			}
			_pos.x = Mathf.Lerp (mTransform.position.x, mDestPoint.x, Time.deltaTime*5);
			_pos.y = Mathf.Lerp (mTransform.position.y, mDestPoint.y, Time.deltaTime*5);
			mTransform.position = new Vector3 (_pos.x, _pos.y, 0f);
			yield return new WaitForFixedUpdate();
		}
	}

	IEnumerator ShowFade(){
		float currAlpha = 1f;
		color = new Color (color.r, color.g, color.b, currAlpha);
		while (true) {
			if (color.a <= 0f) {
				if (OnFaded != null)
					OnFaded ();
				break;
			}
			currAlpha -= Time.deltaTime * 2f;
			color = new Color (color.r, color.g, color.b, currAlpha);
			yield return new WaitForFixedUpdate();
		}
	}
}
