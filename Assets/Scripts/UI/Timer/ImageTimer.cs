﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Collections;
/*
 * class for UI circle timer
 */
public class ImageTimer : MonoBehaviour{
	[SerializeField]
	Image timerImage;
	[SerializeField]
	Text timerText;

	int time;
	int currTime;
	//it's callback for time ending
	public Action TimeOut;

	bool isWorking;
	WaitForSeconds mWaitForSeconds;

	public void Run(int currCount, int count){
		isWorking = true;
		time = count;
		currTime = currCount;
		mWaitForSeconds = new WaitForSeconds (_define.timeStep);
		StopAllCoroutines ();
		StartCoroutine (Process ());
	}

	public void Stop(){
		isWorking = false;
		StopAllCoroutines ();
	}

	void OutOfTime(){
		isWorking = false;
		if (TimeOut != null)
			TimeOut ();
		gameObject.SetActive (false);
	}

	IEnumerator Process(){
		while (true) {
			if (currTime == 0) {
				OutOfTime ();
				break;
			}
			timerText.text = (--currTime/60).ToString() +"m";
			timerImage.fillAmount = 1f - (float)currTime / time;
			yield return mWaitForSeconds;
		}
	}

	/*void OnDisable(){
		if (currTime > 0) {
			StaticData.playerData.freeMoneyTick = currTime;
			StaticData.playerData.lastTimePlayed = ConnectionManager.GetGlobalTime ();
			StaticData.SaveInfo ();
			gameObject.SetActive (false);
			CDebug.Log ("save curr tick");		
		}
	}

	void OnApplicationQuit(){
		if (enabled) {
			StaticData.playerData.freeMoneyTick = currTime;
			StaticData.playerData.lastTimePlayed = ConnectionManager.GetGlobalTime ();
			StaticData.SaveInfo ();
			gameObject.SetActive (false);
			CDebug.Log ("save curr tick");	
		}
	}*/
}
