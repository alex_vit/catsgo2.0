﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TextTimer : MonoBehaviour {
	[SerializeField]
	Text timerText;

	int time;
	int currTime;
	bool isWorking;
	WaitForSeconds mWaitForSeconds;
	//it's callback for time ending
	public Action TimeOut;

	public void Run(){
		isWorking = true;
		gameObject.SetActive (true);
		currTime = _define.timeForAdBalls;
		mWaitForSeconds = new WaitForSeconds (_define.timeStep);
		StopAllCoroutines ();
		StartCoroutine (Process ());
	}

	public void Stop(){
		isWorking = false;
		StopAllCoroutines ();
		gameObject.SetActive (false);
	}

	public bool IsWork(){
		return isWorking;
	}

	void OutOfTime(){
		isWorking = false;
		if (TimeOut != null)
			TimeOut ();
		gameObject.SetActive (false);
	}

	IEnumerator Process(){
		while (true) {
			if (currTime <= 1) {
				OutOfTime ();
				break;
			}
			timerText.text = (--currTime).ToString();
			yield return mWaitForSeconds;
		}
	}
}
