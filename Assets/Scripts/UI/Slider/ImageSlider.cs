﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImageSlider : MonoBehaviour {
	public RectTransform backGround;
	public Image fillImage;

	float mDefaultValue = 0f;
	float mDefaultWidth;
	// Use this for initialization
	void Awake () {
		//mDefaultWidth = fillImage.sizeDelta.x;
	}

	void Start(){
		//SetDefault ();
	}

	public void SetDefaultValue(float value){
		mDefaultValue = value;
	}

	public void SetDefault(){
		fillImage.fillAmount = 0f;
		//fillImage.sizeDelta = new Vector2 (mDefaultWidth, fillImage.sizeDelta.y);
	}

	public void RecalculateValue(float value){
		//float _width = ((value * mDefaultWidth) / mDefaultValue);
		float _widthFloat = (value) / mDefaultValue;
		//fillImage.sizeDelta = new Vector2 (_width, fillImage.sizeDelta.y);
		fillImage.fillAmount = _widthFloat;
	}

	public void SetValue(float value){
		fillImage.fillAmount = value;
		CDebug.Log ("fillAmount " + fillImage.fillAmount);
	}

	public void SetBackgroundColor(string color){
	
	}

	public void SetFillColor(string color){
		Color _newColor = new Color();
		ColorUtility.TryParseHtmlString (color, out _newColor);
		fillImage.color = _newColor;
	}
}
