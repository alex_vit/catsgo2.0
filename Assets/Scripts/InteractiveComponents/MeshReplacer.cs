﻿using UnityEngine;
using System.Collections;

public class MeshReplacer : MonoBehaviour {
	public MeshFilter meshFilter;
	[SerializeField]
	Mesh defaultMesh;
	[SerializeField]
	Mesh rightHitMesh;
	[SerializeField]
	Mesh leftHitMesh;
	[SerializeField]
	Mesh backHitMesh;
	[SerializeField]
	Mesh forwardHitMesh;

	MeshFilter _meshFilter;

	public void SetDefault(){
		ReplaceMesh (defaultMesh);
	}

	void ReplaceMesh(Mesh replaceMesh){
		if(meshFilter != null)
			meshFilter.mesh = replaceMesh;
	}
}
