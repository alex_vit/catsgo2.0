using UnityEngine;
using System.Collections;

public class Sounder : MonoBehaviour
{
    public static Sounder me;
	
	public int SourcesCount = 5;
	
	AudioSource[] sources;
	
	void Start ()
	{
	    me = this;
		Refresh ();
	}

	[ContextMenu("Refresh")]
	public void Refresh ()
	{
		if (transform.childCount != SourcesCount) {
			for (int i = transform.childCount - 1; i >= 0; i--) {
				DestroyImmediate (transform.GetChild (i).gameObject);
			}

			sources = new AudioSource[SourcesCount];
			
			for (int i = 0; i < SourcesCount; i++) {
				var aSrc = new GameObject ("Source" + (i + 1).ToString ());
				aSrc.transform.parent = transform;
				aSrc.transform.localPosition = Vector3.zero;
				
				sources [i] = aSrc.AddComponent<AudioSource> ();
				sources [i].playOnAwake = false;
				sources [i].loop = false;
			}	
		} else {
			sources = transform.GetComponentsInChildren<AudioSource> ();
		}

	}
	
	public static void Play (string name)
	{
		if(me != null)
        me.Play (name, StaticData.Sound, false);
	}
    public static void Play(AudioClip audio)
    {
        if (me != null)
            me.Play(audio, StaticData.Sound, false);
    }

	public static void Play(string name, bool loop){
		if (me != null)
			me.Play (name, StaticData.Sound, loop);
	}
	public void Play(AudioClip audio, float volume, bool loop)
    {
        foreach (var item in sources)
        {
            if (!item.isPlaying)
            {
                item.clip = audio;
                item.volume = volume;
				item.loop = loop;
                item.Play();
                return;
            }
        }
        var sacriface = sources[Random.Range(0, SourcesCount)];
        sacriface.clip = audio;
        sacriface.volume = volume;
		sacriface.loop = loop;
        sacriface.Play();
    }
	public void Play (string name, float volume, bool loop)
	{
		var audio = StaticData.Audio [name];
		foreach (var item in sources) {
			if (!item.isPlaying) {
				item.clip = audio;
				item.volume = volume;
				item.loop = loop;
				item.Play ();
				return;
			}
		}
		var sacriface = sources [Random.Range (0, SourcesCount)];
		sacriface.clip = audio;
		sacriface.volume = volume;
		sacriface.loop = loop;
		sacriface.Play ();
	}

	public bool IsPlaying (string name)
	{
		var audio = StaticData.Audio [name];

		foreach (var item in sources) 
			if (item.clip == audio) 
				return true;

		return false;
	}

	public void PlayButtonSound ()
	{
		Play ("BtnDown");
	}

    public void OnDestroy()
    {
        me = null;
    }
}
