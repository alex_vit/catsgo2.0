﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Timer : MonoBehaviour {
	public Action OnTimeEnded;
	Text timerText;
	// Use this for initialization
	void Awake(){
		timerText = GetComponent<Text> ();
	}

	public void Run(){
		StopAllCoroutines ();
		StartCoroutine (Process ());
	}

	void Stop(){
		if (OnTimeEnded != null)
			OnTimeEnded ();
	}

	IEnumerator Process(){
		int _count = _define.timerForGame;
		while (true) {
			if (_count <= 0) {
				Stop ();
				break;
			}
			timerText.text = (--_count / 60) + ":" + (_count % 60);
			yield return new WaitForSeconds (_define.timeStep);
		}
	}
}
