﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Clickable : MonoBehaviour {
	void Start(){
		GetComponent<Button> ().onClick.AddListener (HandleClick);	
	}

	void HandleClick(){
		Sounder.Play ("click");
		Analytics.LogEvent (gameObject.name);
	}
}
