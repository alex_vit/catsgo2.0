﻿using UnityEngine;
using System.Collections;

public class Tracker : MonoBehaviour {
	public Transform trackedTransform;
	public Vector3 trackedPos;
	Transform mTransform;
	// Use this for initialization
	void Awake(){
		mTransform = transform;
	}

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Track ();	
	}

	void Track(){
		if (trackedTransform != null)
			mTransform.position = trackedTransform.position + trackedPos;
	}
}
