﻿using UnityEngine;
using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class Serializator{
	static string achievPath = Application.persistentDataPath + "/achievs.wzd";
	static string dailyQuestPath = Application.persistentDataPath + "/daily_quests.wzd";

	#region achieves
	public static List<Achieve> LoadAchivs(){
		List<Achieve> _res = new List<Achieve> ();
		List<Achieve> _tmp = new List<Achieve> ();

		if (File.Exists (achievPath)) {
			CDebug.Log ("file exist, load data");
			_res = StaticData.AchieveStore;

			BinaryFormatter _bf = new BinaryFormatter ();
			FileStream _fileRead = File.Open (achievPath, FileMode.Open);
			_tmp = (List<Achieve>)_bf.Deserialize (_fileRead);
			_fileRead.Close ();

			for (int i = 0; i < _tmp.Count; i++) {
				if (_res.Where (x => x.id == _tmp [i].id).Count() != 0) {
					Achieve _ach = _res.Where (x => x.id == _tmp [i].id).First ();
					_res.Remove (_ach);
					_res.Add (_tmp.Where (x => x.id == _ach.id).First ());
				}
			}
		} else {
			CDebug.Log ("file doesn't exist, load default data");
			_res = StaticData.AchieveStore;
		}
		return _res;
	}

	public static void SaveAchivs(List<Achieve> data){
		BinaryFormatter _bf = new BinaryFormatter ();
		FileStream _fileWrite = File.Create (achievPath);
		_bf.Serialize (_fileWrite, data);
		_fileWrite.Close ();
	}
	#endregion
	#region daily quest
	public static List<DailyQuest> LoadDailyQuests(){
		List<DailyQuest> _res = new List<DailyQuest> ();

		if (File.Exists (dailyQuestPath)) {
			CDebug.Log ("file exist, load data");

			BinaryFormatter _bf = new BinaryFormatter ();
			FileStream _fileRead = File.Open (dailyQuestPath, FileMode.Open);
			_res = (List<DailyQuest>)_bf.Deserialize (_fileRead);
			_fileRead.Close ();
		}
		return _res;		
	}

	public static void SaveDailyQuests(List<DailyQuest> data){
		BinaryFormatter _bf = new BinaryFormatter ();
		FileStream _fileWrite = File.Create (dailyQuestPath);
		_bf.Serialize (_fileWrite, data);
		_fileWrite.Close ();
	}
	#endregion
	public static void RemoveProgress(){
		if(File.Exists(achievPath))
			File.Delete (achievPath);
		if(File.Exists(dailyQuestPath))
			File.Delete (dailyQuestPath);
	}
}
