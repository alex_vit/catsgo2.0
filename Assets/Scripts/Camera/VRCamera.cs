﻿using UnityEngine;
using System.Collections;

public class VRCamera : MonoBehaviour {
	public float sensitivity;
	public Vector2 deadZone;
	Vector2 gyroData;
	Transform mTransform;
	Gyroscope mGyro;

	Quaternion cameraRot = Quaternion.identity;
	Quaternion calibrateRot = Quaternion.identity;
	Quaternion baseRotation = Quaternion.Euler(0,0,0);

	bool isRunning = false;
	// Use this for initialization
	void Awake(){
		mTransform = transform;
		mGyro = Input.gyro;
		Debug.Log (mGyro);
	}

	void Start () {
		CalibrateGyro ();
	}

	public void Run(){
		Debug.Log ("Run game");
		Centralize ();
		if (!isRunning) {
			isRunning = true;
			mGyro = Input.gyro;
			mGyro.enabled = true;
		}
	}

	public void Stop(){
		isRunning = false;
	}
	
	// Update is called once per frame
	void Update () {
		Progress ();
	}

	void Centralize(){
		mTransform.rotation = Quaternion.Euler (Vector3.zero); 
	}

	void CalibrateGyro(){
		calibrateRot = mGyro.attitude;
	}
	/// <summary>
	/// Gets the gyro data.
	/// </summary>
	/// <returns>The gyro data.</returns>
	Vector2 GetGyroData(){
		float x = mGyro.rotationRate.x;
		if (Mathf.Abs (x) <= deadZone.x)
			x = 0;
		float y = mGyro.rotationRate.y;
		if (Mathf.Abs (y) <= deadZone.y)
			y = 0;
		return new Vector2 (x, y);
	}

	void Progress(){
		#if UNITY_ANDROID
		UpdateRotation();
		#endif
	}

	void UpdateRotation(){
		if(isRunning){
			gyroData = GetGyroData ();
			//x rotation
			{
				mTransform.RotateAround (mTransform.position, mTransform.right, -gyroData.x * Time.fixedDeltaTime * sensitivity);
				//y rotation
				mTransform.RotateAround (mTransform.position, Vector3.up, -gyroData.y * Time.fixedDeltaTime * sensitivity);
				baseRotation = mTransform.rotation;
			}
			//cameraRot = Quaternion.Inverse (baseRotation) * Quaternion.Inverse (calibrateRot) * mGyro.attitude * sensitivity;
			//mTransform.rotation = cameraRot;
		}
	}

	public Quaternion GetRotation(){
		return mTransform.rotation;
	}
}
