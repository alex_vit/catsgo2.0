﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraPlane : MonoBehaviour {
	WebCamTexture camTex = null;
	RawImage mImage;
	public Material camMat;

	// Use this for initialization
	void Awake () {
		mImage = GetComponent<RawImage> ();
	}
	
	public void Show(){
		camTex = new WebCamTexture ();
		mImage.texture = camTex;
		mImage.material.mainTexture = camTex;
		camTex.Play ();
	}

	public void Hide(){
		if(camTex != null)
		camTex.Stop ();
		camTex = null;
	}
}
