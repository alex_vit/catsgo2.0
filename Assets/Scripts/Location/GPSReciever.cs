﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class GPSReciever : MonoBehaviour {
	static GPSReciever _instance;

	public float updateInterval = 1f;
	public string result = "";

	public bool isStarted { get; private set; }

	public double latitude { get; private set; }
	public double longitude { get; private set; }
	public double accuracy { get; private set; }
	private double newLatitude, newLongitude, newAccuracy;
	public Vector2 coords { get; private set; }

	public const int GPS_NOT_INITIALIZED = -1;
	public const int GPS_NOT_ENABLED = 1;
	public const int GPS_RESTRICTED = 2;
	public const int GPS_NOT_ALLOWED = 3;
	public const int GPS_NOT_DETERMINED = 4;
	public const int GPS_READY = 0;

	public float lastChangeTime { get; private set; }

	#if UNITY_ANDROID && !UNITY_EDITOR
	private AndroidJavaObject jo;
	#elif UNITY_IOS && !UNITY_EDITOR
	[DllImport ("__Internal")]
	private static extern bool startGPSService();
	[DllImport ("__Internal")]
	private static extern void stopGPSService();
	[DllImport ("__Internal")]
	private static extern double getLatitude();
	[DllImport ("__Internal")]
	private static extern double getLongitude();
	[DllImport ("__Internal")]
	private static extern double getAccuracy();
	[DllImport ("__Internal")]
	private static extern bool isGPSServiceReady();
	[DllImport ("__Internal")]
	private static extern int getGPSStatus();
	[DllImport ("__Internal")]
	private static extern void showNoticeOrStart();
	#endif

	void Awake(){
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);

		isStarted = false;
		lastChangeTime = 0;
	}

	void Start () {
		//TryEnableGPS ();
	}

	public static GPSReciever GetInstance(){
		return _instance;
	}

	public int GetGPSStatus() {
		int status = GPS_NOT_INITIALIZED;
		#if UNITY_ANDROID && !UNITY_EDITOR
		if(jo != null) {
			status = jo.CallStatic<int> ("checkGPSService");
		}
		#elif UNITY_IOS && !UNITY_EDITOR
		status = getGPSStatus();
		#endif
		return status;
	}

	public void ShowErrorDialog() {
		#if UNITY_ANDROID && !UNITY_EDITOR
		jo.CallStatic("showNotice");
		#elif UNITY_IOS && !UNITY_EDITOR
		showNoticeOrStart();
		#endif
	}

	public bool TryEnableGPS() {
		if (isStarted == false) {

			#if UNITY_ANDROID && !UNITY_EDITOR
			/*AndroidJavaClass jc = new AndroidJavaClass("com.wazzapps.unitylocationlibrary.GPSManager");
			int status = jc.CallStatic<int> ("checkGPSStatus");
			if(status != 0) {
				jc.CallStatic("showInfoNotice");
				isStarted = false;
			} else {
				Input.location.Start();
				isStarted = true;
			}*/
			isStarted = ConnectionManager.CheckGPS();
			#elif UNITY_IOS && !UNITY_EDITOR
			if(startGPSService()) {
				showNoticeOrStart();
				isStarted = true;
			}
			#endif

			if (isStarted) {
				CancelInvoke ();
				InvokeRepeating ("UpdatePosition", 0.2f, updateInterval);
			}

		}
		//Debug.Log ("GPS started = " + isStarted);
		return isStarted;
	}

	public void OnAuthStatusChanged(string message) {
		Debug.Log (message);
		TryEnableGPS ();
	}

	void UpdatePosition() {
		#if UNITY_ANDROID && !UNITY_EDITOR
		/*if (jo != null) {
			Debug.Log("isGPSServiceReady="+jo.CallStatic<bool> ("isGPSServiceReady")+", stat="+jo.CallStatic<int> ("getGPSStatus"));
			latitude = jo.CallStatic<double> ("getLatitude");
			longitude = jo.CallStatic<double> ("getLongitude");
			accuracy = jo.CallStatic<double> ("getAccuracy");
		Debug.Log ("android "+latitude+","+longitude+" accuracy="+accuracy);
		}*/

		newLatitude = Input.location.lastData.latitude;
		newLongitude = Input.location.lastData.longitude;
		newAccuracy = Mathf.Max(Input.location.lastData.horizontalAccuracy, Input.location.lastData.verticalAccuracy);
		if(latitude != newLatitude || longitude != newLongitude || accuracy != newAccuracy) {
			lastChangeTime = Time.time;
		}
		latitude = newLatitude;
		longitude = newLongitude;
		accuracy = newAccuracy;
		coords = new Vector2((float)latitude, (float)longitude);
		//Debug.Log ("android "+Input.location.lastData.latitude+","+Input.location.lastData.longitude + ", " + Input.location.lastData.horizontalAccuracy+ ", " + Input.location.lastData.verticalAccuracy);

		#elif UNITY_IOS && !UNITY_EDITOR
		newLatitude = getLatitude();
		newLongitude = getLongitude();
		newAccuracy = getAccuracy();
		if(latitude != newLatitude || longitude != newLongitude || accuracy != newAccuracy) {
			lastChangeTime = Time.time;
		}
		latitude = newLatitude;
		longitude = newLongitude;
		accuracy = newAccuracy;
		coords = new Vector2((float)latitude, (float)longitude);
		//Debug.Log ("ios "+latitude+","+longitude+" "+accuracy);
		#endif


	}

	public bool isActualData() {
		return (Time.time - lastChangeTime) < _define.gps_update_time;
	}
	/*
	void OnGUI() {
		string str = "Lat: " + latitude + ", Long: " + longitude + ", acc: " + accuracy + ", auth=";
		#if UNITY_ANDROID && !UNITY_EDITOR
		str += jo.CallStatic<int> ("getGPSStatus");
		#elif UNITY_IOS && !UNITY_EDITOR
		str += getGPSStatus();
		#endif
		result = str;
	}*/

	void OnDestroy() {
		#if UNITY_ANDROID && !UNITY_EDITOR
		Input.location.Stop();
		#elif UNITY_IOS && !UNITY_EDITOR
		stopGPSService ();
		#endif
	}
}
