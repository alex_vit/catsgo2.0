﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class FreeCoinsController : MonoBehaviour {
	static FreeCoinsController _instance;

	GameState _state;
	public GameState State{
		get{ 
			return _state;
		}set{ 
			_state = value;
			CheckState ();
		}
	}
	bool mFreeCoinAvailable;

	int time;
	int currTime;
	//it's callback for time ending
	public Action TimeOut;

	bool isWorking;
	WaitForSeconds mWaitForSeconds;


	// Use this for initialization
	void Awake(){
		if (_instance == null)
			_instance = this;
		else
			Destroy (gameObject);
		//State = GameState.LOADING;
	}

	public static FreeCoinsController GetInstance(){
		return _instance;
	}

	void OnApplicationQuit(){
		StoreInfo ();
	}

	void CheckState(){
		switch (_state) {
		case GameState.MENU:
			SetActiveMainMenuUI ();
			break;
		case GameState.PLAYER_MENU:
			SetActivePlayerMenuUI ();
			break;
		default:
			SetDefault ();
			break;
		}
	}
	//handle main menu state
	void SetActiveMainMenuUI(){
		TimeOut += () => {
			mFreeCoinAvailable = true;
			FreeCoinsMenuUI.GetInstance().Activate();
		};
		CheckTimer ();
	}
	//handle player menu state
	void SetActivePlayerMenuUI(){
		TimeOut += () => {
			mFreeCoinAvailable = true;
			FreeCoinsPlayerUI.GetInstance().Activate();
		};
		CheckTimer ();
	}

	void SetDefault(){
		TimeOut = null;
		StoreInfo ();
	}

	public void CheckTimer(){
		//if we are not show AD yet
		if (StaticData.playerData.freeMoneyTick <= 0) {
			CDebug.Log ("timer wasn;t active");
			mFreeCoinAvailable = true;
			if (_state == GameState.PLAYER_MENU) {
				FreeCoinsPlayerUI.GetInstance ().Activate ();
			}
			if (_state == GameState.MENU) {
				FreeCoinsMenuUI.GetInstance ().Activate ();
			}
		}
		else {
			CDebug.Log ("timer was active");
			int deltaTime = (int)(ConnectionManager.GetGlobalTime () - StaticData.playerData.lastTimePlayed);
			CDebug.Log ("delta time " + deltaTime + "currTick " + StaticData.playerData.freeMoneyTick);
			int _totalTime = StaticData.playerData.freeMoneyTick - deltaTime;
			//if last time more tha needed
			if (_totalTime <= 0) {
				mFreeCoinAvailable = true;
				if (_state == GameState.PLAYER_MENU) {
					FreeCoinsPlayerUI.GetInstance ().Activate ();
				}
				if (_state == GameState.MENU) {
					FreeCoinsMenuUI.GetInstance ().Activate ();
				}
				return;
			} else {
				mFreeCoinAvailable = false;
				if (_state == GameState.PLAYER_MENU) {
					FreeCoinsPlayerUI.GetInstance ().Desactivate ();
				}
				if (_state == GameState.MENU) {
					FreeCoinsMenuUI.GetInstance ().Desactivate ();
				}
				SetFreeMoneyTimer (_totalTime);
			}
		}
	}

	public void StartTimer(){
		SetFreeMoneyTimer (_define.timerForFreeMoney);
		mFreeCoinAvailable = false;
		if (_state == GameState.PLAYER_MENU)
			FreeCoinsPlayerUI.GetInstance ().Desactivate ();
		if(_state == GameState.MENU)
			FreeCoinsMenuUI.GetInstance ().Desactivate ();
	}

	void StopTimer(){
		
	}

	public bool IsAvailable(){
		return mFreeCoinAvailable;
	}

	void SetFreeMoneyTimer(int currTick){
		/*if (_state == GameState.PLAYER_MENU) {
			playerFreeMoneyButton.TurnOff ();
			playerFreeMoneyTimer.gameObject.SetActive (true);
			freeCoinsTitle.gameObject.SetActive (true);
		}
		if (_state == GameState.MENU) {
			menuFreeMoneyButton.TurnOff ();
		}*/
		if (currTick == 0) {
			Run (0, _define.timerForFreeMoney);
		} else {
			Run (currTick, _define.timerForFreeMoney);
		}
	}
		
	#region work with timer
	public void Run(int currCount, int count){
		isWorking = true;
		time = count;
		currTime = currCount;
		mWaitForSeconds = new WaitForSeconds (_define.timeStep);
		StopAllCoroutines ();
		StartCoroutine (Process ());
	}

	public void Stop(){
		isWorking = false;
		StopAllCoroutines ();
	}

	void OutOfTime(){
		isWorking = false;
		if (TimeOut != null)
			TimeOut ();
	}

	/*void OnDisable(){
		if (enabled) {
			StaticData.playerData.freeMoneyTick = currTime;
			StaticData.playerData.lastTimePlayed = ConnectionManager.GetGlobalTime ();
			StaticData.SaveInfo ();
			CDebug.Log ("save curr tick");
		}
	}*/

	void StoreInfo(){
		if (enabled) {
			StaticData.playerData.freeMoneyTick = currTime;
			CDebug.Log ("curr Time" + currTime);
			StaticData.playerData.lastTimePlayed = ConnectionManager.GetGlobalTime ();
			StaticData.SaveInfo ();
			CDebug.Log ("save curr tick");
		}
	}



	IEnumerator Process(){
		int _currMin, _currSec;
		while (true) {
			if (currTime == 0) {
				OutOfTime ();
				break;
			}
			_currMin = (--currTime / 60);
			_currSec = (currTime % 60);

			if (_state == GameState.PLAYER_MENU) {
				FreeCoinsPlayerUI.GetInstance().UpdateInfo(_currMin, _currSec, 1f-(float)currTime/time);
			}
			if(_state == GameState.MENU){
				FreeCoinsMenuUI.GetInstance ().UpdateInfo (_currMin, _currSec);
			}
			yield return mWaitForSeconds;
		}
	}
	#endregion
}
	
