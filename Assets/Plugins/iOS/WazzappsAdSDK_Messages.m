//
//  WazzappsAdSDK_Messages.h
//  Для возможности отправки сообщений в Unity необходимо задать внешнюю функцию, которой доступно UnitySendMessage
//  Должен располагаться в Assets/Plugins/iOS
//  Wazzapps AD SDK
//
//  Created by Mikhail Petrushkov on 08/08/16.
//  Copyright © 2016 Wazzapps. All rights reserved.
//
#import "WazzappsAdSDK_Messages.h"

WazzappsAdsViewInterface * delegate;

@implementation WazzappsAdsViewInterface

- (void)WazzappsOnMessageRecieved:(const char*)gameObject methodName:(const char*)method message:(const char*)msg {
	UnitySendMessage(gameObject, method, msg);
}

@end

void WazzappsAds_InitUnityMessages() {
	if(delegate == NULL) {
		delegate = [[WazzappsAdsViewInterface alloc] init];
	}
    WazzappsAds_SetUnityCallback(delegate);
}
