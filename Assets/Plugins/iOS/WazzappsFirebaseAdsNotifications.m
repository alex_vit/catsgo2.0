//
//  WazzappsAdSDK_Messages.h
//  Делегат для взаимодействия с notifications через Firebase
//  Необходимо добавить его в проект приложения и вызвать WazzappsAds_SetNotificationsCallback(WazzappsFirebaseAdsNotifications.getInstance()) до WazzappsAds_InitSDK
//  Wazzapps AD SDK
//
//  Created by Mikhail Petrushkov on 08/08/16.
//  Copyright © 2016 Wazzapps. All rights reserved.
//
#import "WazzappsFirebaseAdsNotifications.h"

@implementation WazzappsFirebaseAdsNotifications

- (void)WazzappsOnInit {
    @try {
        [FIRApp configure];
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
}

- (void)WazzappsOnDidEnterBackground {
    [[FIRMessaging messaging] disconnect];
}

- (void)WazzappsOnDidBecomeActive {
    [self connectToFcm];
}

- (void)WazzappsOnPushRecieved:(NSDictionary*)pushInfo {
    // Print message ID.
    NSLog(@"Message ID: %@", pushInfo[@"gcm.message_id"]);
    
    // Pring full message.
    NSLog(@"%@", pushInfo);
}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    if(refreshedToken != NULL) {
        WazzappsAds_AddEvent(@"gcm_token_updated", @{@"token":refreshedToken});
    }
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}

- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}

@end

void WazzappsAds_InitFirebaseNotifications() {
    WazzappsAds_SetNotificationsCallback([[WazzappsFirebaseAdsNotifications alloc] init]);
}
