#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface GPSService : NSObject<CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    bool isReady;
    int authStatus;
    double latitude;
    double longitude;
    double accuracy;
}
@end