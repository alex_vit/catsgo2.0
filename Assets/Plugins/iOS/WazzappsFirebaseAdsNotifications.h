//
//  WazzappsAdSDK_Messages.h
//  Делегат для взаимодействия с notifications через Firebase
//  Необходимо добавить его в проект приложения и вызвать WazzappsAds_SetNotificationsCallback(WazzappsFirebaseAdsNotifications.getInstance()) до WazzappsAds_InitSDK
//  Wazzapps AD SDK
//
//  Created by Mikhail Petrushkov on 08/08/16.
//  Copyright © 2016 Wazzapps. All rights reserved.
//
#ifndef WazzappsAdSDK_FirebaseNotifications_h
#define WazzappsAdSDK_FirebaseNotifications_h

#ifdef UNITY_IOS
#import "../../../Unity-iPhone/Frameworks/WazzappsAdSDK.framework/Headers/WazzappsAds.h"
#else
#import "Frameworks/WazzappsAdSDK.framework/Headers/WazzappsAds.h"
#endif

@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;

void WazzappsAds_InitFirebaseNotifications();

@interface WazzappsFirebaseAdsNotifications : NSObject <WazzappsAdsNotificationsDelegate>
- (void)WazzappsOnInit;
- (void)WazzappsOnDidEnterBackground;
- (void)WazzappsOnDidBecomeActive;
- (void)WazzappsOnPushRecieved:(NSDictionary*)pushInfo;
- (void)tokenRefreshNotification:(NSNotification *)notification;
- (void)connectToFcm;
@end

#endif
