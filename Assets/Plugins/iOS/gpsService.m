#import "gpsService.h"

GPSService * service;

@implementation GPSService

- (bool)startGPSService
{
	isReady = false;
    if (self->locationManager == nil)
    {
		self->locationManager = [[CLLocationManager alloc] init];
		self->locationManager.desiredAccuracy = kCLLocationAccuracyBest;
		self->locationManager.delegate = self;
	}
    
    self->authStatus = -1;
    [self setAuthStatus];
    return self->authStatus==0;
    //return [self showNoticeOrStart];
}

- (void)setAuthStatus
{
    if(![CLLocationManager locationServicesEnabled]) {
        self->authStatus = 1;
    } else {
        switch ([CLLocationManager authorizationStatus]) {
            case kCLAuthorizationStatusRestricted:
                self->authStatus = 2;
                [self->locationManager requestWhenInUseAuthorization];
                break;
            case kCLAuthorizationStatusDenied:
                self->authStatus = 3;
                [self->locationManager requestWhenInUseAuthorization];
                break;
            case kCLAuthorizationStatusNotDetermined:
                self->authStatus = 4;
                break;
            default:
                self->authStatus = 0;
                [self->locationManager startUpdatingLocation];
                break;
        }
    }
    NSLog(@"GPS Status: %d", self->authStatus);
}

- (bool)showNoticeOrStart
{
    if(self->authStatus == 0) {
        [self->locationManager startUpdatingLocation];
        return true;
    } else if(self->authStatus != 4) {
        // go settings
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Использовать GPS"
                                                                       message:@"Для работы приложения необходимо включить и разрешить GPS (Настройки > Конфиденциальность > Службы геолокации)"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Включить"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"prefs:root=LOCATION_SERVICES"]];
                                                          }];
        [alert addAction:yesButton];
        UIViewController *rootController = [UIApplication sharedApplication].keyWindow.rootViewController;
        [rootController presentViewController:alert animated:YES completion:nil];
        return false;
    } else {
        [self->locationManager requestWhenInUseAuthorization];
        return false;
    }
}


- (void)stopGPSService
{
    [self->locationManager stopUpdatingLocation];
}

- (bool)isGPSServiceReady
{
    return isReady;
}

- (int)getGPSStatus
{
    return authStatus;
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    CLLocation *curPos = locationManager.location;

    self->latitude = curPos.coordinate.latitude;
    self->longitude = curPos.coordinate.longitude;
    self->accuracy = MAX(curPos.verticalAccuracy, curPos.horizontalAccuracy);
    
	/*NSString *slatitude = [[NSNumber numberWithDouble:curPos.coordinate.latitude] stringValue];
    NSString *slongitude = [[NSNumber numberWithDouble:curPos.coordinate.longitude] stringValue];

	NSLog(@"Lat: %@", slatitude);
	NSLog(@"Long: %@", slongitude);*/

	isReady = true;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    UnitySendMessage("GPSReciever", "OnAuthStatusChanged", "iOS: didChangeAuthorizationStatus");
}

- (double)getLatitude
{
    return self->latitude;
}

- (double)getLongitude
{
    return self->longitude;
}

- (double)getAccuracy
{
    return self->accuracy;
}

- (void) locationManager:(CLLocationManager *)manager 
        didFailWithError:(NSError *)error 
{
    NSLog(@"%@", @"Core location can't get a fix.");
}

- (CLLocationManager *) getManager
{
    return locationManager;
}

@end

bool startGPSService()
{
    if(service == NULL) {
        service = [[GPSService alloc] init];
    }
    return [service startGPSService];
}

int getGPSStatus()
{
    if(service != NULL) {
	    return [service getGPSStatus];
	} else {
		return -1;
	}
}

void stopGPSService()
{
    if(service != NULL) {
	    [service stopGPSService];
	}
}

void showNoticeOrStart()
{
    if(service != NULL) {
    	[service showNoticeOrStart];
	}
}

bool isGPSServiceReady()
{
    if(service != NULL) {
	    return [service isGPSServiceReady];
	}
    return false;
}


double getLatitude()
{
    double result = 0;
    if(service != NULL) {
        result = [service getManager].location.coordinate.latitude;
    }
    return result;
}

double getLongitude()
{
    double result = 0;
    if(service != NULL && [service getManager].location != NULL) {
        result = [service getManager].location.coordinate.longitude;
    }
    return result;
}



double getAccuracy()
{
    double result = 0;
    if(service != NULL) {
        result = [service getAccuracy];
    }
    return result;
}