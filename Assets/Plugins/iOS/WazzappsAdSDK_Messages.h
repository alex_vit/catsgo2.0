//
//  WazzappsAdSDK_Messages.h
//  Для возможности отправки сообщений в Unity необходимо задать внешнюю функцию, которой доступно UnitySendMessage
//  Wazzapps AD SDK
//
//  Created by Mikhail Petrushkov on 08/08/16.
//  Copyright © 2016 Wazzapps. All rights reserved.
//
#ifndef WazzappsAdSDK_Messages_h
#define WazzappsAdSDK_Messages_h

#ifdef UNITY_IOS
#import "../../../Unity-iPhone/Frameworks/WazzappsAdSDK.framework/Headers/WazzappsAds.h"
#else
#import "Frameworks/WazzappsAdSDK.framework/Headers/WazzappsAds.h"
#endif

extern void UnitySendMessage(const char *, const char *, const char *);
void WazzappsAds_InitUnityMessages();

@interface WazzappsAdsViewInterface : NSObject <WazzappsAdsViewDelegate>
- (void)WazzappsOnMessageRecieved:(const char*)go methodName:(const char*)method message:(const char*)msg;
@end

#endif
