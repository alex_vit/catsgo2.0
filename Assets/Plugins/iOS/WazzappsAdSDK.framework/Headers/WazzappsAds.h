//
//  SDK.h
//  Эти методы можно использовать в проектах, написанных на Obj-c и Unity
//  Проекты, написанные на Swift используют методы из WazzappsAds.swift
//  Wazzapps AD SDK
//
//  Created by Mikhail Petrushkov on 03/08/16.
//  Copyright © 2016 Wazzapps. All rights reserved.
//

#ifndef WazzappsAdSDK_h
#define WazzappsAdSDK_h

#import <UIKit/UIKit.h>

@protocol WazzappsAdsViewDelegate<NSObject>
@optional
- (void)WazzappsAdsViewLoaded;
- (void)WazzappsAdsViewClosed;
- (void)WazzappsAdsViewGoStore;
- (void)WazzappsOnSeeAlsoLoaded:(NSString*)data;
@end

@protocol WazzappsAdsUnityDelegate<NSObject>
- (void)WazzappsOnMessageRecieved:(const char*)gameObject methodName:(const char*)method message:(const char*)msg;
@end

@protocol WazzappsAdsNotificationsDelegate<NSObject>
- (void)WazzappsOnInit;
- (void)WazzappsOnDidEnterBackground;
- (void)WazzappsOnDidBecomeActive;
- (void)WazzappsOnPushRecieved:(NSDictionary*)pushInfo;
@end

extern const NSString * UNITY_GAME_OBJECT;

const int WAZZAPPS_AD_SDK_VERSION = 1;

// Loads Ad
void WazzappsAds_LoadAd(Boolean isForced);

// Check is Ad ready
bool WazzappsAds_IsAdReady();

// Show Ad if ready
void WazzappsAds_ShowAd();

// Load See Also
void WazzappsAds_LoadSeeAlso();

// Run See Also
void WazzappsAds_RunSeeAlsoAction();

// Set AD Delegate
void WazzappsAds_SetDelegate(id<WazzappsAdsViewDelegate> delegate);

// Init SDK and set token from info.plist
void WazzappsAds_InitSDK(NSDictionary * launchOptions);
void WazzappsAds_InitUnitySDK();

void WazzappsAds_AskAndInitNotifications();

// Init SDK and set token from info.plist
void WazzappsAds_SetNotificationsCallback(id<WazzappsAdsNotificationsDelegate> delegate);

// Set Unity callback to send messages
void WazzappsAds_SetUnityCallback(id<WazzappsAdsUnityDelegate> delegate);

// Did Pause
void WazzappsAds_ApplicationDidEnterBackground();

// Did Resume
void WazzappsAds_ApplicationDidBecomeActive();

// Will Destroy
void WazzappsAds_ApplicationWillTerminate();

// Push Recieved
void WazzappsAds_PushRecieved(NSDictionary * pushInfo);
void WazzappsAds_PushRecievedJSON(const char* pushInfo);

// Send callbacks for Unity
void WazzappsAds_SendCallbackToUnity(NSString *, NSString *, NSString *);

void WazzappsAds_AddEvent(NSString * name, NSDictionary * details);

#endif